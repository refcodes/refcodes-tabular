// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.zip.ZipException;

import org.refcodes.time.DateFormats;

/**
 * Declarative syntactic sugar which may be statically imported in order to
 * allow declarative definitions for the tabular package's elements, e.g. for
 * creating {@link Header} or {@link Column} instances or
 * {@link CsvRecordReader} ({@link CsvStringRecordReader}) or
 * {@link CsvRecordWriter} ({@link CsvStringRecordWriter}) instances.
 */
public class TabularSugar {

	private TabularSugar() {
		throw new IllegalStateException( "Utility class" );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 *
	 * @param <T> The type managed by the {@link Records}.
	 * 
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( ColumnFactory<T> aColumnFactory, File aCsvFile ) throws IOException {
		return new CsvRecordReader<>( aColumnFactory, aCsvFile, true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 *
	 * @param <T> The type managed by the {@link Records}.
	 * 
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link CsvRecordReader#getErroneousRecordCount()} is incremented
	 *        by each erroneous {@link Record}.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( ColumnFactory<T> aColumnFactory, File aCsvFile, boolean isStrict ) throws IOException {
		return new CsvRecordReader<>( aColumnFactory, aCsvFile, isStrict );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 *
	 * @param <T> The type managed by the {@link Records}.
	 * 
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( ColumnFactory<T> aColumnFactory, File aCsvFile, char aCsvSeparator ) throws IOException {
		return new CsvRecordReader<>( aColumnFactory, aCsvFile, aCsvSeparator, true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 *
	 * @param <T> The type managed by the {@link Records}.
	 * 
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link CsvRecordReader#getErroneousRecordCount()} is incremented
	 *        by each erroneous {@link Record}.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( ColumnFactory<T> aColumnFactory, File aCsvFile, char aCsvSeparator, boolean isStrict ) throws IOException {
		return new CsvRecordReader<>( aColumnFactory, aCsvFile, aCsvSeparator, isStrict );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 *
	 * @param <T> The type managed by the {@link Records}.
	 * 
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( ColumnFactory<T> aColumnFactory, File aCsvFile, Charset aEncoding ) throws IOException {
		return new CsvRecordReader<>( aColumnFactory, aCsvFile, aEncoding, true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 *
	 * @param <T> The type managed by the {@link Records}.
	 * 
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link CsvRecordReader#getErroneousRecordCount()} is incremented
	 *        by each erroneous {@link Record}.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( ColumnFactory<T> aColumnFactory, File aCsvFile, Charset aEncoding, boolean isStrict ) throws IOException {
		return new CsvRecordReader<>( aColumnFactory, aCsvFile, aEncoding, isStrict );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 *
	 * @param <T> The type managed by the {@link Records}.
	 * 
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( ColumnFactory<T> aColumnFactory, File aCsvFile, Charset aEncoding, char aCsvSeparator ) throws IOException {
		return new CsvRecordReader<>( aColumnFactory, aCsvFile, aEncoding, aCsvSeparator, true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 *
	 * @param <T> The type managed by the {@link Records}.
	 * 
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link CsvRecordReader#getErroneousRecordCount()} is incremented
	 *        by each erroneous {@link Record}.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( ColumnFactory<T> aColumnFactory, File aCsvFile, Charset aEncoding, char aCsvSeparator, boolean isStrict ) throws IOException {
		return new CsvRecordReader<>( aColumnFactory, aCsvFile, aEncoding, aCsvSeparator, isStrict );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 *
	 * @param <T> The generic type of the {@link Header}.
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( ColumnFactory<T> aColumnFactory, InputStream aCsvInputStream ) throws IOException {
		return new CsvRecordReader<>( aColumnFactory, aCsvInputStream, true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 *
	 * @param <T> The generic type of the {@link Header}.
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link CsvRecordReader#getErroneousRecordCount()} is incremented
	 *        by each erroneous {@link Record}.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( ColumnFactory<T> aColumnFactory, InputStream aCsvInputStream, boolean isStrict ) throws IOException {
		return new CsvRecordReader<>( aColumnFactory, aCsvInputStream, isStrict );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 *
	 * @param <T> The generic type of the {@link Header}.
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aCsvDelimiter The delimiter being expected for the CSV input
	 *        stream.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( ColumnFactory<T> aColumnFactory, InputStream aCsvInputStream, char aCsvDelimiter ) throws IOException {
		return new CsvRecordReader<>( aColumnFactory, aCsvInputStream, aCsvDelimiter, true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 *
	 * @param <T> The generic type of the {@link Header}.
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aCsvDelimiter The delimiter being expected for the CSV input
	 *        stream.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link CsvRecordReader#getErroneousRecordCount()} is incremented
	 *        by each erroneous {@link Record}.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( ColumnFactory<T> aColumnFactory, InputStream aCsvInputStream, char aCsvDelimiter, boolean isStrict ) throws IOException {
		return new CsvRecordReader<>( aColumnFactory, aCsvInputStream, aCsvDelimiter, isStrict );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 *
	 * @param <T> The generic type of the {@link Header}.
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( ColumnFactory<T> aColumnFactory, InputStream aCsvInputStream, Charset aEncoding ) throws IOException {
		return new CsvRecordReader<>( aColumnFactory, aCsvInputStream, aEncoding, true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 *
	 * @param <T> The generic type of the {@link Header}.
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link CsvRecordReader#getErroneousRecordCount()} is incremented
	 *        by each erroneous {@link Record}.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( ColumnFactory<T> aColumnFactory, InputStream aCsvInputStream, Charset aEncoding, boolean isStrict ) throws IOException {
		return new CsvRecordReader<>( aColumnFactory, aCsvInputStream, aEncoding, isStrict );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 *
	 * @param <T> The generic type of the {@link Header}.
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvDelimiter The delimiter being expected for the CSV input
	 *        stream.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( ColumnFactory<T> aColumnFactory, InputStream aCsvInputStream, Charset aEncoding, char aCsvDelimiter ) throws IOException {
		return new CsvRecordReader<>( aColumnFactory, aCsvInputStream, aEncoding, aCsvDelimiter, true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 *
	 * @param <T> The generic type of the {@link Header}.
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvDelimiter The delimiter being expected for the CSV input
	 *        stream.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link CsvRecordReader#getErroneousRecordCount()} is incremented
	 *        by each erroneous {@link Record}.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( ColumnFactory<T> aColumnFactory, InputStream aCsvInputStream, Charset aEncoding, char aCsvDelimiter, boolean isStrict ) throws IOException {
		return new CsvRecordReader<>( aColumnFactory, aCsvInputStream, aEncoding, aCsvDelimiter, isStrict );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link CsvRecordReader#getErroneousRecordCount()} is incremented
	 *        by each erroneous {@link Record}.
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * 
	 * @return The accordingly created {@link CsvStringRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static CsvStringRecordReader csvRecordReader( File aCsvFile, char aCsvSeparator, boolean isStrict, String... aHeader ) throws IOException {
		return new CsvStringRecordReader( aHeader, aCsvFile, aCsvSeparator, isStrict );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * 
	 * @return The accordingly created {@link CsvStringRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static CsvStringRecordReader csvRecordReader( File aCsvFile, char aCsvSeparator, String... aHeader ) throws IOException {
		return new CsvStringRecordReader( aHeader, aCsvFile, aCsvSeparator );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link CsvRecordReader#getErroneousRecordCount()} is incremented
	 *        by each erroneous {@link Record}.
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * 
	 * @return The accordingly created {@link CsvStringRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static CsvStringRecordReader csvRecordReader( File aCsvFile, Charset aEncoding, boolean isStrict, String... aHeader ) throws IOException {
		return new CsvStringRecordReader( aHeader, aCsvFile, aEncoding, isStrict );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link CsvRecordReader#getErroneousRecordCount()} is incremented
	 *        by each erroneous {@link Record}.
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * 
	 * @return The accordingly created {@link CsvStringRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static CsvStringRecordReader csvRecordReader( File aCsvFile, Charset aEncoding, char aCsvSeparator, boolean isStrict, String... aHeader ) throws IOException {
		return new CsvStringRecordReader( aHeader, aCsvFile, aEncoding, aCsvSeparator, isStrict );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * 
	 * @return The accordingly created {@link CsvStringRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static CsvStringRecordReader csvRecordReader( File aCsvFile, Charset aEncoding, char aCsvSeparator, String... aHeader ) throws IOException {
		return new CsvStringRecordReader( aHeader, aCsvFile, aEncoding, aCsvSeparator );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * 
	 * @return The accordingly created {@link CsvStringRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static CsvStringRecordReader csvRecordReader( File aCsvFile, Charset aEncoding, String... aHeader ) throws IOException {
		return new CsvStringRecordReader( aHeader, aCsvFile, aEncoding );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * 
	 * @return The accordingly created {@link CsvStringRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static CsvStringRecordReader csvRecordReader( File aCsvFile, String... aHeader ) throws IOException {
		return new CsvStringRecordReader( aHeader, aCsvFile );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param <T> The type managed by the {@link Records}.
	 * 
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( Header<T> aHeader, File aCsvFile ) throws IOException {
		return new CsvRecordReader<>( aHeader, aCsvFile, true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param <T> The type managed by the {@link Records}.
	 * 
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link CsvRecordReader#getErroneousRecordCount()} is incremented
	 *        by each erroneous {@link Record}.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( Header<T> aHeader, File aCsvFile, boolean isStrict ) throws IOException {
		return new CsvRecordReader<>( aHeader, aCsvFile, isStrict );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param <T> The type managed by the {@link Records}.
	 * 
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( Header<T> aHeader, File aCsvFile, char aCsvSeparator ) throws IOException {
		return new CsvRecordReader<>( aHeader, aCsvFile, aCsvSeparator, true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param <T> The type managed by the {@link Records}.
	 * 
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link CsvRecordReader#getErroneousRecordCount()} is incremented
	 *        by each erroneous {@link Record}.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( Header<T> aHeader, File aCsvFile, char aCsvSeparator, boolean isStrict ) throws IOException {
		return new CsvRecordReader<>( aHeader, aCsvFile, aCsvSeparator, isStrict );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param <T> The type managed by the {@link Records}.
	 * 
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( Header<T> aHeader, File aCsvFile, Charset aEncoding ) throws IOException {
		return new CsvRecordReader<>( aHeader, aCsvFile, aEncoding, true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param <T> The type managed by the {@link Records}.
	 * 
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link CsvRecordReader#getErroneousRecordCount()} is incremented
	 *        by each erroneous {@link Record}.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( Header<T> aHeader, File aCsvFile, Charset aEncoding, boolean isStrict ) throws IOException {
		return new CsvRecordReader<>( aHeader, aCsvFile, aEncoding, isStrict );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param <T> The type managed by the {@link Records}.
	 * 
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( Header<T> aHeader, File aCsvFile, Charset aEncoding, char aCsvSeparator ) throws IOException {
		return new CsvRecordReader<>( aHeader, aCsvFile, aEncoding, aCsvSeparator, true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param <T> The type managed by the {@link Records}.
	 * 
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link CsvRecordReader#getErroneousRecordCount()} is incremented
	 *        by each erroneous {@link Record}.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( Header<T> aHeader, File aCsvFile, Charset aEncoding, char aCsvSeparator, boolean isStrict ) throws IOException {
		return new CsvRecordReader<>( aHeader, aCsvFile, aEncoding, aCsvSeparator, isStrict );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param <T> The generic type of the {@link Header}.
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( Header<T> aHeader, InputStream aCsvInputStream ) throws IOException {
		return new CsvRecordReader<>( aHeader, aCsvInputStream, true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param <T> The generic type of the {@link Header}.
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link CsvRecordReader#getErroneousRecordCount()} is incremented
	 *        by each erroneous {@link Record}.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( Header<T> aHeader, InputStream aCsvInputStream, boolean isStrict ) throws IOException {
		return new CsvRecordReader<>( aHeader, aCsvInputStream, isStrict );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param <T> The type managed by the {@link Records}.
	 * 
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aCsvDelimiter The delimiter being expected for the CSV input
	 *        stream.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( Header<T> aHeader, InputStream aCsvInputStream, char aCsvDelimiter ) throws IOException {
		return new CsvRecordReader<>( aHeader, aCsvInputStream, aCsvDelimiter, true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param <T> The generic type of the {@link Header}.
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aCsvDelimiter The delimiter being expected for the CSV input
	 *        stream.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link CsvRecordReader#getErroneousRecordCount()} is incremented
	 *        by each erroneous {@link Record}.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( Header<T> aHeader, InputStream aCsvInputStream, char aCsvDelimiter, boolean isStrict ) throws IOException {
		return new CsvRecordReader<>( aHeader, aCsvInputStream, aCsvDelimiter, isStrict );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param <T> The generic type of the {@link Header}.
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( Header<T> aHeader, InputStream aCsvInputStream, Charset aEncoding ) throws IOException {
		return new CsvRecordReader<>( aHeader, aCsvInputStream, aEncoding, true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param <T> The generic type of the {@link Header}.
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link CsvRecordReader#getErroneousRecordCount()} is incremented
	 *        by each erroneous {@link Record}.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( Header<T> aHeader, InputStream aCsvInputStream, Charset aEncoding, boolean isStrict ) throws IOException {
		return new CsvRecordReader<>( aHeader, aCsvInputStream, aEncoding, isStrict );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param <T> The type managed by the {@link Records}.
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding The {@link Charset } to be used as encoding.
	 * @param aCsvDelimiter The delimiter being expected for the CSV input
	 *        stream.
	 * 
	 * @return the csv record reader
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( Header<T> aHeader, InputStream aCsvInputStream, Charset aEncoding, char aCsvDelimiter ) throws IOException {
		return new CsvRecordReader<>( aHeader, aCsvInputStream, aEncoding, aCsvDelimiter, true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param <T> The generic type of the {@link Header}.
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvDelimiter The delimiter being expected for the CSV input
	 *        stream.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link CsvRecordReader#getErroneousRecordCount()} is incremented
	 *        by each erroneous {@link Record}.
	 * 
	 * @return The accordingly created {@link CsvRecordReader}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public static <T> CsvRecordReader<T> csvRecordReader( Header<T> aHeader, InputStream aCsvInputStream, Charset aEncoding, char aCsvDelimiter, boolean isStrict ) throws IOException {
		return new CsvRecordReader<>( aHeader, aCsvInputStream, aEncoding, aCsvDelimiter, isStrict );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link CsvRecordReader#getErroneousRecordCount()} is incremented
	 *        by each erroneous {@link Record}.
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * 
	 * @return The accordingly created {@link CsvStringRecordReader}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public static CsvStringRecordReader csvRecordReader( InputStream aCsvInputStream, boolean isStrict, String... aHeader ) throws IOException {
		return new CsvStringRecordReader( aHeader, aCsvInputStream, isStrict );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aCsvDelimiter The delimiter being expected for the CSV input
	 *        stream.
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * 
	 * @return The accordingly created {@link CsvStringRecordReader}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public static CsvStringRecordReader csvRecordReader( InputStream aCsvInputStream, char aCsvDelimiter, String... aHeader ) throws IOException {
		return new CsvStringRecordReader( aHeader, aCsvInputStream, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link CsvRecordReader#getErroneousRecordCount()} is incremented
	 *        by each erroneous {@link Record}.
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * 
	 * @return The accordingly created {@link CsvStringRecordReader}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public static CsvStringRecordReader csvRecordReader( InputStream aCsvInputStream, Charset aEncoding, boolean isStrict, String... aHeader ) throws IOException {
		return new CsvStringRecordReader( aHeader, aCsvInputStream, aEncoding, isStrict );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding The {@link Charset } to be used as encoding.
	 * @param aCsvDelimiter The delimiter being expected for the CSV input
	 *        stream.
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * 
	 * @return The accordingly created {@link CsvStringRecordReader}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public static CsvStringRecordReader csvRecordReader( InputStream aCsvInputStream, Charset aEncoding, char aCsvDelimiter, String... aHeader ) throws IOException {
		return new CsvStringRecordReader( aHeader, aCsvInputStream, aEncoding, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * 
	 * @return The accordingly created {@link CsvStringRecordReader}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public static CsvStringRecordReader csvRecordReader( InputStream aCsvInputStream, Charset aEncoding, String... aHeader ) throws IOException {
		return new CsvStringRecordReader( aHeader, aCsvInputStream, aEncoding );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * 
	 * @return The accordingly created {@link CsvStringRecordReader}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public static CsvStringRecordReader csvRecordReader( InputStream aCsvInputStream, String... aHeader ) throws IOException {
		return new CsvStringRecordReader( aHeader, aCsvInputStream );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * 
	 * @return The accordingly created {@link CsvStringRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static CsvStringRecordReader csvRecordReader( String[] aHeader, File aCsvFile ) throws IOException {
		return new CsvStringRecordReader( aHeader, aCsvFile );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link CsvRecordReader#getErroneousRecordCount()} is incremented
	 *        by each erroneous {@link Record}.
	 * 
	 * @return The accordingly created {@link CsvStringRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static CsvStringRecordReader csvRecordReader( String[] aHeader, File aCsvFile, boolean isStrict ) throws IOException {
		return new CsvStringRecordReader( aHeader, aCsvFile, isStrict );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * 
	 * @return The accordingly created {@link CsvStringRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static CsvStringRecordReader csvRecordReader( String[] aHeader, File aCsvFile, char aCsvSeparator ) throws IOException {
		return new CsvStringRecordReader( aHeader, aCsvFile, aCsvSeparator );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link CsvRecordReader#getErroneousRecordCount()} is incremented
	 *        by each erroneous {@link Record}.
	 * 
	 * @return The accordingly created {@link CsvStringRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static CsvStringRecordReader csvRecordReader( String[] aHeader, File aCsvFile, char aCsvSeparator, boolean isStrict ) throws IOException {
		return new CsvStringRecordReader( aHeader, aCsvFile, aCsvSeparator, isStrict );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * 
	 * @return The accordingly created {@link CsvStringRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static CsvStringRecordReader csvRecordReader( String[] aHeader, File aCsvFile, Charset aEncoding ) throws IOException {
		return new CsvStringRecordReader( aHeader, aCsvFile, aEncoding );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link CsvRecordReader#getErroneousRecordCount()} is incremented
	 *        by each erroneous {@link Record}.
	 * 
	 * @return The accordingly created {@link CsvStringRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static CsvStringRecordReader csvRecordReader( String[] aHeader, File aCsvFile, Charset aEncoding, boolean isStrict ) throws IOException {
		return new CsvStringRecordReader( aHeader, aCsvFile, aEncoding, isStrict );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * 
	 * @return The accordingly created {@link CsvStringRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static CsvStringRecordReader csvRecordReader( String[] aHeader, File aCsvFile, Charset aEncoding, char aCsvSeparator ) throws IOException {
		return new CsvStringRecordReader( aHeader, aCsvFile, aEncoding, aCsvSeparator );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link CsvRecordReader#getErroneousRecordCount()} is incremented
	 *        by each erroneous {@link Record}.
	 * 
	 * @return The accordingly created {@link CsvStringRecordReader}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public static CsvStringRecordReader csvRecordReader( String[] aHeader, File aCsvFile, Charset aEncoding, char aCsvSeparator, boolean isStrict ) throws IOException {
		return new CsvStringRecordReader( aHeader, aCsvFile, aEncoding, aCsvSeparator, isStrict );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * 
	 * @return The accordingly created {@link CsvStringRecordReader}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public static CsvStringRecordReader csvRecordReader( String[] aHeader, InputStream aCsvInputStream ) throws IOException {
		return new CsvStringRecordReader( aHeader, aCsvInputStream );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link CsvRecordReader#getErroneousRecordCount()} is incremented
	 *        by each erroneous {@link Record}.
	 * 
	 * @return The accordingly created {@link CsvStringRecordReader}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public static CsvStringRecordReader csvRecordReader( String[] aHeader, InputStream aCsvInputStream, boolean isStrict ) throws IOException {
		return new CsvStringRecordReader( aHeader, aCsvInputStream, isStrict );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aCsvDelimiter The delimiter being expected for the CSV input
	 *        stream.
	 * 
	 * @return The accordingly created {@link CsvStringRecordReader}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public static CsvStringRecordReader csvRecordReader( String[] aHeader, InputStream aCsvInputStream, char aCsvDelimiter ) throws IOException {
		return new CsvStringRecordReader( aHeader, aCsvInputStream, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * 
	 * @return The accordingly created {@link CsvStringRecordReader}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public static CsvStringRecordReader csvRecordReader( String[] aHeader, InputStream aCsvInputStream, Charset aEncoding ) throws IOException {
		return new CsvStringRecordReader( aHeader, aCsvInputStream, aEncoding );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link CsvRecordReader#getErroneousRecordCount()} is incremented
	 *        by each erroneous {@link Record}.
	 * 
	 * @return The accordingly created {@link CsvStringRecordReader}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public static CsvStringRecordReader csvRecordReader( String[] aHeader, InputStream aCsvInputStream, Charset aEncoding, boolean isStrict ) throws IOException {
		return new CsvStringRecordReader( aHeader, aCsvInputStream, aEncoding, isStrict );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding The {@link Charset } to be used as encoding.
	 * @param aCsvDelimiter The delimiter being expected for the CSV input
	 *        stream.
	 * 
	 * @return The accordingly created {@link CsvStringRecordReader}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public static CsvStringRecordReader csvRecordReader( String[] aHeader, InputStream aCsvInputStream, Charset aEncoding, char aCsvDelimiter ) throws IOException {
		return new CsvStringRecordReader( aHeader, aCsvInputStream, aEncoding, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param <T> The generic type of the {@link Header}.
	 * @param aColumnFactory The header used for logging in the correct format.
	 * @param aCsvFile The {@link File} to be used for printing.
	 * 
	 * @return The accordingly created {@link CsvRecordWriter}.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 *         opening or creating the file.
	 */
	public static <T> CsvRecordWriter<T> csvRecordWriter( ColumnFactory<T> aColumnFactory, File aCsvFile ) throws FileNotFoundException {
		return new CsvRecordWriter<>( aColumnFactory, aCsvFile );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param <T> The generic type of the {@link Header}.
	 * @param aColumnFactory The header used for logging in the correct format.
	 * @param aCsvFile The {@link File} to be used for printing.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @return The accordingly created {@link CsvRecordWriter}.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 *         opening or creating the file.
	 */
	public static <T> CsvRecordWriter<T> csvRecordWriter( ColumnFactory<T> aColumnFactory, File aCsvFile, char aCsvDelimiter ) throws FileNotFoundException {
		return new CsvRecordWriter<>( aColumnFactory, aCsvFile, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param <T> The generic type of the {@link Header}.
	 * @param aColumnFactory The header used for logging in the correct format.
	 * @param aCsvFile The {@link File} to be used for printing.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * 
	 * @return The accordingly created {@link CsvRecordWriter}.
	 * 
	 * @throws IOException in case the file is not found or the encoding is not
	 *         supported.
	 */
	public static <T> CsvRecordWriter<T> csvRecordWriter( ColumnFactory<T> aColumnFactory, File aCsvFile, Charset aEncoding ) throws IOException {
		return new CsvRecordWriter<>( aColumnFactory, new PrintStream( aCsvFile, aEncoding.name() ) );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param <T> The generic type of the {@link Header}.
	 * @param aColumnFactory The header used for logging in the correct format.
	 * @param aCsvFile The {@link File} to be used for printing.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @return The accordingly created {@link CsvRecordWriter}.
	 * 
	 * @throws IOException in case the file is not found or the encoding is not
	 *         supported.
	 */
	public static <T> CsvRecordWriter<T> csvRecordWriter( ColumnFactory<T> aColumnFactory, File aCsvFile, Charset aEncoding, char aCsvDelimiter ) throws IOException {
		return new CsvRecordWriter<>( aColumnFactory, new PrintStream( aCsvFile, aEncoding.name() ), aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param <T> The generic type of the {@link Header}.
	 * @param aColumnFactory The header used for logging in the correct format.
	 * @param aCsvOutputStream The {@link OutputStream} to be used for printing.
	 * 
	 * @return The accordingly created {@link CsvRecordWriter}.
	 */
	public static <T> CsvRecordWriter<T> csvRecordWriter( ColumnFactory<T> aColumnFactory, OutputStream aCsvOutputStream ) {
		return new CsvRecordWriter<>( aColumnFactory, aCsvOutputStream );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param <T> The generic type of the {@link Header}.
	 * @param aColumnFactory The header used for logging in the correct format.
	 * @param aCsvOutputStream The {@link OutputStream} to be used for printing.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @return The accordingly created {@link CsvRecordWriter}.
	 */
	public static <T> CsvRecordWriter<T> csvRecordWriter( ColumnFactory<T> aColumnFactory, OutputStream aCsvOutputStream, char aCsvDelimiter ) {
		return new CsvRecordWriter<>( aColumnFactory, aCsvOutputStream, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param <T> The generic type of the {@link Header}.
	 * @param aColumnFactory The header used for logging in the correct format.
	 * @param aCsvOutputStream The {@link OutputStream} to be used for printing.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * 
	 * @return The accordingly created {@link CsvRecordWriter}.
	 * 
	 * @throws UnsupportedEncodingException thrown in case an unsupported
	 *         encoding ({@link Charset}) has been provided.
	 */
	public static <T> CsvRecordWriter<T> csvRecordWriter( ColumnFactory<T> aColumnFactory, OutputStream aCsvOutputStream, Charset aEncoding ) throws UnsupportedEncodingException {
		return new CsvRecordWriter<>( aColumnFactory, new PrintStream( aCsvOutputStream, true, aEncoding.name() ) );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param <T> The generic type of the {@link Header}.
	 * @param aColumnFactory The header used for logging in the correct format.
	 * @param aCsvOutputStream The {@link OutputStream} to be used for printing.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @return The accordingly created {@link CsvRecordWriter}.
	 * 
	 * @throws UnsupportedEncodingException thrown in case an unsupported
	 *         encoding ({@link Charset}) has been provided.
	 */
	public static <T> CsvRecordWriter<T> csvRecordWriter( ColumnFactory<T> aColumnFactory, OutputStream aCsvOutputStream, Charset aEncoding, char aCsvDelimiter ) throws UnsupportedEncodingException {
		return new CsvRecordWriter<>( aColumnFactory, new PrintStream( aCsvOutputStream, true, aEncoding.name() ), aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param <T> The generic type of the {@link Header}.
	 * @param aColumnFactory The header used for logging in the correct format.
	 * @param aPrintStream The {@link PrintStream} to be used for printing.
	 * 
	 * @return The accordingly created {@link CsvRecordWriter}.
	 */
	public static <T> CsvRecordWriter<T> csvRecordWriter( ColumnFactory<T> aColumnFactory, PrintStream aPrintStream ) {
		return new CsvRecordWriter<>( aColumnFactory, aPrintStream );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param <T> The generic type of the {@link Header}.
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aPrintStream The {@link PrintStream} to be used for printing
	 *        output.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @return The accordingly created {@link CsvRecordWriter}.
	 */
	public static <T> CsvRecordWriter<T> csvRecordWriter( ColumnFactory<T> aColumnFactory, PrintStream aPrintStream, char aCsvDelimiter ) {
		return new CsvRecordWriter<>( aColumnFactory, aPrintStream, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter}.
	 *
	 * @param <T> The generic type of the {@link Header}.
	 * @param aCsvFile The {@link File} to be used for printing output.
	 * 
	 * @return The accordingly created {@link CsvRecordWriter}.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 *         opening or creating the file.
	 */
	public static <T> CsvRecordWriter<T> csvRecordWriter( File aCsvFile ) throws FileNotFoundException {
		return new CsvRecordWriter<>( aCsvFile );
	}

	/**
	 * Constructs a {@link CsvRecordWriter}.
	 *
	 * @param <T> The generic type of the {@link Header}.
	 * @param aCsvFile The {@link File} to be used for printing output.
	 *        aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @return The accordingly created {@link CsvRecordWriter}.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 *         opening or creating the file.
	 */
	public static <T> CsvRecordWriter<T> csvRecordWriter( File aCsvFile, char aCsvDelimiter ) throws FileNotFoundException {
		return new CsvRecordWriter<>( aCsvFile, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aFile the {@link File} to which to write.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * 
	 * @return The accordingly created {@link CsvStringRecordWriter}.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 */

	public static CsvStringRecordWriter csvRecordWriter( File aFile, char aCsvDelimiter, String... aHeader ) throws FileNotFoundException {
		return new CsvStringRecordWriter( aHeader, aFile, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter}.
	 *
	 * @param <T> the generic type
	 * @param aCsvFile The {@link File} to be used for printing output.
	 * @param aEncoding The {@link Charset } to be used as encoding.
	 * 
	 * @return The accordingly created {@link CsvRecordWriter}.
	 * 
	 * @throws IOException in case the file is not found or the encoding is not
	 *         supported.
	 */
	public static <T> CsvRecordWriter<T> csvRecordWriter( File aCsvFile, Charset aEncoding ) throws IOException {
		return new CsvRecordWriter<>( new PrintStream( aCsvFile, aEncoding.name() ) );
	}

	/**
	 * Constructs a {@link CsvRecordWriter}.
	 *
	 * @param <T> the generic type
	 * @param aCsvFile The {@link File} to be used for printing output.
	 *        aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * @param aEncoding The {@link Charset } to be used as encoding.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @return The accordingly created {@link CsvRecordWriter}.
	 * 
	 * @throws IOException in case the file is not found or the encoding is not
	 *         supported.
	 */
	public static <T> CsvRecordWriter<T> csvRecordWriter( File aCsvFile, Charset aEncoding, char aCsvDelimiter ) throws IOException {
		return new CsvRecordWriter<>( aCsvFile, aEncoding, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aFile the {@link File} to which to write.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * 
	 * @return The accordingly created {@link CsvStringRecordWriter}.
	 * 
	 * @throws IOException in case the file is not found or the encoding is not
	 *         supported.
	 */

	public static CsvStringRecordWriter csvRecordWriter( File aFile, Charset aEncoding, char aCsvDelimiter, String... aHeader ) throws IOException {
		return new CsvStringRecordWriter( aHeader, aFile, aEncoding, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aFile the {@link File} to which to write.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * 
	 * @return The accordingly created {@link CsvStringRecordWriter}.
	 * 
	 * @throws IOException in case the file is not found or the encoding is not
	 *         supported.
	 */

	public static CsvStringRecordWriter csvRecordWriter( File aFile, Charset aEncoding, String... aHeader ) throws IOException {
		return new CsvStringRecordWriter( aHeader, aFile, aEncoding );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aFile the {@link File} to which to write.
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * 
	 * @return The accordingly created {@link CsvStringRecordWriter}.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 *         opening or creating the file.
	 */

	public static CsvStringRecordWriter csvRecordWriter( File aFile, String... aHeader ) throws FileNotFoundException {
		return new CsvStringRecordWriter( aHeader, aFile );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param <T> The generic type of the {@link Header}.
	 * @param aHeader The header used for logging in the correct format.
	 * @param aCsvFile The {@link File} to be used for printing.
	 * 
	 * @return The accordingly created {@link CsvRecordWriter}.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 *         opening or creating the file.
	 */
	public static <T> CsvRecordWriter<T> csvRecordWriter( Header<T> aHeader, File aCsvFile ) throws FileNotFoundException {
		return new CsvRecordWriter<>( aHeader, aCsvFile );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param <T> The generic type of the {@link Header}.
	 * @param aHeader The header used for logging in the correct format.
	 * @param aCsvFile The {@link File} to be used for printing.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @return The accordingly created {@link CsvRecordWriter}.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 *         opening or creating the file.
	 */
	public static <T> CsvRecordWriter<T> csvRecordWriter( Header<T> aHeader, File aCsvFile, char aCsvDelimiter ) throws FileNotFoundException {
		return new CsvRecordWriter<>( aHeader, aCsvFile, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param <T> The generic type of the {@link Header}.
	 * @param aHeader The header used for logging in the correct format.
	 * @param aCsvFile The {@link File} to be used for printing.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * 
	 * @return The accordingly created {@link CsvRecordWriter}.
	 * 
	 * @throws IOException in case the file is not found or the encoding is not
	 *         supported.
	 */
	public static <T> CsvRecordWriter<T> csvRecordWriter( Header<T> aHeader, File aCsvFile, Charset aEncoding ) throws IOException {
		return new CsvRecordWriter<>( aHeader, new PrintStream( aCsvFile, aEncoding.name() ) );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param <T> The generic type of the {@link Header}.
	 * @param aHeader The header used for logging in the correct format.
	 * @param aCsvFile The {@link File} to be used for printing.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @return The accordingly created {@link CsvRecordWriter}.
	 * 
	 * @throws IOException in case the file is not found or the encoding is not
	 *         supported.
	 */
	public static <T> CsvRecordWriter<T> csvRecordWriter( Header<T> aHeader, File aCsvFile, Charset aEncoding, char aCsvDelimiter ) throws IOException {
		return new CsvRecordWriter<>( aHeader, new PrintStream( aCsvFile, aEncoding.name() ), aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param <T> The generic type of the {@link Header}.
	 * @param aHeader The header used for logging in the correct format.
	 * @param aCsvOutputStream The {@link OutputStream} to be used for printing.
	 * 
	 * @return The accordingly created {@link CsvRecordWriter}.
	 */
	public static <T> CsvRecordWriter<T> csvRecordWriter( Header<T> aHeader, OutputStream aCsvOutputStream ) {
		return new CsvRecordWriter<>( aHeader, aCsvOutputStream );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param <T> The generic type of the {@link Header}.
	 * @param aHeader The header used for logging in the correct format.
	 * @param aCsvOutputStream The {@link OutputStream} to be used for printing.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @return The accordingly created {@link CsvRecordWriter}.
	 */
	public static <T> CsvRecordWriter<T> csvRecordWriter( Header<T> aHeader, OutputStream aCsvOutputStream, char aCsvDelimiter ) {
		return new CsvRecordWriter<>( aHeader, aCsvOutputStream, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param <T> The generic type of the {@link Header}.
	 * @param aHeader The header used for logging in the correct format.
	 * @param aCsvOutputStream The {@link OutputStream} to be used for printing.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * 
	 * @return The accordingly created {@link CsvRecordWriter}.
	 * 
	 * @throws UnsupportedEncodingException thrown in case an unsupported
	 *         encoding ({@link Charset}) has been provided.
	 */
	public static <T> CsvRecordWriter<T> csvRecordWriter( Header<T> aHeader, OutputStream aCsvOutputStream, Charset aEncoding ) throws UnsupportedEncodingException {
		return new CsvRecordWriter<>( aHeader, new PrintStream( aCsvOutputStream, true, aEncoding.name() ) );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param <T> The generic type of the {@link Header}.
	 * @param aHeader The header used for logging in the correct format.
	 * @param aCsvOutputStream The {@link OutputStream} to be used for printing.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @return The accordingly created {@link CsvRecordWriter}.
	 * 
	 * @throws UnsupportedEncodingException thrown in case an unsupported
	 *         encoding ({@link Charset}) has been provided.
	 */
	public static <T> CsvRecordWriter<T> csvRecordWriter( Header<T> aHeader, OutputStream aCsvOutputStream, Charset aEncoding, char aCsvDelimiter ) throws UnsupportedEncodingException {
		return new CsvRecordWriter<>( aHeader, new PrintStream( aCsvOutputStream, true, aEncoding.name() ), aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param <T> The generic type of the {@link Header}.
	 * @param aHeader The header used for logging in the correct format.
	 * @param aPrintStream The {@link PrintStream} to be used for printing.
	 * 
	 * @return The accordingly created {@link CsvRecordWriter}.
	 */
	public static <T> CsvRecordWriter<T> csvRecordWriter( Header<T> aHeader, PrintStream aPrintStream ) {
		return new CsvRecordWriter<>( aHeader, aPrintStream );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param <T> The generic type of the {@link Header}.
	 * @param aHeader The header used for logging in the correct format.
	 * @param aPrintStream The {@link PrintStream} to be used for printing.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @return The accordingly created {@link CsvRecordWriter}.
	 */
	public static <T> CsvRecordWriter<T> csvRecordWriter( Header<T> aHeader, PrintStream aPrintStream, char aCsvDelimiter ) {
		return new CsvRecordWriter<>( aHeader, aPrintStream, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter}.
	 * 
	 * @param <T> The generic type of the {@link Header}.
	 * @param aCsvOutputStream The {@link OutputStream} to be used for printing
	 *        output.
	 * 
	 * @return The accordingly created {@link CsvRecordWriter}.
	 */
	public static <T> CsvRecordWriter<T> csvRecordWriter( OutputStream aCsvOutputStream ) {
		return new CsvRecordWriter<>( aCsvOutputStream );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aOutputStream The {@link OutputStream} where to write to.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * 
	 * @return The accordingly created {@link CsvStringRecordWriter}.
	 */

	public static CsvStringRecordWriter csvRecordWriter( OutputStream aOutputStream, char aCsvDelimiter, String... aHeader ) {
		return new CsvStringRecordWriter( aHeader, aOutputStream, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter}.
	 *
	 * @param <T> the generic type
	 * @param aCsvOutputStream The {@link OutputStream} to be used for printing
	 *        output.
	 * @param aEncoding The {@link Charset } to be used as encoding.
	 * 
	 * @return The accordingly created {@link CsvRecordWriter}.
	 * 
	 * @throws UnsupportedEncodingException thrown in case an unsupported
	 *         encoding ({@link Charset}) has been provided.
	 */
	public static <T> CsvRecordWriter<T> csvRecordWriter( OutputStream aCsvOutputStream, Charset aEncoding ) throws UnsupportedEncodingException {
		return new CsvRecordWriter<>( new PrintStream( aCsvOutputStream, true, aEncoding.name() ) );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aOutputStream The {@link OutputStream} where to write to.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * 
	 * @return The accordingly created {@link CsvStringRecordWriter}.
	 * 
	 * @throws UnsupportedEncodingException thrown in case an unsupported
	 *         encoding ({@link Charset}) has been provided.
	 */

	public static CsvStringRecordWriter csvRecordWriter( OutputStream aOutputStream, Charset aEncoding, char aCsvDelimiter, String... aHeader ) throws UnsupportedEncodingException {
		return new CsvStringRecordWriter( aHeader, aOutputStream, aEncoding, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aOutputStream The {@link OutputStream} where to write to.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * 
	 * @return The accordingly created {@link CsvStringRecordWriter}.
	 * 
	 * @throws UnsupportedEncodingException thrown in case an unsupported
	 *         encoding ({@link Charset}) has been provided.
	 */

	public static CsvStringRecordWriter csvRecordWriter( OutputStream aOutputStream, Charset aEncoding, String... aHeader ) throws UnsupportedEncodingException {
		return new CsvStringRecordWriter( aHeader, aOutputStream, aEncoding );
	}

	/**
	 * Constructs a {@link CsvRecordWriter}.
	 * 
	 * @param <T> The generic type of the {@link Header}.
	 * @param aPrintStream The {@link PrintStream} to be used for printing.
	 * 
	 * @return The accordingly created {@link CsvRecordWriter}.
	 */
	public static <T> CsvRecordWriter<T> csvRecordWriter( PrintStream aPrintStream ) {
		return new CsvRecordWriter<>( aPrintStream );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aPrintStream The {@link PrintStream} to be used for printing
	 *        output.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * 
	 * @return The accordingly created {@link CsvStringRecordWriter}.
	 */

	public static CsvStringRecordWriter csvRecordWriter( PrintStream aPrintStream, char aCsvDelimiter, String... aHeader ) {
		return new CsvStringRecordWriter( aHeader, aPrintStream, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * @param aFile the {@link File} to which to write.
	 * 
	 * @return The accordingly created {@link CsvStringRecordWriter}.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 *         opening or creating the file.
	 */

	public static CsvStringRecordWriter csvRecordWriter( String[] aHeader, File aFile ) throws FileNotFoundException {
		return new CsvStringRecordWriter( aHeader, aFile );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * @param aFile the {@link File} to which to write.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @return The accordingly created {@link CsvStringRecordWriter}.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 */

	public static CsvStringRecordWriter csvRecordWriter( String[] aHeader, File aFile, char aCsvDelimiter ) throws FileNotFoundException {
		return new CsvStringRecordWriter( aHeader, aFile, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * @param aFile the {@link File} to which to write.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * 
	 * @return The accordingly created {@link CsvStringRecordWriter}.
	 * 
	 * @throws IOException in case the file is not found or the encoding is not
	 *         supported.
	 */

	public static CsvStringRecordWriter csvRecordWriter( String[] aHeader, File aFile, Charset aEncoding ) throws IOException {
		return new CsvStringRecordWriter( aHeader, aFile, aEncoding );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * @param aFile the {@link File} to which to write.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @return The accordingly created {@link CsvStringRecordWriter}.
	 * 
	 * @throws IOException in case the file is not found or the encoding is not
	 *         supported.
	 */

	public static CsvStringRecordWriter csvRecordWriter( String[] aHeader, File aFile, Charset aEncoding, char aCsvDelimiter ) throws IOException {
		return new CsvStringRecordWriter( aHeader, aFile, aEncoding, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * @param aOutputStream The {@link OutputStream} where to write to.
	 * 
	 * @return The accordingly created {@link CsvStringRecordWriter}.
	 */

	public static CsvStringRecordWriter csvRecordWriter( String[] aHeader, OutputStream aOutputStream ) {
		return new CsvStringRecordWriter( aHeader, aOutputStream );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * @param aOutputStream The {@link OutputStream} where to write to.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @return The accordingly created {@link CsvStringRecordWriter}.
	 */

	public static CsvStringRecordWriter csvRecordWriter( String[] aHeader, OutputStream aOutputStream, char aCsvDelimiter ) {
		return new CsvStringRecordWriter( aHeader, aOutputStream, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * @param aOutputStream The {@link OutputStream} where to write to.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * 
	 * @return The accordingly created {@link CsvStringRecordWriter}.
	 * 
	 * @throws UnsupportedEncodingException thrown in case an unsupported
	 *         encoding ({@link Charset}) has been provided.
	 */

	public static CsvStringRecordWriter csvRecordWriter( String[] aHeader, OutputStream aOutputStream, Charset aEncoding ) throws UnsupportedEncodingException {
		return new CsvStringRecordWriter( aHeader, aOutputStream, aEncoding );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * @param aOutputStream The {@link OutputStream} where to write to.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @return The accordingly created {@link CsvStringRecordWriter}.
	 * 
	 * @throws UnsupportedEncodingException thrown in case an unsupported
	 *         encoding ({@link Charset}) has been provided.
	 */

	public static CsvStringRecordWriter csvRecordWriter( String[] aHeader, OutputStream aOutputStream, Charset aEncoding, char aCsvDelimiter ) throws UnsupportedEncodingException {
		return new CsvStringRecordWriter( aHeader, aOutputStream, aEncoding, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * @param aPrintStream The {@link PrintStream} to be used for printing.
	 * 
	 * @return The accordingly created {@link CsvStringRecordWriter}.
	 */

	public static CsvStringRecordWriter csvRecordWriter( String[] aHeader, PrintStream aPrintStream ) {
		return new CsvStringRecordWriter( aHeader, aPrintStream );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * @param aPrintStream The {@link PrintStream} to be used for printing
	 *        output.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @return The accordingly created {@link CsvStringRecordWriter}.
	 */

	public static CsvStringRecordWriter csvRecordWriter( String[] aHeader, PrintStream aPrintStream, char aCsvDelimiter ) {
		return new CsvStringRecordWriter( aHeader, aPrintStream, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link Column} with support for the default
	 * {@link SimpleDateFormat} instances as defined in the
	 * {@link DateFormats#DEFAULT_DATE_FORMATS}.
	 * 
	 * @param aKey The key to be used.
	 * 
	 * @return The according {@link Column} representing {@link Date} values.
	 */
	public static Column<Date> dateColumn( String aKey ) {
		return new DateColumn( aKey );
	}

	/**
	 * Constructs a {@link Column} with support for the provided
	 * {@link SimpleDateFormat} instances.
	 * 
	 * @param aKey The key to be used.
	 * @param aDateFormats The {@link SimpleDateFormat} instances used for
	 *        parsing date strings.
	 * 
	 * @return The according {@link Column} representing {@link Date} values.
	 */
	public static Column<Date> dateColumn( String aKey, DateTimeFormatter... aDateFormats ) {
		return new DateColumn( aKey, aDateFormats );
	}

	/**
	 * Constructs a {@link Column} with support for double values.
	 * 
	 * @param aKey The key to be used.
	 * 
	 * @return The according {@link Column} representing double values.
	 */
	public static Column<Double> doubleColumn( String aKey ) {
		return new DoubleColumn( aKey );
	}

	/**
	 * Constructs a {@link Column} with support for enumeration values.
	 *
	 * @param <T> the generic enumerations's type
	 * @param aKey The key to be used.
	 * @param aType The enumeration's type.
	 * 
	 * @return The according {@link Column} representing enumeration values.
	 */
	public static <T extends Enum<T>> Column<T> enumColumn( String aKey, Class<T> aType ) {
		return new EnumColumn<>( aKey, aType );
	}

	/**
	 * Constructs a {@link Column} with support for float values.
	 * 
	 * @param aKey The key to be used.
	 * 
	 * @return The according {@link Column} representing float values.
	 */
	public static Column<Float> floatColumn( String aKey ) {
		return new FloatColumn( aKey );
	}

	/**
	 * Creates a new {@link Header} instance from the given {@link Column}
	 * elements.
	 *
	 * @param aColumns The {@link Column} instances representing the
	 *        {@link Header}.
	 * 
	 * @return The accordingly created {@link Header}.
	 */
	public static Header<?> headerOf( Column<?>... aColumns ) {
		return new HeaderImpl<Object>( aColumns );
	}

	/**
	 * Instantiates a {@link Header} for managing {@link String} instances.
	 * 
	 * @param aHeader The elements representing the {@link Header}'s
	 *        {@link Column} instances.
	 * 
	 * @return The accordingly created {@link Header}.
	 */
	public static Header<String> headerOf( String... aHeader ) {
		return new StringHeader( aHeader );
	}

	/**
	 * Constructs a {@link Column} with support for boolean values.
	 * 
	 * @param aKey The key to be used.
	 * 
	 * @return The according {@link Column} representing boolean values.
	 */
	public static Column<Boolean> booleanColumn( String aKey ) {
		return new BooleanColumn( aKey );
	}

	/**
	 * Constructs a {@link Column} with support for integer values.
	 * 
	 * @param aKey The key to be used.
	 * 
	 * @return The according {@link Column} representing integer values.
	 */
	public static Column<Integer> intColumn( String aKey ) {
		return new IntColumn( aKey );
	}

	/**
	 * Constructs a {@link Column} with support for long values.
	 * 
	 * @param aKey The key to be used.
	 * 
	 * @return The according {@link Column} representing long values.
	 */
	public static Column<Long> longColumn( String aKey ) {
		return new LongColumn( aKey );
	}

	/**
	 * Constructs a {@link Column} with support for string values.
	 * 
	 * @param aKey The key to be used.
	 * 
	 * @return The according {@link Column} representing string values.
	 */
	public static Column<String> stringColumn( String aKey ) {
		return new StringColumn( aKey );
	}
}
