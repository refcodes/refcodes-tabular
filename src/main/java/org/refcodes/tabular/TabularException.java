// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import org.refcodes.exception.AbstractException;
import org.refcodes.mixin.KeyAccessor;
import org.refcodes.mixin.ValueAccessor;

/**
 * Base exception for this artifact.
 */
public abstract class TabularException extends AbstractException {

	private static final long serialVersionUID = 1L;

	/**
	 * {@inheritDoc}
	 */
	public TabularException( String aMessage, String aErrorCode ) {
		super( aMessage, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public TabularException( String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public TabularException( String aMessage, Throwable aCause ) {
		super( aMessage, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public TabularException( String aMessage ) {
		super( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	public TabularException( Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public TabularException( Throwable aCause ) {
		super( aCause );
	}

	/**
	 * Base exception with a {@link Column}.
	 */
	@SuppressWarnings("rawtypes")
	protected abstract static class ColumnException extends TabularException implements ColumnAccessor {

		private static final long serialVersionUID = 1L;

		protected Column<?> _column;

		/**
		 * {@inheritDoc}
		 * 
		 * @param aColumn The column involved in this exception.
		 */
		public ColumnException( String aMessage, Column<?> aColumn, String aErrorCode ) {
			super( aMessage, aErrorCode );
			_column = aColumn;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aColumn The column involved in this exception.
		 */
		public ColumnException( String aMessage, Column<?> aColumn, Throwable aCause, String aErrorCode ) {
			super( aMessage, aCause, aErrorCode );
			_column = aColumn;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aColumn The column involved in this exception.
		 */
		public ColumnException( String aMessage, Column<?> aColumn, Throwable aCause ) {
			super( aMessage, aCause );
			_column = aColumn;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aColumn The column involved in this exception.
		 */
		public ColumnException( String aMessage, Column<?> aColumn ) {
			super( aMessage );
			_column = aColumn;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aColumn The column involved in this exception.
		 */
		public ColumnException( Column<?> aColumn, Throwable aCause, String aErrorCode ) {
			super( aCause, aErrorCode );
			_column = aColumn;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aColumn The column involved in this exception.
		 */
		public ColumnException( Column<?> aColumn, Throwable aCause ) {
			super( aCause );
			_column = aColumn;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Column<?> getColumn() {
			return _column;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Object[] getPatternArguments() {
			return new Object[] { _column };
		}

		/**
		 * Base exception with a {@link Column} and a related value.
		 */
		protected abstract static class ColumnValueException extends ColumnException implements ValueAccessor<Object> {

			private static final long serialVersionUID = 1L;

			protected Object _value;

			/**
			 * {@inheritDoc}
			 * 
			 * @param aValue The according value.
			 */
			public ColumnValueException( String aMessage, Column<?> aColumn, Object aValue, String aErrorCode ) {
				super( aMessage, aColumn, aErrorCode );
				_value = aValue;
			}

			/**
			 * {@inheritDoc}
			 * 
			 * @param aValue The according value.
			 */
			public ColumnValueException( String aMessage, Column<?> aColumn, Object aValue, Throwable aCause, String aErrorCode ) {
				super( aMessage, aColumn, aCause, aErrorCode );
				_value = aValue;
			}

			/**
			 * {@inheritDoc}
			 * 
			 * @param aValue The according value.
			 */
			public ColumnValueException( String aMessage, Column<?> aColumn, Object aValue, Throwable aCause ) {
				super( aMessage, aColumn, aCause );
				_value = aValue;
			}

			/**
			 * {@inheritDoc}
			 * 
			 * @param aValue The according value.
			 */
			public ColumnValueException( String aMessage, Column<?> aColumn, Object aValue ) {
				super( aMessage, aColumn );
				_value = aValue;
			}

			/**
			 * {@inheritDoc}
			 *
			 * @param aValue The according value.
			 */
			public ColumnValueException( Column<?> aColumn, Object aValue, Throwable aCause, String aErrorCode ) {
				super( aColumn, aCause, aErrorCode );
				_value = aValue;
			}

			/**
			 * {@inheritDoc}
			 *
			 * @param aValue The according value.
			 */
			public ColumnValueException( Column<?> aColumn, Object aValue, Throwable aCause ) {
				super( aColumn, aCause );
				_value = aValue;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public Object getValue() {
				return _value;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public Object[] getPatternArguments() {
				return new Object[] { _column, _value };
			}
		}
	}

	/**
	 * Base exception with a {@link Header}.
	 */
	@SuppressWarnings("rawtypes")
	protected abstract static class HeaderException extends TabularException implements HeaderAccessor {

		private static final long serialVersionUID = 1L;

		protected Header _header;

		/**
		 * {@inheritDoc}
		 * 
		 * @param aHeader The header involved in this exception.
		 */
		public HeaderException( String aMessage, Header aHeader, String aErrorCode ) {
			super( aMessage, aErrorCode );
			_header = aHeader;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aHeader The header involved in this exception.
		 */
		public HeaderException( String aMessage, Header aHeader, Throwable aCause, String aErrorCode ) {
			super( aMessage, aCause, aErrorCode );
			_header = aHeader;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aHeader The header involved in this exception.
		 */
		public HeaderException( String aMessage, Header aHeader, Throwable aCause ) {
			super( aMessage, aCause );
			_header = aHeader;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aHeader The header involved in this exception.
		 */
		public HeaderException( String aMessage, Header aHeader ) {
			super( aMessage );
			_header = aHeader;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aHeader The header involved in this exception.
		 */
		public HeaderException( Header aHeader, Throwable aCause, String aErrorCode ) {
			super( aCause, aErrorCode );
			_header = aHeader;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aHeader The header involved in this exception.
		 */
		public HeaderException( Header aHeader, Throwable aCause ) {
			super( aCause );
			_header = aHeader;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Header getHeader() {
			return _header;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Object[] getPatternArguments() {
			return new Object[] { _header };
		}
	}

	/**
	 * Base exception with a key (name).
	 */
	protected abstract static class KeyException extends TabularException implements KeyAccessor<String> {

		private static final long serialVersionUID = 1L;

		protected String _key;

		/**
		 * {@inheritDoc}
		 * 
		 * @param aKey The key involved in this exception.
		 */
		public KeyException( String aMessage, String aKey, String aErrorCode ) {
			super( aMessage, aErrorCode );
			_key = aKey;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aKey The key involved in this exception.
		 */
		public KeyException( String aMessage, String aKey, Throwable aCause, String aErrorCode ) {
			super( aMessage, aCause, aErrorCode );
			_key = aKey;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aKey The key involved in this exception.
		 */
		public KeyException( String aMessage, String aKey, Throwable aCause ) {
			super( aMessage, aCause );
			_key = aKey;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aKey The key involved in this exception.
		 */
		public KeyException( String aMessage, String aKey ) {
			super( aMessage );
			_key = aKey;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aKey The key involved in this exception.
		 */
		public KeyException( String aKey, Throwable aCause, String aErrorCode ) {
			super( aCause, aErrorCode );
			_key = aKey;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aKey The key involved in this exception.
		 */
		public KeyException( String aKey, Throwable aCause ) {
			super( aCause );
			_key = aKey;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String getKey() {
			return _key;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Object[] getPatternArguments() {
			return new Object[] { _key };
		}
	}
}
