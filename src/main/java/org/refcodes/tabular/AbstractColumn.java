// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import java.text.ParseException;
import java.util.List;

import org.refcodes.data.Delimiter;
import org.refcodes.textual.CsvBuilder;
import org.refcodes.textual.CsvEscapeMode;

/**
 * The {@link AbstractColumn} is the default implementation for the
 * {@link Column} interface. It implements amongst others the
 * {@link #toStorageString(Object)} and {@link #fromStorageString(String)}
 * methods leaving the developer to implement in sub-classes only the two
 * methods {@link #toStorageStrings(Object)} as well as
 * {@link #fromStorageStrings(String[])} which are to do the actual conversions.
 * <p>
 * Special care is taken when the type T is an array type:
 * <p>
 * The method {@link #toStorageString(Object)} returns a {@link String} with
 * separated values (in terms of CSV) in case the type T is an array type. The
 * delimiter of the values in this {@link String} then is the
 * {@link Delimiter#ARRAY} character as we use the {@link Delimiter#CSV} when
 * embedding the returned {@link String} in a CSV list (file).
 * <p>
 * The method {@link #fromStorageString(String)} interprets the passed
 * {@link String} as separated values (in terms of CSV) in case the type T is an
 * array type. It creates a {@link String} array from the {@link String} value;
 * the delimiter of the values in the {@link String} used is the
 * {@link Delimiter#ARRAY} character as we use the {@link Delimiter#CSV} when
 * parsing a {@link String} in a CSV list (file).
 *
 * @param <T> The type managed by the column.
 */
public abstract class AbstractColumn<T> implements Column<T>, Cloneable {

	private final Class<T> _type;

	private final String _key;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a column.
	 * 
	 * @param aKey The key describing the column.
	 * @param aType The type describing the column.
	 */
	public AbstractColumn( String aKey, Class<T> aType ) {
		_key = aKey;
		_type = aType;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toStorageString( T aValue ) {
		final String[] theStorageStrings = toStorageStrings( aValue );
		if ( theStorageStrings == null || theStorageStrings.length == 0 ) {
			return null;
		}
		else if ( theStorageStrings.length == 1 && !getType().isArray() ) {
			return theStorageStrings[0];
		}
		else if ( getType().isArray() ) {
			return new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( toStorageStrings( aValue ) ).withDelimiter( Delimiter.ARRAY.getChar() ).toString();
		}
		throw new IllegalArgumentException( "The type <" + getType().getName() + "> is not an array type though the number of elements in the provided string array is <" + ( (Object[]) aValue ).length + "> whereas only one element is being expected." );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T fromStorageString( String aStringValue ) throws ParseException {
		if ( !getType().isArray() ) {
			return fromStorageStrings( new String[] { aStringValue } );
		}
		else {
			final List<String> theFromSeparatedValues = new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withRecord( aStringValue ).withDelimiter( Delimiter.ARRAY.getChar() ).toFields();
			return fromStorageStrings( theFromSeparatedValues.toArray( new String[theFromSeparatedValues.size()] ) );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toPrintable( T aValue ) {
		if ( aValue.getClass().isArray() ) {
			return new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( (Object[]) aValue ).withDelimiter( Delimiter.ARRAY.getChar() ).toString();
		}
		return aValue.toString();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean contains( Record<?> aRecord ) {
		if ( aRecord.containsKey( getKey() ) ) {
			final Object eValue = aRecord.get( getKey() );
			if ( eValue != null ) {
				return ( getType().isAssignableFrom( eValue.getClass() ) );
			}
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public T get( Record<?> aRecord ) throws ColumnMismatchException {
		final Object eValue;
		if ( aRecord.containsKey( getKey() ) ) {
			eValue = aRecord.get( getKey() );
			if ( eValue != null ) {
				if ( getType().isAssignableFrom( eValue.getClass() ) ) {
					return (T) eValue;
				}
				else {
					throw new ColumnMismatchException( "The type <" + getType().getName() + "> for column with key \"" + getKey() + "\" does not match the value's type <" + eValue.getClass().getName() + ">!", this, eValue );
				}
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public T remove( Record<?> aRecord ) throws ColumnMismatchException {
		final Object eValue;
		if ( aRecord.containsKey( getKey() ) ) {
			eValue = aRecord.get( getKey() );
			if ( eValue != null ) {
				if ( getType().isAssignableFrom( eValue.getClass() ) ) {
					aRecord.remove( getKey() );
					return (T) eValue;
				}
				else {
					throw new ColumnMismatchException( "The type <" + getType().getName() + "> for column with key \"" + getKey() + "\" does not match the value's type <" + eValue.getClass().getName() + ">!", this, eValue );
				}
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<T> getType() {
		return _type;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getKey() {
		return _key;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
