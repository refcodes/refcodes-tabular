// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.refcodes.data.Delimiter;
import org.refcodes.time.DateFormats;

/**
 * Utility for listing specific functionality.
 */
public final class TabularUtility {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Private empty constructor to prevent instantiation as of being a utility
	 * with just static public methods.
	 */
	private TabularUtility() {}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a header for a given string array with the column names to be
	 * used for the header. The data type of the header columns will be
	 * {@link Object}.
	 *
	 * @param <T> the generic type
	 * @param aColumnKeys The column names to be use for the header.
	 * @param aColumnFactory creates column instances depending on the key
	 *        passed. This is useful when https://www.metacodes.pro header keys
	 *        are to have a dedicated type such as {@link java.util.Date}. If
	 *        null is passed then just {@link ObjectColumn} instances for type
	 *        Object.class are created.
	 * 
	 * @return A header constructed from the given column names.
	 */
	public static <T> Header<T> toHeader( String[] aColumnKeys, ColumnFactory<T> aColumnFactory ) {
		return toHeader( Arrays.asList( aColumnKeys ), aColumnFactory );
	}

	/**
	 * Creates a header for a given list of strings containing the column names
	 * to be used for the header. The data type of the header columns will be
	 * {@link Object}.
	 *
	 * @param <T> the generic type
	 * @param aColumnKeys The column names to be use for the header.
	 * @param aColumnFactory creates column instances depending on the key
	 *        passed. This is useful when https://www.metacodes.pro header keys
	 *        are to have a dedicated type such as {@link java.util.Date}. If
	 *        null is passed then just {@link ObjectColumn} instances for type
	 *        {@link Object} are created.
	 * 
	 * @return A header constructed from the given column names.
	 */
	@SuppressWarnings("unchecked")
	public static <T> Header<T> toHeader( Collection<String> aColumnKeys, ColumnFactory<T> aColumnFactory ) {
		final Column<? extends T>[] theColumns = new Column[aColumnKeys.size()];
		final Iterator<String> e = aColumnKeys.iterator();
		int i = 0;
		while ( e.hasNext() ) {
			theColumns[i] = aColumnFactory.create( e.next() );
			i++;
		}
		return new HeaderImpl<>( theColumns );
	}

	// /////////////////////////////////////////////////////////////////////////
	// STRING:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates the string representation from from the given record. The key /
	 * value pairs of each item in the record are separated from each other with
	 * the assignment operator "=" and the items are separated from each other
	 * by the default CSV separator character and date objects are formatted
	 * with the default date format.
	 * 
	 * @param aRecord The record for which to create a string.
	 * 
	 * @return A string representation of the given record.
	 */
	public static String toString( Record<?> aRecord ) {
		return toString( aRecord, Delimiter.CSV.getChar(), DateFormats.DEFAULT_DATE_FORMATS.getDateFormats() );
	}

	/**
	 * Creates the string representation from from the given record. The key /
	 * value pairs of each item in the record are separated from each other with
	 * the assignment operator "=" and the items are separated from each other
	 * by the given separator character.
	 * 
	 * @param aRecord The record for which to create a string.
	 * @param aSeparator The separator to separate the items (key/value-pairs)
	 *        of the record from each other.
	 * @param aDateFormats The date formats to use when formatting date objects.
	 * 
	 * @return A string representation of the given record.
	 */
	public static String toString( Record<?> aRecord, char aSeparator, DateTimeFormatter[] aDateFormats ) {
		final StringBuilder theBuffer = new StringBuilder();
		Object eValue;
		String eKey;
		final Iterator<String> e = aRecord.keySet().iterator();
		while ( e.hasNext() ) {
			eKey = e.next();
			theBuffer.append( eKey );
			theBuffer.append( '=' );
			eValue = aRecord.get( eKey );
			if ( eValue instanceof Date ) {
				final Instant theInstant = Instant.ofEpochMilli( ( (Date) eValue ).getTime() );
				theBuffer.append( aDateFormats[0].format( theInstant ) );
			}
			else if ( eValue != null ) {
				theBuffer.append( toString( eValue ) );
			}
			if ( e.hasNext() ) {
				theBuffer.append( aSeparator );
			}
		}
		return theBuffer.toString();
	}

	// /////////////////////////////////////////////////////////////////////////
	// SEPARATED VALUES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns a separated values representation of the implementing collection
	 * by separating each item with the default separator {@link Delimiter#CSV}.
	 * The common CSV conventions are to be obeyed (although there is none CSV
	 * standard). In case a value's string representation contains the delimiter
	 * char, then this char must be escaped (i.e. by using the backslash '\').
	 * 
	 * @param aRecord The record from which to generate separated values.
	 * 
	 * @return The aDelimiter separated string.
	 */
	public static String toSeparatedValues( Record<?> aRecord ) {
		return toSeparatedValues( aRecord, Delimiter.CSV.getChar() );
	}

	/**
	 * Returns a separated values representation of the implementing collection
	 * by separating each item with the given separator. The common CSV
	 * conventions are to be obeyed (although there is none CSV standard). In
	 * case a value's string representation contains the delimiter char, then
	 * this char must be escaped (i.e. by using the backslash '\').
	 * 
	 * @param aRecord The record from which to generate separated values.
	 * @param aDelimiter The delimiter to use when separating the values.
	 * 
	 * @return The aDelimiter separated string.
	 */
	public static String toSeparatedValues( Record<?> aRecord, char aDelimiter ) {
		final StringBuilder theBuffer = new StringBuilder();
		String eKey;
		Object eValue;
		final Iterator<String> e = aRecord.keySet().iterator();
		while ( e.hasNext() ) {
			eKey = e.next();
			theBuffer.append( eKey );
			theBuffer.append( '=' );
			eValue = aRecord.get( eKey );
			if ( eValue != null ) {
				theBuffer.append( toString( eValue ) );
			}
			if ( e.hasNext() ) {
				theBuffer.append( aDelimiter );
			}
		}
		return theBuffer.toString();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Converts a value or an array to a string or string array without having a
	 * {@link Column} available.
	 * 
	 * @param aValue The value to convert to a string or a string array.
	 * 
	 * @return A plain string or a string array depending on the type of the
	 *         given value.
	 */
	private static Object toString( Object aValue ) {
		if ( aValue instanceof Object[] ) {
			final Object[] theValues = (Object[]) aValue;
			final String[] theResult = new String[theValues.length];
			for ( int i = 0; i < theValues.length; i++ ) {
				theResult[i] = theValues[i].toString();
			}
			return theResult;
		}
		else {
			return aValue.toString();
		}
	}
}
