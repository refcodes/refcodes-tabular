// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import java.text.ParseException;

import org.refcodes.mixin.KeyAccessor;
import org.refcodes.mixin.TypeAccessor;

/**
 * Similar to a {@link Column} definition for a DB or a spread sheet (or, with
 * reservations, a CSV file), a {@link Column} describes properties such as name
 * (key) and type of elements related to that {@link Column} (e.g. the according
 * elements of the {@link Row} lines).
 *
 * @param <T> The type managed by the {@link Column}.
 */
public interface Column<T> extends KeyAccessor<String>, TypeAccessor<T> {

	/**
	 * A {@link Column} implementation can provide its own text exchange format
	 * for the given objects. This method enables the {@link Column} to convert
	 * a value of the given type to a {@link String} and via
	 * {@link #fromStorageString(String)} back to the value (bijective). This
	 * method supports data sinks (such as relational databases) which allow
	 * only a single value in a row's entry: In case T is an array type, then
	 * the storage {@link String} representation of the elements in that array
	 * are represented by a single returned {@link String}. Question: Why does
	 * the detour through the method {@link #toStorageString(Object)} not
	 * function?!? Having this snippet in the code seems to be fine for the
	 * compiler ... trying {@link #toStorageString_(Object)} instead :-( In case
	 * a data sink (such as Amazon's SimpleDb) is to be addressed which provides
	 * dedicated support for multiple values in one row's entry, then the method
	 * {@link #toStorageStrings(Object)} may be used instead.
	 * 
	 * @param aValue the element to be converted to a {@link String}.
	 * 
	 * @return The {@link String} representation of the value.
	 */
	String toStorageString( T aValue );

	/**
	 * A {@link Column} implementation can provide its own text exchange format
	 * for the given objects. This method enables the {@link Column} to convert
	 * a value of the given type to a {@link String} and via
	 * {@link #fromStorageString(String)} back to the value (bijective). This
	 * method supports data sinks (such as relational databases) which allow
	 * only a single value in a row's entry: In case T is an array type, then
	 * the storage {@link String} representation of the elements in that array
	 * are represented by a single returned {@link String}. In case a data sink
	 * (such as Amazon's SimpleDb) is to be addressed which provides dedicated
	 * support for multiple values in one row's entry, then the method
	 * {@link #toStorageStrings(Object)} may be used instead. Question: Why does
	 * the detour through the method {@link #toStorageString(Object)} not
	 * function?!? Having this snippet in the code seems to be fine for the
	 * compiler ... trying {@link #toStorageString_(Object)} instead :-(
	 * 
	 * @param aValue the element to be converted to a {@link String}.
	 * 
	 * @return The {@link String} representation of the value.
	 * 
	 * @throws ClassCastException - if the object is not null and is not
	 *         assignable to the type T.
	 */
	default String toStorageString_( Object aValue ) {
		final T theValue = getType().cast( aValue );
		return toStorageString( theValue );
	}

	/**
	 * A {@link Column} implementation can provide its own text exchange format
	 * for the given objects. This method enables the {@link Column} to convert
	 * a value of the given type to a {@link String} array and via
	 * {@link #fromStorageStrings(String[])} back to the value (bijective). This
	 * method supports data sinks (such as Amazon's SimpleDb) which provide
	 * dedicated support for multiple values in a row's entry: In case T is an
	 * array type, then the storage {@link String} representations of the
	 * elements in that array may be placed in dedicated entries of the returned
	 * {@link String} array. In case T is not an array type then the returned
	 * {@link String} array may contain just one value. In case data sinks (such
	 * as relational databases) are to be addressed which allow only a single
	 * value in a row's entry, then the method {@link #toStorageString(Object)}
	 * may be used instead.
	 * 
	 * @param aValue the element to be converted to a {@link String} array.
	 * 
	 * @return The {@link String} array representation of the value.
	 */
	String[] toStorageStrings( T aValue );

	/**
	 * A {@link Column} implementation can provide its own text exchange format
	 * for the given objects. This method enables the {@link Column} to convert
	 * a {@link String} value to a value of the given type and via
	 * {@link #toStorageString(Object)} back to the {@link String} (bijective).
	 * This method supports data sinks (such as relational databases) which
	 * allow only a single value in a row's entry: In case T is an array type,
	 * then the storage {@link String} representation of the elements in that
	 * array are represented by the single passed {@link String}. In case a data
	 * sink (such as Amazon's SimpleDb) is to be addressed which provides
	 * dedicated support for multiple values in one row's entry, then the method
	 * {@link #fromStorageStrings(String[])} may be used instead.
	 * 
	 * @param aStringValue The value to be converted to a type instance.
	 * 
	 * @return The type representation of the value.
	 * 
	 * @throws ParseException in case parsing the {@link String} was not
	 *         possible
	 */
	T fromStorageString( String aStringValue ) throws ParseException;

	/**
	 * A {@link Column} implementation can provide its own text exchange format
	 * for the given objects. This method enables the {@link Column} to convert
	 * a {@link String} array value to a value of the given type and via
	 * {@link #toStorageStrings(Object)} back to the {@link String} array
	 * (bijective). This method supports data sinks (such as Amazon's SimpleDb)
	 * which provide dedicated support for multiple values in a row's entry: In
	 * case T is an array type, then the storage {@link String} representations
	 * of the elements in that array may be placed in dedicated entries of the
	 * provided {@link String} array. In case T is not an array type then the
	 * passed {@link String} array may contain just one value. In case data
	 * sinks (such as relational databases) are to be addressed which allow only
	 * a single value in a row's entry, then the method
	 * {@link #fromStorageString(String)} may be used instead.
	 * 
	 * @param aStringArray The value to be converted to a type instance.
	 * 
	 * @return The type representation of the value.
	 * 
	 * @throws ParseException in case parsing the {@link String} was not
	 *         possible
	 */
	T fromStorageStrings( String[] aStringArray ) throws ParseException;

	/**
	 * A {@link Column} implementation can provide its own printable format of
	 * the given objects; for example a human readable text representation of
	 * the value (or in very https://www.metacodes.proized cases even enriched
	 * with ANSI escape codes). This method enables the {@link Column} to
	 * convert a value of the given type to a human readable text. The human
	 * readable text, in comparison to the method {@link Object#toString()} (or
	 * {@link #toStorageString(Object)}) is not intended to be converted back to
	 * the actual value (not bijective). This method may be used a
	 * {@link Header} instance's method {@link Header#toPrintable(Record)}.
	 * 
	 * @param aValue the element to be converted to a human readable text.
	 * 
	 * @return The human readable representation of the value.
	 */
	String toPrintable( T aValue );

	/**
	 * Tests whether the {@link Record} contains a value identified by the
	 * {@link Column} instance's key and where the value's type is assignable to
	 * the {@link Column} instance's type. Only if them both criteria match,
	 * then true is returned. False is returned if there is no such value or the
	 * value is not assignable to the {@link Column} instance's type.
	 * 
	 * @param aRecord The {@link Record} which to test if there is a value
	 *        associated to the {@link Column} instance's key and if it can be
	 *        casted to the {@link Column} instance's type.
	 * 
	 * @return True in case the {@link Record} contains a value identified by
	 *         the {@link Column} instance's key and where the value's type is
	 *         assignable to the {@link Column} instance's type.
	 */
	boolean contains( Record<?> aRecord );

	/**
	 * Retrieves a type correct value from the {@link Record} identified by the
	 * {@link Column} instance's key. In case the type of the {@link Column}
	 * instance does not match the value's type associated with the
	 * {@link Column} instance's key, then a {@link ColumnMismatchException} is
	 * thrown.
	 * 
	 * @param aRecord The {@link Record} from which to retrieve the value
	 *        associated to the {@link Column} instances key.
	 * 
	 * @return The value from the {@link Record} associated to the
	 *         {@link Column} instances key.
	 * 
	 * @throws ColumnMismatchException in case the value in the {@link Record}
	 *         associated with the {@link Column} instance's key does not fit
	 *         the {@link Column} instance's type.
	 */
	T get( Record<?> aRecord ) throws ColumnMismatchException;

	/**
	 * Removes an entry (key and value pair) from the provided {@link Record}
	 * matching the key and the type of the {@link Column}.
	 * 
	 * @param aRecord The {@link Record} from which to remove the related entry.
	 * 
	 * @return The value related to the given {@link Column} instance's key or
	 *         null if there was none such value found.
	 * 
	 * @throws ColumnMismatchException in case the value in the {@link Record}
	 *         associated with the {@link Column} instance's key does not fit
	 *         the {@link Column} instance's type.
	 */
	T remove( Record<?> aRecord ) throws ColumnMismatchException;

	/**
	 * Retrieves the corresponding value from the provided record. Question: Why
	 * does the detour through this method not function?!? :-(
	 * 
	 * @param aRecord The record from which to get the value.
	 * 
	 * @return The according value or null if there is none the such.
	 */
	//	default T getValue( Record<T> aRecord ) {
	//		return aRecord.get( getKey() );
	//	}

	/**
	 * Retrieves the corresponding value from the provided record.
	 * 
	 * @param aRecord The record from which to get the value.
	 * 
	 * @return The according value or null if there is none the such.
	 */
	@SuppressWarnings("unchecked")
	default T getValue( Record<?> aRecord ) {
		return (T) aRecord.get( getKey() );
	}
}
