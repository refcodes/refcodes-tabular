// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import java.awt.Component;
import java.io.IOException;
import java.util.Collection;

import org.refcodes.component.ComponentComposite;
import org.refcodes.component.ComponentUtility;
import org.refcodes.component.InitializeException;
import org.refcodes.component.PauseException;
import org.refcodes.component.ResumeException;
import org.refcodes.component.StartException;
import org.refcodes.component.StopException;
import org.refcodes.controlflow.ExecutionStrategy;
import org.refcodes.controlflow.InvocationStrategy;

/**
 * Implementation of the {@link Records} interface wrapping multiple
 * {@link Records} instances. The {@link RecordsComposite} makes use of a flavor
 * of the composite pattern.
 *
 * @param <T> The type managed by the {@link Records}.
 */
public class RecordsComposite<T> implements Records<T>, ComponentComposite {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private InvocationStrategy _recordServeStrategy;
	private ExecutionStrategy _componentExecutionStrategy;
	private Records<T>[] _records;
	private int _recordsIndex = 0;
	boolean _hasInitialNext = false;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link RecordsComposite} serving {@link Record} instances
	 * from multiple {@link Records} instances.
	 * 
	 * @param aRecordServeStrategy The strategy to be used for serving the
	 *        {@link Record} instances.
	 * @param aRecords The {@link Records} instances from which to serve the
	 *        {@link Record} instances.
	 */
	@SafeVarargs
	public RecordsComposite( InvocationStrategy aRecordServeStrategy, Records<T>... aRecords ) {
		this( ExecutionStrategy.JOIN, aRecordServeStrategy, aRecords );
	}

	/**
	 * Constructs a {@link RecordsComposite} serving {@link Record} instances
	 * from multiple {@link Records} instances.
	 * 
	 * @param aComponentExecutionStrategy The strategy on how to invoke the
	 *        state change requests (as of the {@link ComponentComposite})
	 *        defined methods) on the herein contained {@link Component}
	 *        instances. CAUTION: The strategy does not affect on how the
	 *        {@link #next()} methods of the herein contained instances are
	 *        invoked.
	 * @param aRecordServeStrategy The strategy to be used for serving the
	 *        {@link Record} instances.
	 * @param aRecords The {@link Records} instances from which to serve the
	 *        {@link Record} instances.
	 */
	@SafeVarargs
	public RecordsComposite( ExecutionStrategy aComponentExecutionStrategy, InvocationStrategy aRecordServeStrategy, Records<T>... aRecords ) {
		_recordServeStrategy = aRecordServeStrategy;
		_records = aRecords;
		if ( _recordServeStrategy == InvocationStrategy.ROUND_ROBIN ) {
			_recordsIndex = 0;
		}
		else if ( _recordServeStrategy == InvocationStrategy.FIRST_TO_LAST ) {
			_recordsIndex = 0;
		}
		else if ( _recordServeStrategy == InvocationStrategy.LAST_TO_FIRST ) {
			_recordsIndex = aRecords.length - 1;
		}
		_componentExecutionStrategy = aComponentExecutionStrategy;
	}

	/**
	 * Constructs a {@link RecordsComposite} serving {@link Record} instances
	 * from multiple {@link Records} instances.
	 * 
	 * @param aRecordServeStrategy The strategy to be used for serving the
	 *        {@link Record} instances.
	 * @param aRecords The {@link Records} instances from which to serve the
	 *        {@link Record} instances.
	 */
	public RecordsComposite( InvocationStrategy aRecordServeStrategy, Collection<Records<T>> aRecords ) {
		this( ExecutionStrategy.JOIN, aRecordServeStrategy, aRecords );
	}

	/**
	 * Constructs a {@link RecordsComposite} serving {@link Record} instances
	 * from multiple {@link Records} instances.
	 * 
	 * @param aComponentExecutionStrategy The strategy on how to invoke the
	 *        state change requests (as of the {@link ComponentComposite})
	 *        defined methods) on the herein contained {@link Component}
	 *        instances. CAUTION: The strategy does not affect on how the
	 *        {@link #next()} methods of the herein contained instances are
	 *        invoked.
	 * @param aRecordServeStrategy The strategy to be used for serving the
	 *        {@link Record} instances.
	 * @param aRecords The {@link Records} instances from which to serve the
	 *        {@link Record} instances.
	 */
	@SuppressWarnings("unchecked")
	public RecordsComposite( ExecutionStrategy aComponentExecutionStrategy, InvocationStrategy aRecordServeStrategy, Collection<Records<T>> aRecords ) {
		this( aComponentExecutionStrategy, aRecordServeStrategy, aRecords.toArray( new Records[aRecords.size()] ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// ITERATOR:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext() {
		for ( Records<T> eRecords : _records ) {
			if ( eRecords.hasNext() ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Record<T> next() {
		synchronized ( this ) {
			doPrepareNext();
			final Record<T> theRecord = _records[_recordsIndex].next();
			_hasInitialNext = true;
			return theRecord;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void remove() {
		synchronized ( this ) {
			if ( !_hasInitialNext ) {
				throw new IllegalStateException( "This method can only be called after at least initially calling the \"next()\" method once." );
			}
			_records[_recordsIndex].remove();
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Prepares the index of the next {@link Records} instance to be used.
	 */
	private void doPrepareNext() {
		int theIndex = _recordsIndex;
		while ( hasNext() ) {
			switch ( _recordServeStrategy ) {
			case ROUND_ROBIN -> {
				theIndex++;
				if ( theIndex >= _records.length ) {
					theIndex = 0;
				}
				if ( _records[theIndex].hasNext() ) {
					_recordsIndex = theIndex;
					return;
				}
			}
			case FIRST_TO_LAST -> {
				if ( _records[theIndex].hasNext() ) {
					_recordsIndex = theIndex;
					return;
				}
				if ( theIndex == _records.length - 1 ) {
					return;
				}
				theIndex++;
			}
			case LAST_TO_FIRST -> {
				if ( _records[theIndex].hasNext() ) {
					_recordsIndex = theIndex;
					return;
				}
				if ( theIndex == 0 ) {
					return;
				}
				theIndex--;
			}
			default -> {
			}
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// COMPONENT:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialize() throws InitializeException {
		ComponentUtility.initialize( _componentExecutionStrategy, (Object[]) _records );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void start() throws StartException {
		ComponentUtility.start( _componentExecutionStrategy, (Object[]) _records );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void pause() throws PauseException {
		ComponentUtility.pause( _componentExecutionStrategy, (Object[]) _records );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void resume() throws ResumeException {
		ComponentUtility.resume( _componentExecutionStrategy, (Object[]) _records );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void stop() throws StopException {
		ComponentUtility.stop( _componentExecutionStrategy, (Object[]) _records );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void decompose() {
		ComponentUtility.decompose( _componentExecutionStrategy, (Object[]) _records );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException {
		ComponentUtility.flush( _componentExecutionStrategy, (Object[]) _records );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
		ComponentUtility.destroy( _componentExecutionStrategy, (Object[]) _records );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		ComponentUtility.reset( _componentExecutionStrategy, (Object[]) _records );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open() throws IOException {
		ComponentUtility.open( _componentExecutionStrategy, (Object[]) _records );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() {
		ComponentUtility.close( _componentExecutionStrategy, (Object[]) _records );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dispose() {
		ComponentUtility.dispose( _componentExecutionStrategy, (Object[]) _records );
	}
}
