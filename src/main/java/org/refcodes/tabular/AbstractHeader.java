// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.NoSuchElementException;
import java.util.Set;

import org.refcodes.exception.BugException;

/**
 * Implementation of the {@link Header} interface being {@link Cloneable}.
 *
 * @param <T> The type managed by the {@link Header}.
 * @param <C> the generic type
 */
public abstract class AbstractHeader<T, C extends Column<? extends T>> extends ArrayList<C> implements HeaderRow<T, C> {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * A {@link LinkedHashSet} is used in order to preserve the order of the
	 * elements as contained in the {@link Header} itself.
	 */
	protected Set<String> _keys = new LinkedHashSet<>();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link AbstractHeader} instance.
	 */
	public AbstractHeader() {}

	/**
	 * Constructs the {@link AbstractHeader} instance configured with the
	 * provided {@link Column} instances.
	 * 
	 * @param aColumns The {@link Column} instances to be contained in the
	 *        {@link AbstractHeader} in the order as passed.
	 */
	@SafeVarargs
	public AbstractHeader( C... aColumns ) {
		int theIndex = 0;
		for ( C eColumn : aColumns ) {
			if ( containsKey( eColumn.getKey() ) ) {
				throw new IllegalArgumentException( "A column with key \"" + eColumn.getKey() + "\" at index <" + theIndex + "> already exists, cannot add a column with the same key twice." );
			}
			add( eColumn );
			theIndex++;
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsKey( Object aKey ) {
		for ( Column<? extends T> eColumn : this ) {
			if ( ( eColumn.getKey() == null && aKey == null ) || ( eColumn.getKey() != null && eColumn.getKey().equals( aKey ) ) ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public C get( Object aKey ) {
		for ( C eColumn : this ) {
			if ( eColumn.getKey().equals( aKey ) ) {
				return eColumn;
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<String> keySet() {
		if ( size() != _keys.size() ) {
			synchronized ( this ) {
				if ( size() != _keys.size() ) {
					_keys.clear();
					for ( C eColumn : this ) {
						_keys.add( eColumn.getKey() );
					}

				}
			}
		}
		return _keys;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int indexOf( String aKey ) {
		int theIndex = 0;
		for ( C eColumn : this ) {
			if ( eColumn.getKey().equals( aKey ) ) {
				return theIndex;
			}
			theIndex++;
		}
		return -1;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public C delete( String aKey ) {
		final int theIndex = indexOf( aKey );
		if ( theIndex == -1 ) {
			throw new NoSuchElementException( "No such element with key \"" + aKey + "\" found." );
		}
		final C theColumn = remove( theIndex );
		if ( theColumn == null ) {
			throw new NoSuchElementException( "No such element with key \"" + aKey + "\" found any more (thread race condition)." );
		}
		return theColumn;
	}

	/**
	 * To storage string.
	 *
	 * @param aRecord the record
	 * 
	 * @return the record
	 * 
	 * @throws HeaderMismatchException the header mismatch exception
	 * @throws ColumnMismatchException the column mismatch exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Record<String> toStorageString( Record<? extends T> aRecord ) throws HeaderMismatchException, ColumnMismatchException {
		// ---------------------------------------------------------------------
		// Test the preconditions:
		// ---------------------------------------------------------------------
		throwHeaderMismatchException( aRecord );
		// ---------------------------------------------------------------------
		final Record<String> theStringRecord = new RecordImpl<>();
		Object eValue;
		String eStorageString;
		for ( Column eColumn : this ) {
			eValue = eColumn.get( aRecord );
			eStorageString = null;
			if ( eValue != null ) {
				throwColumnMismatchException( eValue, eColumn );
				eStorageString = eColumn.toStorageString( eValue );
			}
			theStringRecord.put( (String) eColumn.getKey(), eStorageString );
		}
		return theStringRecord;
	}

	/**
	 * To storage string.
	 *
	 * @param aRow the row
	 * 
	 * @return the row
	 * 
	 * @throws HeaderMismatchException the header mismatch exception
	 * @throws ColumnMismatchException the column mismatch exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Row<String> toStorageString( Row<? extends T> aRow ) throws HeaderMismatchException, ColumnMismatchException {
		// ---------------------------------------------------------------------
		// Test the preconditions:
		// ---------------------------------------------------------------------
		throwHeaderMismatchException( aRow );
		// ---------------------------------------------------------------------
		final Row<String> theStringRow = new RowImpl<>();
		Object eValue;
		String eStorageString;
		Column eColumn;
		for ( int i = 0; i < this.size(); i++ ) {
			eColumn = this.get( i );
			eValue = aRow.get( i );
			eStorageString = null;
			if ( eValue != null ) {
				throwColumnMismatchException( eValue, eColumn );
				eStorageString = eColumn.toStorageString( eValue );
			}
			theStringRow.add( eStorageString );
		}
		return theStringRow;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Record<T> fromStorageString( Record<String> aStringRecord ) throws HeaderMismatchException, ParseException {
		// ---------------------------------------------------------------------
		// Test the preconditions:
		// ---------------------------------------------------------------------
		throwHeaderMismatchException( aStringRecord );
		// ---------------------------------------------------------------------
		final Record<T> theRecord = new RecordImpl<>();
		T eValue;
		String eStorageString;
		for ( C eColumn : this ) {
			eStorageString = aStringRecord.get( eColumn.getKey() );
			eValue = null;
			if ( eStorageString != null ) {
				eValue = eColumn.fromStorageString( eStorageString );
			}
			theRecord.put( eColumn.getKey(), eValue );
		}
		return theRecord;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row<T> fromStorageString( Row<String> aStringRow ) throws HeaderMismatchException, ParseException {
		// ---------------------------------------------------------------------
		// Test the preconditions:
		// ---------------------------------------------------------------------
		throwHeaderMismatchException( aStringRow );
		// ---------------------------------------------------------------------
		final Row<T> theRow = new RowImpl<>();
		T eValue;
		String eStorageString;
		C eColumn;
		for ( int i = 0; i < this.size(); i++ ) {
			eColumn = this.get( i );
			eStorageString = aStringRow.get( i );
			eValue = null;
			if ( eStorageString != null ) {
				eValue = eColumn.fromStorageString( eStorageString );
			}
			theRow.add( eValue );
		}
		return theRow;
	}

	/**
	 * To storage strings.
	 *
	 * @param aRecord the record
	 * 
	 * @return the record
	 * 
	 * @throws HeaderMismatchException the header mismatch exception
	 * @throws ColumnMismatchException the column mismatch exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Record<String[]> toStorageStrings( Record<? extends T> aRecord ) throws HeaderMismatchException, ColumnMismatchException {
		// ---------------------------------------------------------------------
		// Test the preconditions:
		// ---------------------------------------------------------------------
		throwHeaderMismatchException( aRecord );
		// ---------------------------------------------------------------------
		final Record<String[]> theStringsRecord = new RecordImpl<>();
		Object eValue;
		String[] eStorageStrings;
		for ( Column eColumn : this ) {
			eValue = eColumn.get( aRecord );
			eStorageStrings = null;
			if ( eValue != null ) {
				throwColumnMismatchException( eValue, eColumn );
				eStorageStrings = eColumn.toStorageStrings( eValue );
			}
			theStringsRecord.put( (String) eColumn.getKey(), eStorageStrings );
		}
		return theStringsRecord;
	}

	/**
	 * To storage strings.
	 *
	 * @param aRow the row
	 * 
	 * @return the row
	 * 
	 * @throws HeaderMismatchException the header mismatch exception
	 * @throws ColumnMismatchException the column mismatch exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Row<String[]> toStorageStrings( Row<? extends T> aRow ) throws HeaderMismatchException, ColumnMismatchException {
		// ---------------------------------------------------------------------
		// Test the preconditions:
		// ---------------------------------------------------------------------
		throwHeaderMismatchException( aRow );
		// ---------------------------------------------------------------------
		final Row<String[]> theStringsRow = new RowImpl<>();
		Object eValue;
		String[] eStorageStrings;
		Column eColumn;
		for ( int i = 0; i < this.size(); i++ ) {
			eColumn = this.get( i );
			eValue = aRow.get( i );
			eStorageStrings = null;
			if ( eValue != null ) {
				throwColumnMismatchException( eValue, eColumn );
				eStorageStrings = eColumn.toStorageStrings( eValue );
			}
			theStringsRow.add( eStorageStrings );
		}
		return theStringsRow;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row<T> fromStorageStrings( Row<String[]> aStringsRow ) throws HeaderMismatchException, ParseException {
		// ---------------------------------------------------------------------
		// Test the preconditions:
		// ---------------------------------------------------------------------
		throwHeaderMismatchException( aStringsRow );
		// ---------------------------------------------------------------------
		final Row<T> theRow = new RowImpl<>();
		T eValue;
		String[] eStorageStrings;
		C eColumn;
		for ( int i = 0; i < this.size(); i++ ) {
			eColumn = this.get( i );
			eStorageStrings = aStringsRow.get( i );
			eValue = null;
			if ( eStorageStrings != null ) {
				eValue = eColumn.fromStorageStrings( eStorageStrings );
			}
			theRow.add( eValue );
		}
		return theRow;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Record<T> fromStorageStrings( Record<String[]> aStringsRecord ) throws HeaderMismatchException, ParseException {
		// ---------------------------------------------------------------------
		// Test the preconditions:
		// ---------------------------------------------------------------------
		throwHeaderMismatchException( aStringsRecord );
		// ---------------------------------------------------------------------
		final Record<T> theRecord = new RecordImpl<>();
		T eValue;
		String[] eStorageStrings;
		for ( C eColumn : this ) {
			eStorageStrings = aStringsRecord.get( eColumn.getKey() );
			eValue = null;
			if ( eStorageStrings != null ) {
				eValue = eColumn.fromStorageStrings( eStorageStrings );
			}
			theRecord.put( eColumn.getKey(), eValue );
		}
		return theRecord;
	}

	/**
	 * To printable.
	 *
	 * @param aRecord the record
	 * 
	 * @return the record
	 * 
	 * @throws HeaderMismatchException the header mismatch exception
	 * @throws ColumnMismatchException the column mismatch exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Record<String> toPrintable( Record<? extends T> aRecord ) throws HeaderMismatchException, ColumnMismatchException {
		// ---------------------------------------------------------------------
		// Test the preconditions:
		// ---------------------------------------------------------------------
		throwHeaderMismatchException( aRecord );
		// ---------------------------------------------------------------------
		final Record<String> theRecord = new RecordImpl<>();
		Object eValue;
		String ePrintableString;
		for ( Column eColumn : this ) {
			eValue = eColumn.get( aRecord );
			ePrintableString = null;
			if ( eValue != null ) {
				throwColumnMismatchException( eValue, eColumn );
				ePrintableString = eColumn.toPrintable( eValue );
			}
			theRecord.put( (String) eColumn.getKey(), ePrintableString );
		}
		return theRecord;
	}

	/**
	 * To printable.
	 *
	 * @param aRow the row
	 * 
	 * @return the row
	 * 
	 * @throws HeaderMismatchException the header mismatch exception
	 * @throws ColumnMismatchException the column mismatch exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Row<String> toPrintable( Row<? extends T> aRow ) throws HeaderMismatchException, ColumnMismatchException {
		// ---------------------------------------------------------------------
		// Test the preconditions:
		// ---------------------------------------------------------------------
		throwHeaderMismatchException( aRow );
		// ---------------------------------------------------------------------
		final Row<String> theRow = new RowImpl<>();
		Object eValue;
		String ePrintableString;
		Column eColumn;
		for ( int i = 0; i < this.size(); i++ ) {
			eColumn = this.get( i );
			eValue = aRow.get( i );
			ePrintableString = null;
			if ( eValue != null ) {
				throwColumnMismatchException( eValue, eColumn );
				ePrintableString = eColumn.toPrintable( eValue );
			}
			theRow.add( ePrintableString );
		}
		return theRow;
	}

	/**
	 * To row.
	 *
	 * @param aRecord the record
	 * 
	 * @return the row
	 * 
	 * @throws HeaderMismatchException the header mismatch exception
	 * @throws ColumnMismatchException the column mismatch exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Row<T> toRow( Record<? extends T> aRecord ) throws HeaderMismatchException, ColumnMismatchException {
		return (Row<T>) toRow( aRecord, false );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row<?> toRowIgnoreType( Record<?> aRecord ) throws HeaderMismatchException {
		try {
			return toRow( aRecord, true );
		}
		catch ( ColumnMismatchException aException ) {
			throw new BugException( "This exception must not happen; this method's implementnation <" + getClass().getName() + "> is errornous, please check the code.", aException );
		}
	}

	/**
	 * To record.
	 *
	 * @param aRow the row
	 * 
	 * @return the record
	 * 
	 * @throws HeaderMismatchException the header mismatch exception
	 * @throws ColumnMismatchException the column mismatch exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Record<T> toRecord( Row<? extends T> aRow ) throws HeaderMismatchException, ColumnMismatchException {
		return (Record<T>) toRecord( aRow, false );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Record<?> toRecordIgnoreType( Row<?> aRow ) throws HeaderMismatchException {
		try {
			return toRecord( aRow, true );
		}
		catch ( ColumnMismatchException aException ) {
			throw new BugException( "This exception must not happen; this method's implementnation <" + getClass().getName() + "> is errornous, please check the code.", aException );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row<T> fromStorageStringRecord( Record<String> aStringRecord ) throws HeaderMismatchException, ParseException {
		try {
			return toRow( fromStorageString( aStringRecord ) );
		}
		catch ( ColumnMismatchException aException ) {
			throw new BugException( "This exception must not happen; this method's implementnation <" + getClass().getName() + "> is errornous, please check the code.", aException );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Record<String> toStorageStringRecord( Row<? extends T> aRow ) throws HeaderMismatchException, ColumnMismatchException {
		return (Record<String>) toRecordIgnoreType( toStorageString( aRow ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Record<T> fromStorageStringRow( Row<String> aStringRow ) throws HeaderMismatchException, ParseException {
		try {
			return toRecord( fromStorageString( aStringRow ) );
		}
		catch ( ColumnMismatchException aException ) {
			throw new BugException( "This exception must not happen; this method's implementnation <" + getClass().getName() + "> is errornous, please check the code.", aException );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Row<String> toStorageStringRow( Record<? extends T> aRecord ) throws HeaderMismatchException, ColumnMismatchException {
		return (Row<String>) toRowIgnoreType( toStorageString( aRecord ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row<T> fromStorageStringsRecord( Record<String[]> aStringsRecord ) throws HeaderMismatchException, ParseException {
		try {
			return toRow( fromStorageStrings( aStringsRecord ) );
		}
		catch ( ColumnMismatchException aException ) {
			throw new BugException( "This exception must not happen; this method's implementnation <" + getClass().getName() + "> is errornous, please check the code.", aException );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Record<String[]> toStorageStringsRecord( Row<? extends T> aRow ) throws HeaderMismatchException, ColumnMismatchException {
		return (Record<String[]>) toRecordIgnoreType( toStorageStrings( aRow ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Record<T> fromStorageStringsRow( Row<String[]> aStringsRow ) throws HeaderMismatchException, ParseException {
		try {
			return toRecord( fromStorageStrings( aStringsRow ) );
		}
		catch ( ColumnMismatchException aException ) {
			throw new BugException( "This exception must not happen; this method's implementation <" + getClass().getName() + "> is errornous, please check the code.", aException );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Row<String[]> toStorageStringsRow( Record<? extends T> aRecord ) throws HeaderMismatchException, ColumnMismatchException {
		return (Row<String[]>) toRowIgnoreType( toStorageStrings( aRecord ) );
	}

	/**
	 * To printable row.
	 *
	 * @param aRecord the record
	 * 
	 * @return the row
	 * 
	 * @throws HeaderMismatchException the header mismatch exception
	 * @throws ColumnMismatchException the column mismatch exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Row<String> toPrintableRow( Record<? extends T> aRecord ) throws HeaderMismatchException, ColumnMismatchException {
		return (Row<String>) toRowIgnoreType( toPrintable( aRecord ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Record<String> toPrintableRecord( Row<? extends T> aRow ) throws HeaderMismatchException, ColumnMismatchException {
		return (Record<String>) toRecordIgnoreType( toPrintable( aRow ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean add( C aColumn ) {
		if ( containsKey( aColumn.getKey() ) ) {
			throw new IllegalArgumentException( "A column with key \"" + aColumn.getKey() + "\" already exists, cannot add a column with the same key twice." );
		}
		_keys.clear();
		return super.add( aColumn );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void add( int aIndex, C aColumn ) {
		if ( containsKey( aColumn.getKey() ) ) {
			throw new IllegalArgumentException( "A column with key \"" + aColumn.getKey() + "\" already exists, cannot add a column with the same key twice." );
		}
		_keys.clear();
		super.add( aIndex, aColumn );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<C> values() {
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		final StringBuilder theBuffer = new StringBuilder();
		theBuffer.append( getClass().getSimpleName() );
		theBuffer.append( "[ " );
		Column<?> eColum;
		for ( int i = 0; i < size(); i++ ) {
			eColum = get( i );
			theBuffer.append( eColum.getKey() );
			if ( i < size() - 1 ) {
				theBuffer.append( ", " );
			}
		}
		theBuffer.append( " ]" );
		return theBuffer.toString();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////
	/**
	 * Converts a {@link Row} to a {@link Record}; depending on the boolean
	 * argument passed either the type of the {@link Column} is tested with the
	 * according value's type (and in case of a mismatch a
	 * {@link ColumnMismatchException} is thrown) or the {@link Column}'s type
	 * is ignored and none {@link ColumnMismatchException} is ever thrown so
	 * that conversion is done ignoring the types.
	 * 
	 * @param aRow The {@link Row} to be converted to a {@link Record}.
	 * @param isIgnoreType When true no {@link ColumnMismatchException} is
	 *        thrown and conversion is done ignoring the types; when true the
	 *        types must match else a {@link ColumnMismatchException} is thrown.
	 * 
	 * @return The {@link Record} representation of the {@link Row}.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link Header}.
	 * @throws ColumnMismatchException Thrown in case a value was found in the
	 *         e.g. in a {@link Row} of the wrong type than specified by a
	 *         {@link Column} of the {@link Header}.
	 */
	private Record<?> toRecord( Row<?> aRow, boolean isIgnoreType ) throws HeaderMismatchException, ColumnMismatchException {
		// ---------------------------------------------------------------------
		// Test the preconditions:
		// ---------------------------------------------------------------------
		throwHeaderMismatchException( aRow );
		// ---------------------------------------------------------------------
		final Record<Object> theRecord = new RecordImpl<>();
		Object eValue;
		C eColumn;
		for ( int i = 0; i < this.size(); i++ ) {
			eColumn = this.get( i );
			eValue = aRow.get( i );
			if ( !isIgnoreType && eValue != null ) {
				throwColumnMismatchException( eValue, eColumn );
			}
			theRecord.put( eColumn.getKey(), eValue );
		}
		return theRecord;
	}

	/**
	 * Converts a {@link Record} to a {@link Row}; depending on the boolean
	 * argument passed either the type of the {@link Column} is tested with the
	 * according value's type (and in case of a mismatch a
	 * {@link ColumnMismatchException} is thrown) or the {@link Column}'s type
	 * is ignored and none {@link ColumnMismatchException} is ever thrown so
	 * that conversion is done ignoring the types.
	 * 
	 * @param aRecord The {@link Record} to be converted to a {@link Row}.
	 * @param isIgnoreType When true no {@link ColumnMismatchException} is
	 *        thrown and conversion is done ignoring the types; when true the
	 *        types must match else a {@link ColumnMismatchException} is thrown.
	 * 
	 * @return The {@link Row} representation of the {@link Record}.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link Header}.
	 * @throws ColumnMismatchException Thrown in case a value was found in the
	 *         e.g. in a {@link Row} of the wrong type than specified by a
	 *         {@link Column} of the {@link Header}.
	 */
	private Row<?> toRow( Record<?> aRecord, boolean isIgnoreType ) throws HeaderMismatchException, ColumnMismatchException {
		// ---------------------------------------------------------------------
		// Test the preconditions:
		// ---------------------------------------------------------------------
		throwHeaderMismatchException( aRecord );
		// ---------------------------------------------------------------------
		final Row<Object> theRow = new RowImpl<>();
		Object eValue;
		for ( C eColumn : this ) {
			if ( eColumn.contains( aRecord ) ) {
				eValue = eColumn.get( aRecord );
				theRow.add( eValue );
			}
			else if ( aRecord.containsKey( eColumn.getKey() ) ) {
				eValue = aRecord.get( eColumn.getKey() );
				if ( !isIgnoreType ) {
					throwColumnMismatchException( eValue, eColumn );
				}
				else {
					theRow.add( eValue );
				}
			}
			else {
				theRow.add( null );
			}
		}
		return theRow;
	}

	/**
	 * Tests the preconditions of the given {@link Record} with {@link Header}.
	 * Surplus columns in the {@link Record} not found in the {@link Header} are
	 * valid; surplus fields in the {@link Header} not found in the
	 * {@link Record} aCause a {@link HeaderMismatchException} to be thrown.
	 * 
	 * @param aRecord The {@link Record} to be tested against the {@link Header}
	 *        .
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link Header}.
	 */
	private void throwHeaderMismatchException( Record<?> aRecord ) throws HeaderMismatchException {
		for ( String eKey : this.keySet() ) {
			if ( !aRecord.containsKey( eKey ) ) {
				throw new HeaderMismatchException( "The key \"" + eKey + "\" provided by the header is not found in the given record.", eKey );
			}
		}
		// @formatter:off
		/*
			for ( String eKey : aRecord.keySet() ) {
				if ( !this.containsKey( eKey ) ) {
					throw new HeaderMismatchException( eKey, this, "The key \"" + eKey + "\" provided by the record is not found in the given header." );
				}
			}
		*/
		// @formatter:on
	}

	/**
	 * Tests the preconditions of the given {@link Row} with {@link Header}.
	 * 
	 * @param aRow The {@link Row} to be tested against the {@link Header} .
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link Header}.
	 */
	private void throwHeaderMismatchException( Row<?> aRow ) throws HeaderMismatchException {
		if ( this.size() != aRow.size() ) {
			final String theKey = this.size() > aRow.size() ? this.get( aRow.size() ).getKey() : this.get( 0 ).getKey();
			throw new HeaderMismatchException( "The row size <" + aRow.size() + "> does not match the header size <" + this.size() + ">; unable to assign key \"" + theKey + "\" with value from row.", theKey );
		}
	}

	/**
	 * Tests the preconditions of the given {@link Column} with the value.
	 * 
	 * @param aValue The value to be tests whether it fits with the
	 *        {@link Column}.
	 * @param eColumn The {@link Column} with which to test whether the given
	 *        value fits.
	 * 
	 * @throws ColumnMismatchException Thrown in case a value was found in the
	 *         e.g. in a {@link Record} of the wrong type than specified by a
	 *         {@link Column} of the {@link Header}.
	 */
	private void throwColumnMismatchException( Object aValue, Column<?> eColumn ) throws ColumnMismatchException {
		if ( !eColumn.getType().isAssignableFrom( aValue.getClass() ) ) {
			throw new ColumnMismatchException( "The type <" + eColumn.getType().getName() + "> for column with key \"" + eColumn.getKey() + "\" does not match the value's type <" + aValue.getClass().getName() + ">.", eColumn, aValue );
		}
	}
}