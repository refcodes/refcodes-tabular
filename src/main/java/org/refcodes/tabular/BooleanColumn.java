// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import java.text.ParseException;

import org.refcodes.data.BooleanLiterals;
import org.refcodes.data.Literal;

/**
 * Implementation of the {@link Column} interface for working with
 * {@link Boolean} instances, being {@link Cloneable}.
 */
public class BooleanColumn extends AbstractColumn<Boolean> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a {@link BooleanColumn} managing {@link Boolean} instances.
	 * 
	 * @param aKey The key for the {@link BooleanColumn}.
	 */
	public BooleanColumn( String aKey ) {
		super( aKey, Boolean.class );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toStorageStrings( Boolean aValue ) {
		return new String[] { ( aValue != null ) ? ( aValue ? Literal.TRUE.getValue() : Literal.FALSE.getValue() ) : Literal.FALSE.getValue() };
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Boolean fromStorageStrings( String[] aStringValues ) throws ParseException {
		if ( aStringValues == null || aStringValues.length == 0 ) {
			return null;
		}
		else if ( aStringValues.length == 1 ) {
			if ( aStringValues[0] == null ) {
				return null;
			}
			if ( BooleanLiterals.isTrue( aStringValues[0] ) ) {
				return true;
			}
			if ( BooleanLiterals.isFalse( aStringValues[0] ) ) {
				return false;
			}
			return null;
		}
		throw new IllegalArgumentException( "The type <" + getType().getName() + "> is not an array type though the number of elements in the provided string array is <" + aStringValues.length + "> whereas only one element is being expected." );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}