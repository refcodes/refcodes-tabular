// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.refcodes.data.Delimiter;
import org.refcodes.textual.CsvBuilder;
import org.refcodes.textual.CsvEscapeMode;
import org.refcodes.textual.CsvMixin;
import org.refcodes.textual.VerboseTextBuilder;

/**
 * The {@link CsvRecordWriter} writes CSV-Data.
 *
 * @param <T> The type of the {@link Record} instances being used.
 */
public class CsvRecordWriter<T> implements CsvMixin, RecordWriter<T> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected Header<T> _header;
	private ColumnFactory<T> _columnFactory;
	protected PrintStream _outputStream;
	protected CsvBuilder _csvBuilder;
	private String[] _commentPrefixes = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param aHeader The header used for logging in the correct format.
	 * @param aCsvFile The {@link File} to be used for printing.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 *         opening or creating the file.
	 */
	public CsvRecordWriter( Header<T> aHeader, File aCsvFile ) throws FileNotFoundException {
		this( aHeader, null, new PrintStream( aCsvFile ), Delimiter.CSV.getChar() );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param aHeader The header used for logging in the correct format.
	 * @param aCsvFile The {@link File} to be used for printing.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 *         opening or creating the file.
	 */
	public CsvRecordWriter( Header<T> aHeader, File aCsvFile, char aCsvDelimiter ) throws FileNotFoundException {
		this( aHeader, null, new PrintStream( aCsvFile ), aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter}.
	 * 
	 * @param aCsvFile The {@link File} to be used for printing output.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 *         opening or creating the file.
	 */
	public CsvRecordWriter( File aCsvFile ) throws FileNotFoundException {
		this( null, null, new PrintStream( aCsvFile ), Delimiter.CSV.getChar() );
	}

	/**
	 * Constructs a {@link CsvRecordWriter}.
	 * 
	 * @param aCsvFile The {@link File} to be used for printing output.
	 *        aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 *         opening or creating the file.
	 */
	public CsvRecordWriter( File aCsvFile, char aCsvDelimiter ) throws FileNotFoundException {
		this( null, null, new PrintStream( aCsvFile ), aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param aHeader The header used for logging in the correct format.
	 * @param aCsvOutputStream The {@link OutputStream} to be used for printing.
	 */
	public CsvRecordWriter( Header<T> aHeader, OutputStream aCsvOutputStream ) {
		this( aHeader, null, new PrintStream( aCsvOutputStream, true ), Delimiter.CSV.getChar() );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param aHeader The header used for logging in the correct format.
	 * @param aCsvOutputStream The {@link OutputStream} to be used for printing.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 */
	public CsvRecordWriter( Header<T> aHeader, OutputStream aCsvOutputStream, char aCsvDelimiter ) {
		this( aHeader, null, new PrintStream( aCsvOutputStream, true ), aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter}.
	 * 
	 * @param aCsvOutputStream The {@link OutputStream} to be used for printing
	 *        output.
	 */
	public CsvRecordWriter( OutputStream aCsvOutputStream ) {
		this( null, null, new PrintStream( aCsvOutputStream, true ), Delimiter.CSV.getChar() );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param aColumnFactory The header used for logging in the correct format.
	 * @param aCsvFile The {@link File} to be used for printing.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 *         opening or creating the file.
	 */
	public CsvRecordWriter( ColumnFactory<T> aColumnFactory, File aCsvFile ) throws FileNotFoundException {
		this( null, aColumnFactory, new PrintStream( aCsvFile ), Delimiter.CSV.getChar() );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param aColumnFactory The header used for logging in the correct format.
	 * @param aCsvFile The {@link File} to be used for printing.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 *         opening or creating the file.
	 */
	public CsvRecordWriter( ColumnFactory<T> aColumnFactory, File aCsvFile, char aCsvDelimiter ) throws FileNotFoundException {
		this( null, aColumnFactory, new PrintStream( aCsvFile ), aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param aColumnFactory The header used for logging in the correct format.
	 * @param aCsvOutputStream The {@link OutputStream} to be used for printing.
	 */
	public CsvRecordWriter( ColumnFactory<T> aColumnFactory, OutputStream aCsvOutputStream ) {
		this( null, aColumnFactory, new PrintStream( aCsvOutputStream, true ), Delimiter.CSV.getChar() );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param aColumnFactory The header used for logging in the correct format.
	 * @param aCsvOutputStream The {@link OutputStream} to be used for printing.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 */
	public CsvRecordWriter( ColumnFactory<T> aColumnFactory, OutputStream aCsvOutputStream, char aCsvDelimiter ) {
		this( null, aColumnFactory, new PrintStream( aCsvOutputStream, true ), aCsvDelimiter );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param aHeader The header used for logging in the correct format.
	 * @param aCsvFile The {@link File} to be used for printing.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * 
	 * @throws IOException in case the file is not found or the encoding is not
	 *         supported.
	 */
	public CsvRecordWriter( Header<T> aHeader, File aCsvFile, Charset aEncoding ) throws IOException {
		this( aHeader, null, new PrintStream( aCsvFile, aEncoding.name() ), Delimiter.CSV.getChar() );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param aHeader The header used for logging in the correct format.
	 * @param aCsvFile The {@link File} to be used for printing.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @throws IOException in case the file is not found or the encoding is not
	 *         supported.
	 */
	public CsvRecordWriter( Header<T> aHeader, File aCsvFile, Charset aEncoding, char aCsvDelimiter ) throws IOException {
		this( aHeader, null, new PrintStream( aCsvFile, aEncoding.name() ), aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter}.
	 *
	 * @param aCsvFile The {@link File} to be used for printing output.
	 * @param aEncoding the encoding
	 * 
	 * @throws IOException in case the file is not found or the encoding is not
	 *         supported.
	 */
	public CsvRecordWriter( File aCsvFile, Charset aEncoding ) throws IOException {
		this( null, null, new PrintStream( aCsvFile, aEncoding.name() ), Delimiter.CSV.getChar() );
	}

	/**
	 * Constructs a {@link CsvRecordWriter}.
	 *
	 * @param aCsvFile The {@link File} to be used for printing output.
	 *        aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * @param aEncoding the encoding
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @throws IOException in case the file is not found or the encoding is not
	 *         supported.
	 */
	public CsvRecordWriter( File aCsvFile, Charset aEncoding, char aCsvDelimiter ) throws IOException {
		this( null, null, new PrintStream( aCsvFile, aEncoding.name() ), aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param aHeader The header used for logging in the correct format.
	 * @param aCsvOutputStream The {@link OutputStream} to be used for printing.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * 
	 * @throws UnsupportedEncodingException thrown in case an unsupported
	 *         encoding ({@link Charset}) has been provided.
	 */
	public CsvRecordWriter( Header<T> aHeader, OutputStream aCsvOutputStream, Charset aEncoding ) throws UnsupportedEncodingException {
		this( aHeader, null, new PrintStream( aCsvOutputStream, true, aEncoding.name() ), Delimiter.CSV.getChar() );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param aHeader The header used for logging in the correct format.
	 * @param aCsvOutputStream The {@link OutputStream} to be used for printing.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @throws UnsupportedEncodingException thrown in case an unsupported
	 *         encoding ({@link Charset}) has been provided.
	 */
	public CsvRecordWriter( Header<T> aHeader, OutputStream aCsvOutputStream, Charset aEncoding, char aCsvDelimiter ) throws UnsupportedEncodingException {
		this( aHeader, null, new PrintStream( aCsvOutputStream, true, aEncoding.name() ), aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter}.
	 *
	 * @param aCsvOutputStream The {@link OutputStream} to be used for printing
	 *        output.
	 * @param aEncoding the encoding
	 * 
	 * @throws UnsupportedEncodingException thrown in case an unsupported
	 *         encoding ({@link Charset}) has been provided.
	 */
	public CsvRecordWriter( OutputStream aCsvOutputStream, Charset aEncoding ) throws UnsupportedEncodingException {
		this( null, null, new PrintStream( aCsvOutputStream, true, aEncoding.name() ), Delimiter.CSV.getChar() );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param aColumnFactory The header used for logging in the correct format.
	 * @param aCsvFile The {@link File} to be used for printing.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * 
	 * @throws IOException in case the file is not found or the encoding is not
	 *         supported.
	 */
	public CsvRecordWriter( ColumnFactory<T> aColumnFactory, File aCsvFile, Charset aEncoding ) throws IOException {
		this( null, aColumnFactory, new PrintStream( aCsvFile, aEncoding.name() ), Delimiter.CSV.getChar() );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param aColumnFactory The header used for logging in the correct format.
	 * @param aCsvFile The {@link File} to be used for printing.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @throws IOException in case the file is not found or the encoding is not
	 *         supported.
	 */
	public CsvRecordWriter( ColumnFactory<T> aColumnFactory, File aCsvFile, Charset aEncoding, char aCsvDelimiter ) throws IOException {
		this( null, aColumnFactory, new PrintStream( aCsvFile, aEncoding.name() ), aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param aColumnFactory The header used for logging in the correct format.
	 * @param aCsvOutputStream The {@link OutputStream} to be used for printing.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * 
	 * @throws UnsupportedEncodingException thrown in case an unsupported
	 *         encoding ({@link Charset}) has been provided.
	 */
	public CsvRecordWriter( ColumnFactory<T> aColumnFactory, OutputStream aCsvOutputStream, Charset aEncoding ) throws UnsupportedEncodingException {
		this( null, aColumnFactory, new PrintStream( aCsvOutputStream, true, aEncoding.name() ), Delimiter.CSV.getChar() );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param aColumnFactory The header used for logging in the correct format.
	 * @param aCsvOutputStream The {@link OutputStream} to be used for printing.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @throws UnsupportedEncodingException thrown in case an unsupported
	 *         encoding ({@link Charset}) has been provided.
	 */
	public CsvRecordWriter( ColumnFactory<T> aColumnFactory, OutputStream aCsvOutputStream, Charset aEncoding, char aCsvDelimiter ) throws UnsupportedEncodingException {
		this( null, aColumnFactory, new PrintStream( aCsvOutputStream, true, aEncoding.name() ), aCsvDelimiter );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs a {@link CsvRecordWriter}.
	 * 
	 * @param aPrintStream The {@link PrintStream} to be used for printing.
	 */
	public CsvRecordWriter( PrintStream aPrintStream ) {
		this( null, null, aPrintStream, Delimiter.CSV.getChar() );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param aColumnFactory The header used for logging in the correct format.
	 * @param aPrintStream The {@link PrintStream} to be used for printing.
	 */
	public CsvRecordWriter( ColumnFactory<T> aColumnFactory, PrintStream aPrintStream ) {
		this( null, aColumnFactory, aPrintStream, Delimiter.CSV.getChar() );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param aHeader The header used for logging in the correct format.
	 * @param aPrintStream The {@link PrintStream} to be used for printing.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 */
	public CsvRecordWriter( Header<T> aHeader, PrintStream aPrintStream, char aCsvDelimiter ) {
		this( aHeader, null, aPrintStream, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param aHeader The header used for logging in the correct format.
	 * @param aPrintStream The {@link PrintStream} to be used for printing.
	 */
	public CsvRecordWriter( Header<T> aHeader, PrintStream aPrintStream ) {
		this( aHeader, null, aPrintStream, Delimiter.CSV.getChar() );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aPrintStream The {@link PrintStream} to be used for printing
	 *        output.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 */
	public CsvRecordWriter( ColumnFactory<T> aColumnFactory, PrintStream aPrintStream, char aCsvDelimiter ) {
		this( null, aColumnFactory, aPrintStream, aCsvDelimiter );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param aHeader The header used for logging in the correct format.
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aPrintStream The {@link PrintStream} to be used for printing
	 *        output.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 */
	protected CsvRecordWriter( Header<T> aHeader, ColumnFactory<T> aColumnFactory, PrintStream aPrintStream, char aCsvDelimiter ) {
		_csvBuilder = new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withDelimiter( aCsvDelimiter );
		_header = aHeader;
		_outputStream = aPrintStream != null ? aPrintStream : System.out;
		_columnFactory = aColumnFactory;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCommentPrefixes( String... aCommentPrefixes ) {
		_commentPrefixes = aCommentPrefixes;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] getCommentPrefixes() {
		return _commentPrefixes;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clearCommentPrefixes() {
		_commentPrefixes = null;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CsvRecordWriter<T> withCommentPrefixes( String... aCommentPrefixes ) {
		setCommentPrefixes( aCommentPrefixes );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		_outputStream.close();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CsvEscapeMode getCsvEscapeMode() {
		return _csvBuilder.getCsvEscapeMode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isTrim() {
		return _csvBuilder.isTrim();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char getDelimiter() {
		return _csvBuilder.getDelimiter();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setTrim( boolean isTrim ) {
		_csvBuilder.setTrim( isTrim );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setDelimiter( char aCsvDelimiter ) {
		_csvBuilder.setDelimiter( aCsvDelimiter );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CsvRecordWriter<T> withTrim( boolean isTrimRecords ) {
		_csvBuilder.setTrim( isTrimRecords );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CsvRecordWriter<T> withCsvEscapeMode( CsvEscapeMode aCsvEscapeMode ) {
		_csvBuilder.setCsvEscapeMode( aCsvEscapeMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CsvRecordWriter<T> withDelimiter( char aCsvDelimiter ) {
		_csvBuilder.setDelimiter( aCsvDelimiter );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCsvEscapeMode( CsvEscapeMode aCsvEscapeMode ) {
		_csvBuilder.setCsvEscapeMode( aCsvEscapeMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void parseHeader( String... aHeader ) {
		_header = TabularUtility.toHeader( aHeader, _columnFactory );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void writeHeader( String... aHeader ) {
		final String theHeader = _csvBuilder.toRecord( aHeader );
		_header = TabularUtility.toHeader( aHeader, _columnFactory );
		_outputStream.println( theHeader );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void writeHeader( Header<T> aHeader ) {
		_header = aHeader;
		_columnFactory = null;
		final String theHeader = _csvBuilder.toRecord( aHeader.toKeys() );
		_outputStream.println( theHeader );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void writeHeader() {
		final String theHeader = _csvBuilder.toRecord( _header.toKeys() );
		_outputStream.println( theHeader );
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("all")
	@Override
	public void writeNext( T... aRecord ) {
		if ( _header != null ) {
			if ( _header.size() != aRecord.length ) {
				throw new IllegalArgumentException( "Expecting the record to be of (header's) size <" + _header.size() + ">, though it is of length <" + ( aRecord != null ? aRecord.length : "null" ) + ">. The header provided = " + new VerboseTextBuilder().toString( _header.keySet() ) + ", the record provided = " + new VerboseTextBuilder().toString( aRecord ) + "." );
			}
			String eValue;
			final List<String> theRecord = new ArrayList<>();
			for ( int i = 0; i < _header.size(); i++ ) {
				eValue = _header.get( i ).toStorageString_( aRecord[i] );
				theRecord.add( eValue );
			}
			_outputStream.println( _csvBuilder.toRecord( theRecord ) );
		}
		else {
			_outputStream.println( _csvBuilder.toRecord( aRecord ) );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void writeNext( Record<T> aRecord ) throws ColumnMismatchException, HeaderMismatchException {
		if ( _header != null ) {
			final List<String> theStorageStrings = new ArrayList<>();
			T theValue;
			Column<? extends T> eColumn;
			for ( int i = 0; i < _header.size(); i++ ) {
				eColumn = _header.get( i );
				theValue = aRecord.get( eColumn.getKey() );
				theStorageStrings.add( eColumn.toStorageString_( theValue ) );
			}
			_outputStream.println( _csvBuilder.toRecord( theStorageStrings ) );
		}
		else {
			throw new IllegalStateException( "In order to use this method you must have configured / initialized a Header instance." );
		}
	}

	/**
	 * Writes a comment by using the fist defined comment prefix as of
	 * {@link #getCommentPrefixes()} suffixed with a space (" ") followed by the
	 * comment (as of the {@link #toComment(String)}). If no prefix has been
	 * set, then an {@link IllegalStateException} will be thrown.
	 * 
	 * @param aComment The comment to be written.
	 * 
	 * @throws IllegalStateException in case no comment prefixes have been set
	 *         via {@link #setCommentPrefixes(String...)} or
	 *         {@link #withCommentPrefixes(String...)}.
	 */
	public void writeComment( String aComment ) {
		_outputStream.println( toComment( aComment ) );
	}

	/**
	 * Writes the given {@link Header} as of {@link #writeHeader(String...)} as
	 * a comment as of {@link #writeComment(String)}.
	 * 
	 * @param aHeader The {@link Header} to be written as a comment.
	 */
	public void writeHeaderComment( String... aHeader ) {
		final String theHeader = _csvBuilder.toRecord( aHeader );
		_header = TabularUtility.toHeader( aHeader, _columnFactory );
		_outputStream.println( toComment( theHeader ) );
	}

	/**
	 * Writes the given {@link Header} as of {@link #writeHeader(Header)} as a
	 * comment as of {@link #writeComment(String)}.
	 * 
	 * @param aHeader The {@link Header} to be written as a comment.
	 */
	public void writeHeaderComment( Header<T> aHeader ) {
		_header = aHeader;
		_columnFactory = null;
		final String theHeader = _csvBuilder.toRecord( aHeader.toKeys() );
		_outputStream.println( toComment( theHeader ) );
	}

	/**
	 * Writes the {@link Header} as of {@link #writeHeader()} as a comment as of
	 * {@link #writeComment(String)}.
	 */
	public void writeHeaderComment() {
		final String theHeader = _csvBuilder.toRecord( _header.toKeys() );
		_outputStream.println( toComment( theHeader ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Header<T> getHeader() {
		return _header;
	}
}
