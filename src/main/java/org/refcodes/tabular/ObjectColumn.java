// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.lang.reflect.Array;
import java.text.ParseException;
import java.util.Base64;

import org.refcodes.exception.RuntimeIOException;

/**
 * Implementation of a {@link Column} supporting {@link Object} instances, being
 * {@link Cloneable}. The storage {@link String} instance is converted from and
 * back using serialization; the serialized byte arrays are converted using
 * base64 encoding to a {@link String}.
 *
 * @param <T> The type managed by the {@link Column}.
 */
public class ObjectColumn<T> extends AbstractColumn<T> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a {@link ObjectColumn} managing {@link String} instances.
	 * 
	 * @param aKey The key for the {@link ObjectColumn}.
	 * @param aType The type to be used.
	 */
	public ObjectColumn( String aKey, Class<T> aType ) {
		super( aKey, aType );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toStorageStrings( T aValue ) {
		if ( aValue == null ) {
			return null;
		}
		else {
			if ( getType().isArray() ) {
				final Object[] theValues = (Object[]) aValue;
				final String[] theStrings = new String[theValues.length];
				for ( int i = 0; i < theValues.length; i++ ) {
					theStrings[i] = toString( theValues[i] );
				}
				return theStrings;
			}
			else {
				return new String[] { toString( aValue ) };
			}
		}
	}

	/**
	 * From storage strings.
	 *
	 * @param aStringValues the string values
	 * 
	 * @return the t
	 * 
	 * @throws ParseException the parse exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public T fromStorageStrings( String[] aStringValues ) throws ParseException {
		if ( aStringValues == null || aStringValues.length == 0 ) {
			return null;
		}
		if ( getType().isArray() ) {
			final Object[] theValues = (Object[]) Array.newInstance( getType().getComponentType(), aStringValues.length );

			for ( int i = 0; i < aStringValues.length; i++ ) {
				theValues[i] = fromString( aStringValues[i] );
			}
			return (T) theValues;
		}
		else if ( aStringValues.length == 1 ) {
			if ( aStringValues[0] == null ) {
				return null;
			}
			return fromString( aStringValues[0] );
		}
		throw new IllegalArgumentException( "The type <" + getType().getName() + "> is not an array type though the number of elements in the provided string array is <" + aStringValues.length + "> whereas only one element is being expected." );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a {@link String} from the serialized value.
	 * 
	 * @param aValue The value from which to get the serialized {@link String}.
	 * 
	 * @return The serialized {@link String} from the given value.
	 */
	private String toString( Object aValue ) {
		final ByteArrayOutputStream theOutputStream = new ByteArrayOutputStream();
		ObjectOutput theOutput = null;
		try {
			theOutput = new ObjectOutputStream( theOutputStream );
			theOutput.writeObject( aValue );
			final byte[] theBytes = theOutputStream.toByteArray();
			return Base64.getEncoder().encodeToString( theBytes );
		}
		catch ( IOException e ) {
			throw new RuntimeIOException( e );
		}
		finally {
			try {
				if ( theOutput != null ) {
					theOutput.close();
				}
			}
			catch ( IOException ex ) { /* Ignore */}
			try {
				theOutputStream.close();
			}
			catch ( IOException ex ) { /* Ignore */}
		}
	}

	/**
	 * Creates a value of type T from the serialized {@link String} value.
	 *
	 * @param aStringValue the string value
	 * 
	 * @return The serialized {@link String} from the given value.
	 */
	@SuppressWarnings("unchecked")
	private T fromString( String aStringValue ) {
		final ByteArrayInputStream theInputStream = new ByteArrayInputStream( Base64.getDecoder().decode( aStringValue ) );
		ObjectInput theInput = null;
		try {
			theInput = new ObjectInputStream( theInputStream );
			return (T) theInput.readObject();
		}
		catch ( IOException e ) {
			throw new RuntimeIOException( e );
		}
		catch ( ClassNotFoundException e ) {
			throw new IllegalArgumentException( "Cannot create instance of type <" + getType().getName() + "> for value <" + aStringValue + ">!", e );
		}
		finally {
			try {
				theInputStream.close();
			}
			catch ( IOException ex ) {/* Ignore */}
			try {
				if ( theInput != null ) {
					theInput.close();
				}
			}
			catch ( IOException ex ) {/* Ignore */}
		}
	}
}