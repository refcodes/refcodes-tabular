// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import org.refcodes.textual.CsvEscapeMode;

/**
 * @author steiner
 */
public class CsvStringRecordWriter extends CsvRecordWriter<String> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * @param aPrintStream The {@link PrintStream} to be used for printing
	 *        output.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 */
	public CsvStringRecordWriter( String[] aHeader, PrintStream aPrintStream, char aCsvDelimiter ) {
		super( new StringHeader( aHeader ), aPrintStream, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * @param aFile the {@link File} to which to write.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 */
	public CsvStringRecordWriter( String[] aHeader, File aFile, char aCsvDelimiter ) throws FileNotFoundException {
		super( new StringHeader( aHeader ), aFile, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * @param aFile the {@link File} to which to write.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @throws IOException in case the file is not found or the encoding is not
	 *         supported.
	 */
	public CsvStringRecordWriter( String[] aHeader, File aFile, Charset aEncoding, char aCsvDelimiter ) throws IOException {
		super( new StringHeader( aHeader ), aFile, aEncoding, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * @param aFile the {@link File} to which to write.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * 
	 * @throws IOException in case the file is not found or the encoding is not
	 *         supported.
	 */
	public CsvStringRecordWriter( String[] aHeader, File aFile, Charset aEncoding ) throws IOException {
		super( new StringHeader( aHeader ), aFile, aEncoding );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * @param aFile the {@link File} to which to write.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 *         opening or creating the file.
	 */
	public CsvStringRecordWriter( String[] aHeader, File aFile ) throws FileNotFoundException {
		super( new StringHeader( aHeader ), aFile );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * @param aOutputStream The {@link OutputStream} where to write to.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 */
	public CsvStringRecordWriter( String[] aHeader, OutputStream aOutputStream, char aCsvDelimiter ) {
		super( new StringHeader( aHeader ), aOutputStream, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * @param aOutputStream The {@link OutputStream} where to write to.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @throws UnsupportedEncodingException thrown in case an unsupported
	 *         encoding ({@link Charset}) has been provided.
	 */
	public CsvStringRecordWriter( String[] aHeader, OutputStream aOutputStream, Charset aEncoding, char aCsvDelimiter ) throws UnsupportedEncodingException {
		super( new StringHeader( aHeader ), aOutputStream, aEncoding, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * @param aOutputStream The {@link OutputStream} where to write to.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * 
	 * @throws UnsupportedEncodingException thrown in case an unsupported
	 *         encoding ({@link Charset}) has been provided.
	 */
	public CsvStringRecordWriter( String[] aHeader, OutputStream aOutputStream, Charset aEncoding ) throws UnsupportedEncodingException {
		super( new StringHeader( aHeader ), aOutputStream, aEncoding );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * @param aOutputStream The {@link OutputStream} where to write to.
	 */
	public CsvStringRecordWriter( String[] aHeader, OutputStream aOutputStream ) {
		super( new StringHeader( aHeader ), aOutputStream );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 * 
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * @param aPrintStream The {@link PrintStream} to be used for printing.
	 */
	public CsvStringRecordWriter( String[] aHeader, PrintStream aPrintStream ) {
		super( new StringHeader( aHeader ), aPrintStream );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aPrintStream The {@link PrintStream} to be used for printing
	 *        output.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 */
	public CsvStringRecordWriter( PrintStream aPrintStream, char aCsvDelimiter, String... aHeader ) {
		super( new StringHeader( aHeader ), aPrintStream, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aFile the {@link File} to which to write.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 */
	public CsvStringRecordWriter( File aFile, char aCsvDelimiter, String... aHeader ) throws FileNotFoundException {
		super( new StringHeader( aHeader ), aFile, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aFile the {@link File} to which to write.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * 
	 * @throws IOException in case the file is not found or the encoding is not
	 *         supported.
	 */
	public CsvStringRecordWriter( File aFile, Charset aEncoding, char aCsvDelimiter, String... aHeader ) throws IOException {
		super( new StringHeader( aHeader ), aFile, aEncoding, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aFile the {@link File} to which to write.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * 
	 * @throws IOException in case the file is not found or the encoding is not
	 *         supported.
	 */
	public CsvStringRecordWriter( File aFile, Charset aEncoding, String... aHeader ) throws IOException {
		super( new StringHeader( aHeader ), aFile, aEncoding );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aFile the {@link File} to which to write.
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 *         opening or creating the file.
	 */
	public CsvStringRecordWriter( File aFile, String... aHeader ) throws FileNotFoundException {
		super( new StringHeader( aHeader ), aFile );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aOutputStream The {@link OutputStream} where to write to.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 */
	public CsvStringRecordWriter( OutputStream aOutputStream, char aCsvDelimiter, String... aHeader ) {
		super( new StringHeader( aHeader ), aOutputStream, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aOutputStream The {@link OutputStream} where to write to.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * 
	 * @throws UnsupportedEncodingException thrown in case an unsupported
	 *         encoding ({@link Charset}) has been provided.
	 */
	public CsvStringRecordWriter( OutputStream aOutputStream, Charset aEncoding, char aCsvDelimiter, String... aHeader ) throws UnsupportedEncodingException {
		super( new StringHeader( aHeader ), aOutputStream, aEncoding, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aOutputStream The {@link OutputStream} where to write to.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 * 
	 * @throws UnsupportedEncodingException thrown in case an unsupported
	 *         encoding ({@link Charset}) has been provided.
	 */
	public CsvStringRecordWriter( OutputStream aOutputStream, Charset aEncoding, String... aHeader ) throws UnsupportedEncodingException {
		super( new StringHeader( aHeader ), aOutputStream, aEncoding );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aOutputStream The {@link OutputStream} where to write to.
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 */
	public CsvStringRecordWriter( OutputStream aOutputStream, String... aHeader ) {
		super( new StringHeader( aHeader ), aOutputStream );
	}

	/**
	 * Constructs a {@link CsvRecordWriter} with the given header for logging.
	 *
	 * @param aPrintStream The {@link PrintStream} to be used for printing.
	 * @param aHeader The {@link String} elements representing the header used
	 *        for logging in the correct format.
	 */
	public CsvStringRecordWriter( PrintStream aPrintStream, String... aHeader ) {
		super( new StringHeader( aHeader ), aPrintStream );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordWriter( File aFile, char aCsvDelimiter ) throws FileNotFoundException {
		super( new StringColumnFactory(), aFile, aCsvDelimiter );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordWriter( File aFile, Charset aEncoding, char aCsvDelimiter ) throws IOException {
		super( new StringColumnFactory(), aFile, aEncoding, aCsvDelimiter );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordWriter( File aFile, Charset aEncoding ) throws IOException {
		super( new StringColumnFactory(), aFile, aEncoding );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordWriter( File aFile ) throws FileNotFoundException {
		super( new StringColumnFactory(), aFile );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordWriter( OutputStream aOutputStream, char aCsvDelimiter ) {
		super( new StringColumnFactory(), aOutputStream, aCsvDelimiter );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordWriter( OutputStream aOutputStream, Charset aEncoding, char aCsvDelimiter ) throws UnsupportedEncodingException {
		super( new StringColumnFactory(), aOutputStream, aEncoding, aCsvDelimiter );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordWriter( OutputStream aOutputStream, Charset aEncoding ) throws UnsupportedEncodingException {
		super( new StringColumnFactory(), aOutputStream, aEncoding );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordWriter( OutputStream aOutputStream ) {
		super( new StringColumnFactory(), aOutputStream );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordWriter( PrintStream aPrintStream, char aCsvDelimiter ) {
		super( new StringColumnFactory(), aPrintStream, aCsvDelimiter );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordWriter( PrintStream aPrintStream ) {
		super( new StringColumnFactory(), aPrintStream );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordWriter( Header<String> aHeader, PrintStream aPrintStream, char aCsvDelimiter ) {
		super( aHeader, aPrintStream, aCsvDelimiter );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordWriter( Header<String> aHeader, File aFile, char aCsvDelimiter ) throws FileNotFoundException {
		super( aHeader, aFile, aCsvDelimiter );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordWriter( Header<String> aHeader, File aFile, Charset aEncoding, char aCsvDelimiter ) throws IOException {
		super( aHeader, aFile, aEncoding, aCsvDelimiter );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordWriter( Header<String> aHeader, File aFile, Charset aEncoding ) throws IOException {
		super( aHeader, aFile, aEncoding );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordWriter( Header<String> aHeader, File aFile ) throws FileNotFoundException {
		super( aHeader, aFile );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordWriter( Header<String> aHeader, OutputStream aOutputStream, char aCsvDelimiter ) {
		super( aHeader, aOutputStream, aCsvDelimiter );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordWriter( Header<String> aHeader, OutputStream aOutputStream, Charset aEncoding, char aCsvDelimiter ) throws UnsupportedEncodingException {
		super( aHeader, aOutputStream, aEncoding, aCsvDelimiter );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordWriter( Header<String> aHeader, OutputStream aOutputStream, Charset aEncoding ) throws UnsupportedEncodingException {
		super( aHeader, aOutputStream, aEncoding );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordWriter( Header<String> aHeader, OutputStream aOutputStream ) {
		super( aHeader, aOutputStream );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordWriter( Header<String> aHeader, PrintStream aPrintStream ) {
		super( aHeader, aPrintStream );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CsvStringRecordWriter withTrim( boolean isTrimRecords ) {
		_csvBuilder.setTrim( isTrimRecords );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CsvStringRecordWriter withCsvEscapeMode( CsvEscapeMode aCsvEscapeMode ) {
		_csvBuilder.setCsvEscapeMode( aCsvEscapeMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CsvStringRecordWriter withDelimiter( char aCsvDelimiter ) {
		_csvBuilder.setDelimiter( aCsvDelimiter );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CsvStringRecordWriter withCommentPrefixes( String... aCommentPrefixes ) {
		setCommentPrefixes( aCommentPrefixes );
		return this;
	}
}
