// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import java.lang.reflect.Array;
import java.util.LinkedHashSet;

/**
 * A list of {@link Column} instances, for example describing the elements of a
 * CSV file (visually speaking the of the CSV file's header line), is
 * represented by the {@link org.refcodes.tabular.Header}. The
 * {@link org.refcodes.tabular.Header} preserves an order for a list of
 * {@link org.refcodes.tabular.Column} instances. A
 * {@link org.refcodes.tabular.Header} provides the semantics for related
 * {@link org.refcodes.tabular.Row} instances.
 * <p>
 * The {@link #keySet()} method must provide a predictable order as ensured by
 * the {@link LinkedHashSet} class as of the ordered nature of the
 * {@link Header}.
 *
 * @param <T> The type managed by the {@link Header}.
 */
public interface Header<T> extends HeaderRow<T, Column<? extends T>> {

	/**
	 * This method retrieves a value from the {@link Row} by taking the index of
	 * the according column of this {@link Header} of the given key (the one
	 * with the given key) and returns that value. This is possible as the
	 * values in the row and in the header have an order. This method is a
	 * convenience delegate to {@link Row#get(Header, String)}.
	 * 
	 * @param aRow The {@link Row} containing the value of the given key.
	 * @param aKey The key for which to retrieve the value.
	 * 
	 * @return The value in the row representing the key in the header.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link Header}.
	 * @throws ColumnMismatchException in case the type defined in the column
	 *         for that key does not match the type in the row identified by the
	 *         given key.
	 */
	default T get( Row<T> aRow, String aKey ) throws HeaderMismatchException, ColumnMismatchException {
		return aRow.get( this, aKey );
	}

	/**
	 * Returns the keys of the {@link Header} in the intended order.
	 * 
	 * @return The keys of the {@link Header} in correct order.
	 */
	default String[] toKeys() {
		return keySet().toArray( new String[size()] );
	}

	/**
	 * Returns an array of the values stored by the {@link Record} in the order
	 * as defined by the header.
	 * 
	 * @param aRecord The {@link Record} to be converted to an array of values.
	 * 
	 * @return The array of values.
	 */
	@SuppressWarnings("unchecked")
	default T[] toValues( Record<T> aRecord ) {
		T[] theValues = null;
		final Class<T> theType = toType();
		if ( theType != null ) {
			theValues = (T[]) Array.newInstance( theType, size() );
			String eKey;
			for ( int i = 0; i < size(); i++ ) {
				eKey = get( i ).getKey();
				theValues[i] = aRecord.get( eKey );
			}
		}
		return theValues;
	}

	/**
	 * Determines the least common denominator type by evaluating the
	 * {@link Column} instances contained within this {@link Header}.
	 * 
	 * @return Determines the lest common denominator type or null (in case
	 *         there are no {@link Column} instances in the {@link Header}).
	 */
	@SuppressWarnings("unchecked")
	default Class<T> toType() {
		Class<T> theType = null;
		Class<?> eType;
		for ( Column<?> eColumn : this ) {
			eType = eColumn.getType();
			if ( theType == null || eType.isAssignableFrom( theType ) ) {
				theType = (Class<T>) eType;
			}
		}
		return theType;
	}

	/**
	 * Tests whether the given potential subset of {@link Header} elements is
	 * matching the {@link Record}'s elements in terms of matching the same
	 * relevant attributes of the {@link Column} instances with the elements in
	 * the {@link Record}. I.e. all {@link Column} instances in the subset must
	 * match the elements, even if this has more elements. The test also fails
	 * (returns false) in case the types of the instances in the {@link Record}
	 * do not match the according key's type in the {@link Header}
	 * {@link Column} instance or vice versa.
	 *
	 * @param aRecord The {@link Record} to be used for testing.
	 * 
	 * @return true in case all columns in the subset match this' elements or
	 *         the argument passed was null.
	 */
	default boolean isSubsetOf( Record<?> aRecord ) {
		if ( this.size() < aRecord.size() ) {
			return false;
		}
		Column<?> eColumn = null;
		Object eValue = null;
		for ( String eKey : aRecord.keySet() ) {
			if ( !this.containsKey( eKey ) ) {
				return false;
			}
			eColumn = this.get( eKey );
			eValue = aRecord.get( eKey );
			if ( !eColumn.getType().isAssignableFrom( eValue.getClass() ) ) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Tests whether the given potential equal set of {@link Header} is matching
	 * the {@link Record}'s elements in terms of matching the same relevant
	 * attributes of the columns with the elements in the {@link Record}. I.e.
	 * all {@link Header} in the subset must match the elements, and the number
	 * of {@link Header} must be the same as the number of elements in the
	 * {@link Record}. The test also fails (returns false) in case the types of
	 * the instances in the {@link Record} do not match the according key's type
	 * in the {@link Header} {@link Column} instance or vice versa.
	 * 
	 * @param aRecord The {@link Record} to be used for testing.
	 * 
	 * @return True in case all columns in the subset match this' elements and
	 *         the number of elements matches the number of columns or the
	 *         argument passed was null.
	 */
	default boolean isEqualWith( Record<?> aRecord ) {
		if ( this.size() != aRecord.size() ) {
			return false;
		}
		Column<?> eColumn = null;
		Object eValue = null;
		for ( String eKey : aRecord.keySet() ) {
			if ( !this.containsKey( eKey ) ) {
				return false;
			}
			eColumn = this.get( eKey );
			eValue = aRecord.get( eKey );
			if ( !eColumn.getType().isAssignableFrom( eValue.getClass() ) ) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Tests whether the {@link Record}'s elements is matching the given
	 * potential superset of {@link Header} elements in terms of matching the
	 * same relevant attributes of the columns with the elements in the
	 * {@link Record}. I.e. all elements in the {@link Record} must match the
	 * {@link Header} in the superset, even if the superset has more elements.
	 * The test also fails (returns false) in case the types of the instances in
	 * the {@link Record} do not match the according key's type in the
	 * {@link Header} {@link Column} instance or vice versa.
	 * 
	 * @param aRecord The {@link Record} to be used for testing.
	 *
	 * @return True in case all elements in this instance match the columns in
	 *         the superset or the argument passed was null.
	 */
	default boolean isSupersetOf( Record<?> aRecord ) {
		if ( size() > aRecord.size() ) {
			return false;
		}
		Object eValue = null;
		Column<?> eColumn = null;
		for ( String eKey : keySet() ) {
			if ( !aRecord.containsKey( eKey ) ) {
				return false;
			}
			eColumn = get( eKey );
			eValue = aRecord.get( eKey );
			if ( !eColumn.getType().isAssignableFrom( eValue.getClass() ) ) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Tests whether this {@link Header} is a subset of the {@link Row}'s
	 * elements in terms of matching the same relevant attributes of the
	 * {@link Header} with the elements in the {@link Row}. I.e. all
	 * {@link Column} instances in the subset must match the elements, even if
	 * this has more elements. The test also fails (returns false) in case the
	 * types of the instances in the {@link Row} do not match the according
	 * key's type in the {@link Header} {@link Column} instance or vice versa.
	 *
	 * @param aRow The {@link Row} to be used for testing.
	 * 
	 * @return true in case all columns in the subset match this' elements or
	 *         the argument passed was null.
	 */
	default boolean isSubsetOf( Row<?> aRow ) {
		if ( this.size() < aRow.size() ) {
			return false;
		}
		int theIndex = 0;
		Column<?> eColumn = null;
		for ( Object eValue : aRow ) {
			eColumn = this.get( theIndex );
			if ( !eColumn.getType().isAssignableFrom( eValue.getClass() ) ) {
				return false;
			}
			theIndex++;
		}

		return true;
	}

	/**
	 * Tests whether the given potential {@link Header} equal set is matching
	 * the {@link Row}'s elements in terms of matching the same relevant
	 * attributes of the {@link Header} with the elements in the {@link Row}.
	 * I.e. all {@link Column} instances in the subset must match the elements,
	 * and the number of columns must be the same as the number of elements in
	 * the {@link Row}. The test also fails (returns false) in case the types of
	 * the instances in the {@link Row} do not match the according key's type in
	 * the {@link Header} {@link Column} instance or vice versa.
	 * 
	 * @param aRow The {@link Row} to be used for testing.
	 * 
	 * @return True in case all columns in the subset match this' elements and
	 *         the number of elements matches the number of columns or the
	 *         argument passed was null.
	 */
	default boolean isEqualWith( Row<?> aRow ) {
		if ( this.size() != aRow.size() ) {
			return false;
		}
		int theIndex = 0;
		Column<?> eColumn;
		for ( Object eValue : aRow ) {
			eColumn = this.get( theIndex );
			if ( !eColumn.getType().isAssignableFrom( eValue.getClass() ) ) {
				return false;
			}
			theIndex++;
		}
		return true;
	}

	/**
	 * Tests whether the {@link Row}'s elements is matching the given potential
	 * {@link Header} superset in terms of matching the same relevant attributes
	 * of the {@link Header} with the elements in the {@link Row}. I.e. all
	 * elements in the {@link Row} must match the {@link Column} instances in
	 * the superset, even if the superset has more elements. The test also fails
	 * (returns false) in case the types of the instances in the {@link Row} do
	 * not match the according key's type in the {@link Header} {@link Column}
	 * instance or vice versa.
	 * 
	 * @param aRow The {@link Row} to be used for testing.
	 *
	 * @return True in case all elements in this instance match the columns in
	 *         the superset or the argument passed was null.
	 */
	default boolean isSupersetOf( Row<?> aRow ) {
		if ( size() > aRow.size() ) {
			return false;
		}
		int theIndex = 0;
		Object eValue = null;
		for ( Column<?> eColumn : this ) {
			eValue = aRow.get( theIndex );
			if ( !eColumn.getType().isAssignableFrom( eValue.getClass() ) ) {
				return false;
			}
			theIndex++;
		}
		return true;
	}

	/**
	 * Returns a {@link Record} just containing the keys as defined in the
	 * {@link Header} and found in the provided {@link Record}.
	 * 
	 * @param <REC> the generic type
	 * @param aRecord The {@link Record} to use.
	 * 
	 * @return The {@link Record} being a subset of the given {@link Header}.
	 * 
	 * @throws ColumnMismatchException in case the type of a key defined in a
	 *         {@link Column} does not match the type of the according value in
	 *         the {@link Record}.
	 */
	@SuppressWarnings("unchecked")
	default <REC> Record<REC> toIntersection( Record<REC> aRecord ) throws ColumnMismatchException {
		final Record<REC> theRecord = new RecordImpl<>();
		REC eValue;
		for ( Column<?> eColumn : this ) {
			if ( eColumn.contains( aRecord ) ) {
				eValue = (REC) eColumn.get( aRecord );
				theRecord.put( eColumn.getKey(), eValue );
			}
		}
		return theRecord;
	}

	/**
	 * Returns a {@link Record} just containing the keys as defined in the
	 * {@link Header}; keys not found in the provided {@link Record} are
	 * ignored.
	 * 
	 * @param <REC> the generic type
	 * @param aRecord The {@link Record} to use.
	 * 
	 * @return The {@link Record} being a subset of the given {@link Header}.
	 * 
	 * @throws ColumnMismatchException in case the type of a key defined in a
	 *         {@link Column} does not match the type of the according value in
	 *         the {@link Record}.
	 */
	@SuppressWarnings("unchecked")
	default <REC> Record<REC> toSubset( Record<REC> aRecord ) throws ColumnMismatchException {
		final Record<REC> theRecord = new RecordImpl<>();
		REC eValue;
		for ( Column<?> eColumn : this ) {
			eValue = null;
			if ( eColumn.contains( aRecord ) ) {
				eValue = (REC) eColumn.get( aRecord );
			}
			theRecord.put( eColumn.getKey(), eValue );
		}
		return theRecord;
	}
}
