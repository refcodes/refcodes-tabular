// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// /////////////////////////////////////////////////////////////////////////----
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// /////////////////////////////////////////////////////////////////////////----
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// /////////////////////////////////////////////////////////////////////////----
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import java.util.Collection;

import org.refcodes.struct.Keys.MutableKeys;

/**
 * In case no order of the {@link Column} instances is explicitly required
 * (similar to a DB table or a spread sheet, in contrast to a CSV file) then
 * {@link Column} instances are grouped by a {@link ColumnRow} set.
 * <p>
 * {@link ColumnRow} provide additional semantics to the {@link Field} instances
 * stored in a {@link Record}.
 *
 * @param <T> The type managed by the {@link ColumnRow}.
 * @param <C> the generic type
 */
public interface ColumnRow<T, C extends Column<? extends T>> extends Collection<C>, MutableKeys<String, C> {

	/**
	 * With columns.
	 *
	 * @param aColumns the columns
	 * 
	 * @return the columns template
	 */
	@SuppressWarnings("unchecked")
	default ColumnRow<T, C> withColumns( C... aColumns ) {
		for ( C eColumn : aColumns ) {
			add( eColumn );
		}
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default boolean containsValue( Object value ) {
		return values().contains( value );
	}
}
