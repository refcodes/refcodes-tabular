// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import java.text.ParseException;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * A list of {@link Column} instances, for example describing the elements of a
 * CSV file (visually speaking the of the CSV file's header line), is
 * represented by the {@link org.refcodes.tabular.HeaderRow}. The
 * {@link org.refcodes.tabular.HeaderRow} preserves an order for a list of
 * {@link org.refcodes.tabular.Column} instances. A
 * {@link org.refcodes.tabular.HeaderRow} provides the semantics for related
 * {@link org.refcodes.tabular.Row} instances.
 * <p>
 * The {@link #keySet()} method must provide a predictable order as ensured by
 * the {@link LinkedHashSet} class as of the ordered nature of the
 * {@link HeaderRow}.
 *
 * @param <T> The type managed by the {@link HeaderRow}.
 * @param <C> the generic type
 */
public interface HeaderRow<T, C extends Column<? extends T>> extends ColumnRow<T, C>, List<C> {

	/**
	 * Determines the index of the column with the given key or -1 if there is
	 * none such column.
	 * 
	 * @param aKey The key for which to get the column index.
	 * 
	 * @return The index or -1 if there is none such column.
	 */
	int indexOf( String aKey );

	// /////////////////////////////////////////////////////////////////////////
	// CONVERSION:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a {@link Row} of the {@link Record} with the values in the order
	 * of the {@link HeaderRow}. This method is kind of "strong typed" as any
	 * type mismatch between the {@link Column} defined in the {@link HeaderRow}
	 * and the according value of the {@link Record} we get a
	 * {@link ColumnMismatchException}.
	 * 
	 * @param aRecord The {@link Record} to use when creating the {@link Row}.
	 * 
	 * @return The {@link Row} according to the {@link Column} instances of the
	 *         {@link HeaderRow}.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link HeaderRow}.
	 * @throws ColumnMismatchException Thrown in case a value was found in the
	 *         e.g. in a {@link Record} of the wrong type than specified by a
	 *         {@link Column} of the {@link HeaderRow}.
	 */
	Row<T> toRow( Record<? extends T> aRecord ) throws HeaderMismatchException, ColumnMismatchException;

	/**
	 * Similar to {@link #toRow(Record)} with the difference that conversion is
	 * done ignoring the type of the {@link HeaderRow} {@link Column} instances
	 * and the according value(s).
	 * 
	 * @param aRecord The {@link Record} to use when creating the {@link Row}.
	 * 
	 * @return The {@link Row} according to the {@link Column} instances of the
	 *         {@link HeaderRow}.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link HeaderRow}.
	 */
	Row<?> toRowIgnoreType( Record<?> aRecord ) throws HeaderMismatchException;

	/**
	 * Creates a {@link Record} of the {@link Row} with the values in the order
	 * of the {@link HeaderRow}. This method is kind of "strong typed" as any
	 * type mismatch between the {@link Column} defined in the {@link HeaderRow}
	 * and the according value of the {@link Row} we get a
	 * {@link ColumnMismatchException}.
	 * 
	 * @param aRow The {@link Record} to use when creating the {@link Record}.
	 * 
	 * @return The {@link Row} according to the {@link Column} instances of the
	 *         {@link HeaderRow}.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link HeaderRow}.
	 * @throws ColumnMismatchException Thrown in case a value was found in the
	 *         e.g. in a {@link Record} of the wrong type than specified by a
	 *         {@link Column} of the {@link HeaderRow}.
	 */
	Record<T> toRecord( Row<? extends T> aRow ) throws HeaderMismatchException, ColumnMismatchException;

	/**
	 * Converts the {@link String} elements to an according {@link Record}.
	 *
	 * @param aRecord The {@link String} array to convert.
	 * 
	 * @return The accordingly created {@link Record};
	 * 
	 * @throws HeaderMismatchException thrown in case there is a mismatch
	 *         between the given {@link Header} and a {@link Row} (or another
	 *         {@link Header}), i.e. the index for the given key in a header may
	 *         be out of index of a given row or the given key does not exist in
	 *         a {@link Header}.
	 * @throws ParseException in case parsing the input was not possible
	 */
	default Record<T> toRecord( String... aRecord ) throws HeaderMismatchException, ParseException {
		return fromStorageStringsRow( new RowImpl<String[]>( aRecord ) );

	}

	/**
	 * Similar to {@link #toRecord(Row)} with the difference that conversion is
	 * done ignoring the type of the {@link HeaderRow} {@link Column} instances
	 * and the according value(s).
	 * 
	 * @param aRow The {@link Record} to use when creating the {@link Record}.
	 * 
	 * @return The {@link Row} according to the {@link Column} instances of the
	 *         {@link HeaderRow}.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link HeaderRow}.
	 */
	Record<?> toRecordIgnoreType( Row<?> aRow ) throws HeaderMismatchException;

	// /////////////////////////////////////////////////////////////////////////
	// STORAGE STRING:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * A https://www.metacodes.pro {@link Column} implementation might provide
	 * its own text exchange format for the given objects. This method enables
	 * the {@link HeaderRow} to convert a {@link Row} of the given type to a
	 * {@link Row} containing only {@link String} values and via
	 * {@link #fromStorageString(Row)} back to the actual {@link Row}
	 * (bijective). This method may use a {@link Column} instance's method
	 * {@link Column#toStorageString(Object)}.
	 * 
	 * @param aRow the {@link Row} to be converted to a {@link String}
	 *        {@link Row}.
	 * 
	 * @return The {@link String} representation of the value.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link HeaderRow}.
	 * @throws ColumnMismatchException Thrown in case a value was found in the
	 *         e.g. in a {@link Row} of the wrong type than specified by a
	 *         {@link Column} of the {@link HeaderRow}.
	 */
	Row<String> toStorageString( Row<? extends T> aRow ) throws HeaderMismatchException, ColumnMismatchException;

	/**
	 * A https://www.metacodes.pro {@link Column} implementation might provide
	 * its own text exchange format for the given objects. This method enables
	 * the {@link HeaderRow} to convert a {@link Row} containing only
	 * {@link String} objects to a {@link Row} with the given types and via
	 * {@link #toStorageString(Row)} back to the {@link String} {@link Row}
	 * (bijective). This method may use a {@link Column} instance's method
	 * {@link Column#fromStorageString(String)}.
	 * 
	 * @param aStringRow the {@link String} {@link Row} to be converted to a
	 *        type {@link Row}.
	 * 
	 * @return The type representation of the value.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link HeaderRow}.
	 * @throws ParseException in case parsing the input was not possible
	 * @throws UnsupportedOperationException in case this operation is not
	 *         supported.
	 */
	Row<T> fromStorageString( Row<String> aStringRow ) throws HeaderMismatchException, ParseException;

	/**
	 * A https://www.metacodes.pro {@link Column} implementation might provide
	 * its own text exchange format for the given objects. This method enables
	 * the {@link HeaderRow} to convert a {@link Record} of the given type to a
	 * {@link Record} containing only {@link String} values and via
	 * {@link #fromStorageString(Record)} back to the actual {@link Record}
	 * (bijective). This method may use a {@link Column} instance's method
	 * {@link Column#toStorageString(Object)}.
	 * 
	 * @param aRecord the {@link Record} to be converted to a {@link String}
	 *        {@link Record}.
	 * 
	 * @return The {@link String} representation of the value.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link Columns} and the {@link Row}, i.e. the
	 *         index for the given key in the header may be out of index of the
	 *         given row or the given key does not exist in the
	 *         {@link HeaderRow}.
	 * @throws ColumnMismatchException Thrown in case a value was found in the
	 *         e.g. in a {@link Record} of the wrong type than specified by a
	 *         {@link Column} of the {@link HeaderRow}.
	 */
	Record<String> toStorageString( Record<? extends T> aRecord ) throws HeaderMismatchException, ColumnMismatchException;

	/**
	 * A https://www.metacodes.pro {@link Column} implementation might provide
	 * its own text exchange format for the given objects. This method enables
	 * the {@link HeaderRow} to convert a {@link Record} containing only
	 * {@link String} objects to a {@link Record} with the given types and via
	 * {@link #toStorageString(Record)} back to the {@link String}
	 * {@link Record} (bijective). This method may use a {@link Column}
	 * instance's method {@link Column#fromStorageString(String)}.
	 * 
	 * @param aStringRecord the {@link String} {@link Record} to be converted to
	 *        a type {@link Record}.
	 * 
	 * @return The type representation of the value.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link Columns} and the {@link Row}, i.e. the
	 *         index for the given key in the header may be out of index of the
	 *         given row or the given key does not exist in the
	 *         {@link HeaderRow}.
	 * @throws ParseException in case parsing the input was not possible
	 * @throws UnsupportedOperationException in case this operation is not
	 *         supported.
	 */
	Record<T> fromStorageString( Record<String> aStringRecord ) throws HeaderMismatchException, ParseException;

	// /////////////////////////////////////////////////////////////////////////
	// STORAGE STRINGS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * A https://www.metacodes.pro {@link Column} implementation might provide
	 * its own text exchange format for the given objects. This method enables
	 * the {@link HeaderRow} to convert a {@link Row} of the given type to a
	 * {@link Row} containing only {@link String} arrays and via
	 * {@link #fromStorageStrings(Row)} back to the actual {@link Row}
	 * (bijective). This method may use a {@link Column} instance's method
	 * {@link Column#toStorageStrings(Object)}. Supporting {@link String} arrays
	 * enables to address data sinks which support multiple values in one filed
	 * (for example some NoSQL databases such as Amazon's simple DB supports
	 * multiple values in one row's entry).
	 * 
	 * @param aRow the {@link Row} to be converted to a {@link String}
	 *        {@link Row}.
	 * 
	 * @return The {@link String} representation of the value.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link HeaderRow}.
	 * @throws ColumnMismatchException Thrown in case a value was found in the
	 *         e.g. in a {@link Row} of the wrong type than specified by a
	 *         {@link Column} of the {@link HeaderRow}.
	 */
	Row<String[]> toStorageStrings( Row<? extends T> aRow ) throws HeaderMismatchException, ColumnMismatchException;

	/**
	 * A https://www.metacodes.pro {@link Column} implementation might provide
	 * its own text exchange format for the given objects. This method enables
	 * the {@link HeaderRow} to convert a {@link Row} containing only
	 * {@link String} objects to a {@link Row} with the given types and via
	 * {@link #toStorageStrings(Row)} back to the {@link String} array
	 * {@link Row} (bijective). This method may use a {@link Column} instance's
	 * method {@link Column#fromStorageStrings(String[])}. Supporting
	 * {@link String} arrays enables to address data sinks which support
	 * multiple values in one filed (for example some NoSQL databases such as
	 * Amazon's simple DB supports multiple values in one row's entry).
	 * 
	 * @param aStringsRow the {@link String} array {@link Row} to be converted
	 *        to a type {@link Row}.
	 * 
	 * @return The type representation of the value.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link HeaderRow}.
	 * @throws ParseException in case parsing the input was not possible
	 * @throws UnsupportedOperationException in case this operation is not
	 *         supported.
	 */
	Row<T> fromStorageStrings( Row<String[]> aStringsRow ) throws HeaderMismatchException, ParseException;

	/**
	 * A https://www.metacodes.pro {@link Column} implementation might provide
	 * its own text exchange format for the given objects. This method enables
	 * the {@link HeaderRow} to convert a {@link Record} of the given type to a
	 * {@link Record} containing only {@link String} arrays and via
	 * {@link #fromStorageStrings(Record)} back to the actual {@link Record}
	 * (bijective). This method may use a {@link Column} instance's method
	 * {@link Column#toStorageStrings(Object)}. Supporting {@link String} arrays
	 * enables to address data sinks which support multiple values in one filed
	 * (for example some NoSQL databases such as Amazon's simple DB supports
	 * multiple values in one row's entry).
	 * 
	 * @param aRecord the {@link Record} to be converted to a {@link String}
	 *        {@link Record}.
	 * 
	 * @return The {@link String} representation of the value.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link Columns} and the {@link Row}, i.e. the
	 *         index for the given key in the header may be out of index of the
	 *         given row or the given key does not exist in the
	 *         {@link HeaderRow}.
	 * @throws ColumnMismatchException Thrown in case a value was found in the
	 *         e.g. in a {@link Record} of the wrong type than specified by a
	 *         {@link Column} of the {@link HeaderRow}.
	 */
	Record<String[]> toStorageStrings( Record<? extends T> aRecord ) throws HeaderMismatchException, ColumnMismatchException;

	/**
	 * A https://www.metacodes.pro {@link Column} implementation might provide
	 * its own text exchange format for the given objects. This method enables
	 * the {@link HeaderRow} to convert a {@link Record} containing only
	 * {@link String} objects to a {@link Record} with the given types and via
	 * {@link #toStorageStrings(Record)} back to the {@link String}
	 * {@link Record} (bijective). This method may use a {@link Column}
	 * instance's method {@link Column#fromStorageStrings(String[])}. Supporting
	 * {@link String} arrays enables to address data sinks which support
	 * multiple values in one filed (for example some NoSQL databases such as
	 * Amazon's simple DB supports multiple values in one row's entry).
	 * 
	 * @param aStringsRecord the {@link String} array {@link Record} to be
	 *        converted to a type {@link Record}.
	 * 
	 * @return The type representation of the value.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link Columns} and the {@link Row}, i.e. the
	 *         index for the given key in the header may be out of index of the
	 *         given row or the given key does not exist in the
	 *         {@link HeaderRow}.
	 * @throws ParseException in case parsing the input was not possible
	 * @throws UnsupportedOperationException in case this operation is not
	 *         supported.
	 */
	Record<T> fromStorageStrings( Record<String[]> aStringsRecord ) throws HeaderMismatchException, ParseException;

	// /////////////////////////////////////////////////////////////////////////
	// PRINTABLE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * A https://www.metacodes.pro {@link Column} implementation might provide
	 * its own printable format of the given objects; for example a human
	 * readable text representation of the value (or in very
	 * https://www.metacodes.proized cases even enriched with ANSI
	 * Escape-Codes). This method enables the {@link HeaderRow} to convert a
	 * value of the given type to a human readable text. The human readable
	 * text, in comparison to the method {@link #toStorageString(Record)} (or
	 * {@link #toStorageStrings(Record)}) is not intended to be converted back
	 * to the actual value (not bijective). This method may use a {@link Column}
	 * instance's method {@link Column#toPrintable(Object)}; it also might
	 * enrich the output of the {@link Column#toPrintable(Object)} with device
	 * specific additional data such as ANSI Escape-Codes and with information
	 * regarding the {@link Record} as a whole and not just be regarding a
	 * single value.
	 * 
	 * @param aRecord the {@link Record} to be converted to a human readable
	 *        text {@link Record}.
	 * 
	 * @return The human readable representation of the {@link Record}.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link Columns} and the {@link Row}, i.e. the
	 *         index for the given key in the header may be out of index of the
	 *         given row or the given key does not exist in the
	 *         {@link HeaderRow}.
	 * @throws ColumnMismatchException Thrown in case a value was found in the
	 *         e.g. in a {@link Record} of the wrong type than specified by a
	 *         {@link Column} of the {@link HeaderRow}.
	 */
	Record<String> toPrintable( Record<? extends T> aRecord ) throws HeaderMismatchException, ColumnMismatchException;

	/**
	 * A https://www.metacodes.pro {@link Column} implementation might provide
	 * its own printable format of the given objects; for example a human
	 * readable text representation of the value (or in very
	 * https://www.metacodes.proized cases even enriched with ANSI
	 * Escape-Codes). This method enables the {@link HeaderRow} to convert a
	 * value of the given type to a human readable text. The human readable
	 * text, in comparison to the method {@link #toStorageString(Row)} (or
	 * {@link #toStorageStrings(Row)}) is not intended to be converted back to
	 * the actual value (not bijective). This method may use a {@link Column}
	 * instance's method {@link Column#toPrintable(Object)}; it also might
	 * enrich the output of the {@link Column#toPrintable(Object)} with device
	 * specific additional data such as ANSI Escape-Codes and with information
	 * regarding the {@link Row} as a whole and not just be regarding a single
	 * value.
	 * 
	 * @param aRow the {@link Row} to be converted to a human readable text
	 *        {@link Row}.
	 * 
	 * @return The human readable representation of the {@link Row}.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link HeaderRow}.
	 * @throws ColumnMismatchException Thrown in case a value was found in the
	 *         e.g. in a {@link Row} of the wrong type than specified by a
	 *         {@link Column} of the {@link HeaderRow}.
	 */
	Row<String> toPrintable( Row<? extends T> aRow ) throws HeaderMismatchException, ColumnMismatchException;

	// /////////////////////////////////////////////////////////////////////////
	// PRINTABLES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * A https://www.metacodes.pro {@link Column} implementation might provide
	 * its own printable format of the given objects; for example a human
	 * readable {@link String} array representation of the value (or in very
	 * https://www.metacodes.proized cases even enriched with ANSI
	 * Escape-Codes). This method enables the {@link Header} to convert a value
	 * of the given type to a human readable {@link String} arrays. The human
	 * readable {@link String} arrays, in comparison to the method
	 * {@link #toStorageStrings(Record)} (or {@link #toStorageString(Record)})
	 * is not intended to be converted back to the actual value (not bijective).
	 * This method may use a {@link Column} instance's method
	 * {@link Column#toPrintable(Object)}; it also might enrich the output of
	 * the {@link Column#toPrintable(Object)} with device specific additional
	 * data such as ANSI Escape-Codes and with information regarding the
	 * {@link Record} as a whole and not just be regarding a single value.
	 * Supporting {@link String} arrays enables addressing output devices to
	 * beatify output and enhance readability, with proper formatting, by using
	 * multiple rows for one value.
	 *
	 * @param aStringRecord the string record
	 * 
	 * @return The human readable representation of the {@link Record}.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link Columns} and the {@link Row}, i.e. the
	 *         index for the given key in the header may be out of index of the
	 *         given row or the given key does not exist in the {@link Header}.
	 * @throws ParseException the parse exception
	 */
	// Record<String[]> toPrintables( Record<? extends T> aRecord ) throws
	// HeaderMismatchException, ColumnMismatchException;

	/**
	 * A https://www.metacodes.pro {@link Column} implementation might provide
	 * its own printable format of the given objects; for example a human
	 * readable {@link String} array representation of the value (or in very
	 * https://www.metacodes.proized cases even enriched with ANSI
	 * Escape-Codes). This method enables the {@link Header} to convert a value
	 * of the given type to a human readable {@link String} arrays. The human
	 * readable {@link String} arrays, in comparison to the method
	 * {@link #toStorageStrings(Row)} (or {@link #toStorageString(Row)}) is not
	 * intended to be converted back to the actual value (not bijective). This
	 * method may use a {@link Column} instance's method
	 * {@link Column#toPrintable(Object)}; it also might enrich the output of
	 * the {@link Column#toPrintable(Object)} with device specific additional
	 * data such as ANSI Escape-Codes and with information regarding the
	 * {@link Row} as a whole and not just be regarding a single value.
	 * Supporting {@link String} arrays enables addressing output devices to
	 * beatify output and enhance readability, with proper formatting, by using
	 * multiple rows for one value.
	 * 
	 * @param aRow the {@link Row} to be converted to a human readable
	 *        {@link String} array {@link Row}.
	 * 
	 * @return The human readable representation of the {@link Row}.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link Header}.
	 * @throws ColumnMismatchException Thrown in case a value was found in the
	 *         e.g. in a {@link Row} of the wrong type than specified by a
	 *         {@link Column} of the {@link Header}.
	 */
	// Row<String[]> toPrintables( Row<? extends T> aRow ) throws
	// HeaderMismatchException, ColumnMismatchException;

	// /////////////////////////////////////////////////////////////////////////
	// CONVENIANCE STORAGE STRING:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * A https://www.metacodes.pro {@link Column} implementation might provide
	 * its own text exchange format for the given objects. This method enables
	 * the {@link HeaderRow} to convert a {@link Record} containing only
	 * {@link String} objects to a {@link Row} with the given types and via
	 * {@link #toStorageStringRecord(Row)} back to the {@link String}
	 * {@link Record} (bijective). This method may use a {@link Column}
	 * instance's method {@link Column#fromStorageString(String)}.
	 * 
	 * @param aStringRecord the {@link String} {@link Record} to be converted to
	 *        a type {@link Row}.
	 * 
	 * @return The type {@link Row} representation of the {@link String}
	 *         {@link Record}.
	 * 
	 * @throws ParseException in case parsing the input was not possible
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link HeaderRow}.
	 */
	Row<T> fromStorageStringRecord( Record<String> aStringRecord ) throws HeaderMismatchException, ParseException;

	/**
	 * A https://www.metacodes.pro {@link Column} implementation might provide
	 * its own text exchange format for the given objects. This method enables
	 * the {@link HeaderRow} to convert a {@link Row} of the given type to a
	 * {@link Record} containing only {@link String} values and via
	 * {@link #fromStorageStringRecord(Record)} back to the actual {@link Row}
	 * (bijective). This method may use a {@link Column} instance's method
	 * {@link Column#toStorageString(Object)}.
	 * 
	 * @param aRow The {@link Row} to be converted to a {@link String}
	 *        {@link Record}.
	 * 
	 * @return The {@link String} representation of the value.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link HeaderRow}.
	 * @throws ColumnMismatchException Thrown in case a value was found in the
	 *         e.g. in a {@link Row} of the wrong type than specified by a
	 *         {@link Column} of the {@link HeaderRow}.
	 */
	Record<String> toStorageStringRecord( Row<? extends T> aRow ) throws HeaderMismatchException, ColumnMismatchException;

	/**
	 * A https://www.metacodes.pro {@link Column} implementation might provide
	 * its own text exchange format for the given objects. This method enables
	 * the {@link HeaderRow} to convert a {@link Row} containing only
	 * {@link String} objects to a {@link Record} with the given types and via
	 * {@link #toStorageStringRow(Record)} back to the {@link String}
	 * {@link Row} (bijective). This method may use a {@link Column} instance's
	 * method {@link Column#fromStorageString(String)}.
	 *
	 * @param aStringRow the {@link String} {@link Row} to be converted to a
	 *        type {@link Record}.
	 * 
	 * @return The type representation of the value.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link HeaderRow}.
	 * @throws ParseException in case parsing the input was not possible
	 * @throws UnsupportedOperationException in case this operation is not
	 *         supported.
	 */
	Record<T> fromStorageStringRow( Row<String> aStringRow ) throws HeaderMismatchException, ParseException;

	/**
	 * A https://www.metacodes.pro {@link Column} implementation might provide
	 * its own text exchange format for the given objects. This method enables
	 * the {@link HeaderRow} to convert a {@link Record} of the given type to a
	 * {@link Row} containing only {@link String} values and via
	 * {@link #fromStorageStringRecord(Record)} back to the actual
	 * {@link Record} (bijective). This method may use a {@link Column}
	 * instance's method {@link Column#toStorageString(Object)}.
	 * 
	 * @param aRecord The {@link Row} to be converted to a {@link String}
	 *        {@link Record}.
	 * 
	 * @return The {@link String} representation of the value.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link HeaderRow}.
	 * @throws ColumnMismatchException Thrown in case a value was found in the
	 *         e.g. in a {@link Record} of the wrong type than specified by a
	 *         {@link Column} of the {@link HeaderRow}.
	 */
	Row<String> toStorageStringRow( Record<? extends T> aRecord ) throws HeaderMismatchException, ColumnMismatchException;

	// /////////////////////////////////////////////////////////////////////////
	// CONVENIANCE STORAGE STRINGS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * A https://www.metacodes.pro {@link Column} implementation might provide
	 * its own text exchange format for the given objects. This method enables
	 * the {@link HeaderRow} to convert a {@link Record} containing only
	 * {@link String} arrays to a {@link Row} with the given types and via
	 * {@link #toStorageStringsRecord(Row)} back to the {@link String}
	 * {@link Record} (bijective). This method may use a {@link Column}
	 * instance's method {@link Column#fromStorageStrings(String[])}. Supporting
	 * {@link String} arrays enables to address data sinks which support
	 * multiple values in one filed (for example some NoSQL databases such as
	 * Amazon's simple DB supports multiple values in one row's entry).
	 *
	 * @param aStringsRecord the {@link String} {@link Record} to be converted
	 *        to a type {@link Row}.
	 * 
	 * @return The type {@link Row} representation of the {@link String}
	 *         {@link Record}.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link HeaderRow}.
	 * @throws ParseException in case parsing the input was not possible
	 */
	Row<T> fromStorageStringsRecord( Record<String[]> aStringsRecord ) throws HeaderMismatchException, ParseException;

	/**
	 * A https://www.metacodes.pro {@link Column} implementation might provide
	 * its own text exchange format for the given objects. This method enables
	 * the {@link HeaderRow} to convert a {@link Row} of the given type to a
	 * {@link Record} containing only {@link String} arrays and via
	 * {@link #fromStorageStringsRecord(Record)} back to the actual {@link Row}
	 * (bijective). This method may use a {@link Column} instance's method
	 * {@link Column#toStorageStrings(Object)}. Supporting {@link String} arrays
	 * enables to address data sinks which support multiple values in one filed
	 * (for example some NoSQL databases such as Amazon's simple DB supports
	 * multiple values in one row's entry).
	 * 
	 * @param aRow The {@link Row} to be converted to a {@link String}
	 *        {@link Record}.
	 * 
	 * @return The {@link String} representation of the value.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link HeaderRow}.
	 * @throws ColumnMismatchException Thrown in case a value was found in the
	 *         e.g. in a {@link Row} of the wrong type than specified by a
	 *         {@link Column} of the {@link HeaderRow}.
	 */
	Record<String[]> toStorageStringsRecord( Row<? extends T> aRow ) throws HeaderMismatchException, ColumnMismatchException;

	/**
	 * A https://www.metacodes.pro {@link Column} implementation might provide
	 * its own text exchange format for the given objects. This method enables
	 * the {@link HeaderRow} to convert a {@link Row} containing only
	 * {@link String} arrays to a {@link Record} with the given types and via
	 * {@link #toStorageStringsRow(Record)} back to the {@link String}
	 * {@link Row} (bijective). This method may use a {@link Column} instance's
	 * method {@link Column#fromStorageStrings(String[])}. Supporting
	 * {@link String} arrays enables to address data sinks which support
	 * multiple values in one filed (for example some NoSQL databases such as
	 * Amazon's simple DB supports multiple values in one row's entry).
	 *
	 * @param aStringsRow the {@link String} {@link Row} to be converted to a
	 *        type {@link Record}.
	 * 
	 * @return The type representation of the value.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link HeaderRow}.
	 * @throws ParseException in case parsing the input was not possible
	 * @throws UnsupportedOperationException in case this operation is not
	 *         supported.
	 */
	Record<T> fromStorageStringsRow( Row<String[]> aStringsRow ) throws HeaderMismatchException, ParseException;

	/**
	 * A https://www.metacodes.pro {@link Column} implementation might provide
	 * its own text exchange format for the given objects. This method enables
	 * the {@link HeaderRow} to convert a {@link Record} of the given type to a
	 * {@link Row} containing only {@link String} arrays and via
	 * {@link #fromStorageStringsRecord(Record)} back to the actual
	 * {@link Record} (bijective). This method may use a {@link Column}
	 * instance's method {@link Column#toStorageStrings(Object)}. Supporting
	 * {@link String} arrays enables to address data sinks which support
	 * multiple values in one filed (for example some NoSQL databases such as
	 * Amazon's simple DB supports multiple values in one row's entry).
	 * 
	 * @param aRecord The {@link Row} to be converted to a {@link String}
	 *        {@link Record}.
	 * 
	 * @return The {@link String} representation of the value.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link HeaderRow}.
	 * @throws ColumnMismatchException Thrown in case a value was found in the
	 *         e.g. in a {@link Record} of the wrong type than specified by a
	 *         {@link Column} of the {@link HeaderRow}.
	 */
	Row<String[]> toStorageStringsRow( Record<? extends T> aRecord ) throws HeaderMismatchException, ColumnMismatchException;

	// /////////////////////////////////////////////////////////////////////////
	// CONVENIANCE PRINTABLE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * A https://www.metacodes.pro {@link Column} implementation might provide
	 * its own printable format of the given objects; for example a human
	 * readable text representation of the value (or in very
	 * https://www.metacodes.proized cases even enriched with ANSI
	 * Escape-Codes). This method enables the {@link HeaderRow} to convert a
	 * value of the given type to a human readable text. The human readable
	 * text, in comparison to the method {@link #toRow(Record)} (or
	 * {@link #toStorageStringsRecord(Row)} is not intended to be converted back
	 * to the actual value (not bijective). This method may use a {@link Column}
	 * instance's method {@link Column#toPrintable(Object)}; it also might
	 * enrich the output of the {@link Column#toPrintable(Object)} with device
	 * specific additional data such as ANSI Escape-Codes and with information
	 * regarding the {@link Record} as a whole and not just be regarding a
	 * single value.
	 * 
	 * @param aRecord the {@link Record} to be converted to a human readable
	 *        text {@link Row}.
	 * 
	 * @return The human readable {@link Row} representation of the
	 *         {@link Record}.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link HeaderRow}.
	 * @throws ColumnMismatchException Thrown in case a value was found in the
	 *         e.g. in a {@link Record} of the wrong type than specified by a
	 *         {@link Column} of the {@link HeaderRow}.
	 */
	Row<String> toPrintableRow( Record<? extends T> aRecord ) throws HeaderMismatchException, ColumnMismatchException;

	/**
	 * A https://www.metacodes.pro {@link Column} implementation might provide
	 * its own printable format of the given objects; for example a human
	 * readable text representation of the value (or in very
	 * https://www.metacodes.proized cases even enriched with ANSI
	 * Escape-Codes). This method enables the {@link HeaderRow} to convert a
	 * {@link Row} of the given type to a human readable text {@link Record}.
	 * The human readable text, in comparison to the method
	 * {@link #toStorageString(Row)} (or {@link #toRecordIgnoreType(Row)}) is
	 * not intended to be converted back to the actual value (not bijective).
	 * This method may use a {@link Column} instance's method
	 * {@link Column#toPrintable(Object)}; it also might enrich the output of
	 * the {@link Column#toPrintable(Object)} with device specific additional
	 * data such as ANSI Escape-Codes and with information regarding the
	 * {@link Row} as a whole and not just be regarding a single value.
	 * 
	 * @param aRow the {@link Row} to be converted to a human readable text
	 *        {@link Record}.
	 * 
	 * @return The human readable {@link Record} representation of the
	 *         {@link Row}.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link HeaderRow}.
	 * @throws ColumnMismatchException Thrown in case a value was found in the
	 *         e.g. in a {@link Row} of the wrong type than specified by a
	 *         {@link Column} of the {@link HeaderRow}.
	 */
	Record<String> toPrintableRecord( Row<? extends T> aRow ) throws HeaderMismatchException, ColumnMismatchException;

	// /////////////////////////////////////////////////////////////////////////
	// CONVENIANCE PRINTABLES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * A https://www.metacodes.pro {@link Column} implementation might provide
	 * its own printable format of the given objects; for example a human
	 * readable {@link String} array representation of the value (or in very
	 * https://www.metacodes.proized cases even enriched with ANSI
	 * Escape-Codes). This method enables the {@link Header} to convert a value
	 * of the given type to a human readable {@link String} arrays. The human
	 * readable {@link String} arrays, in comparison to the method
	 * {@link #toStorageStrings(Record)} (or {@link #toStorageString(Record)})
	 * is not intended to be converted back to the actual value (not bijective).
	 * This method may use a {@link Column} instance's method
	 * {@link Column#toPrintable(Object)}; it also might enrich the output of
	 * the {@link Column#toPrintable(Object)} with device specific additional
	 * data such as ANSI Escape-Codes and with information regarding the
	 * {@link Record} as a whole and not just be regarding a single value.
	 * Supporting {@link String} arrays enables addressing output devices to
	 * beatify output and enhance readability, with proper formatting, by using
	 * multiple rows for one value.
	 * 
	 * @param aRecord the {@link Record} to be converted to a human readable
	 *        {@link String} array {@link Record}.
	 * 
	 * @return The human readable {@link Row} representation of the
	 *         {@link Record}.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link Columns} and the {@link Row}, i.e. the
	 *         index for the given key in the header may be out of index of the
	 *         given row or the given key does not exist in the {@link Header}.
	 * @throws ColumnMismatchException Thrown in case a value was found in the
	 *         e.g. in a {@link Record} of the wrong type than specified by a
	 *         {@link Column} of the {@link Header}.
	 */
	// Row<String[]> toPrintablesRow( Record<? extends T> aRecord ) throws
	// HeaderMismatchException, ColumnMismatchException;

	/**
	 * A https://www.metacodes.pro {@link Column} implementation might provide
	 * its own printable format of the given objects; for example a human
	 * readable {@link String} array representation of the value (or in very
	 * https://www.metacodes.proized cases even enriched with ANSI
	 * Escape-Codes). This method enables the {@link Header} to convert a value
	 * of the given type to a human readable {@link String} arrays. The human
	 * readable {@link String} arrays, in comparison to the method
	 * {@link #toStorageStrings(Row)} (or {@link #toStorageString(Row)}) is not
	 * intended to be converted back to the actual value (not bijective). This
	 * method may use a {@link Column} instance's method
	 * {@link Column#toPrintable(Object)}; it also might enrich the output of
	 * the {@link Column#toPrintable(Object)} with device specific additional
	 * data such as ANSI Escape-Codes and with information regarding the
	 * {@link Row} as a whole and not just be regarding a single value.
	 * Supporting {@link String} arrays enables addressing output devices to
	 * beatify output and enhance readability, with proper formatting, by using
	 * multiple rows for one value.
	 * 
	 * @param aRow the {@link Row} to be converted to a human readable
	 *        {@link String} array {@link Row}.
	 * 
	 * @return The human readable representation {@link Record} of the
	 *         {@link Row}.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link Header}.
	 * @throws ColumnMismatchException Thrown in case a value was found in the
	 *         e.g. in a {@link Row} of the wrong type than specified by a
	 *         {@link Column} of the {@link Header}.
	 */
	// Record<String[]> toPrintablesRecord( Row<? extends T> aRow ) throws
	// HeaderMismatchException, ColumnMismatchException;
}
