// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package org.refcodes.tabular;

import java.text.ParseException;

import org.refcodes.textual.ColumnFormatMetrics;
import org.refcodes.textual.ColumnSetupMetrics;
import org.refcodes.textual.ColumnSetupMetricsImpl;
import org.refcodes.textual.ColumnWidthMetrics;
import org.refcodes.textual.ColumnWidthType;
import org.refcodes.textual.EscapeCodeFactory;
import org.refcodes.textual.HorizAlignTextMode;
import org.refcodes.textual.MoreTextMode;
import org.refcodes.textual.SplitTextMode;
import org.refcodes.textual.TextFormatMode;

/**
 * Decorator implementation of the HeaderSetupMetrics interface.
 * 
 * @param <T> The type managed by the {@link Column}.
 */
public class FormattedColumnDecorator<T> implements FormattedColumn<T> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final Column<T> _column;

	private final ColumnSetupMetrics _columnSetupMetrics;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new formatted column decorator.
	 *
	 * @param aKey the key
	 * @param aType the type
	 */
	protected FormattedColumnDecorator( String aKey, Class<T> aType ) {
		_column = new ObjectColumn<>( aKey, aType );
		_columnSetupMetrics = new ColumnSetupMetricsImpl();
	}

	/**
	 * Wrapper for the provided {@link Column} decorating it with additional
	 * {@link ColumnSetupMetrics} functionality.
	 *
	 * @param aColumn The {@link Column} to be decorated.
	 */
	public FormattedColumnDecorator( Column<T> aColumn ) {
		_column = aColumn;
		_columnSetupMetrics = new ColumnSetupMetricsImpl();
	}

	/**
	 * Wrapper for the provided {@link ColumnSetupMetrics} decorating it with
	 * additional {@link Column} functionality.
	 * 
	 * @param aKey The key for the {@link Column}.
	 * @param aType The type to be used.
	 * @param aColumnSetupMetrics The {@link ColumnSetupMetrics} to be
	 *        decorated.
	 */
	public FormattedColumnDecorator( String aKey, Class<T> aType, ColumnSetupMetrics aColumnSetupMetrics ) {
		_column = new ObjectColumn<>( aKey, aType );
		_columnSetupMetrics = aColumnSetupMetrics;
	}

	/**
	 * Wrapper for the provided {@link Column} and the provided
	 * {@link ColumnSetupMetrics} decorating it with each other's additional
	 * functionality.
	 * 
	 * @param aColumn The {@link Column} to be decorated.
	 * @param aColumnSetupMetrics The {@link ColumnSetupMetrics} to be
	 *        decorated.
	 */
	public FormattedColumnDecorator( Column<T> aColumn, ColumnSetupMetrics aColumnSetupMetrics ) {
		_column = aColumn;
		_columnSetupMetrics = aColumnSetupMetrics;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return _columnSetupMetrics.getName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isVisible() {
		return _columnSetupMetrics.isVisible();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getColumnWidth() {
		return _columnSetupMetrics.getColumnWidth();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnSetupMetrics withVisible( boolean isVisible ) {
		return _columnSetupMetrics.withVisible( isVisible );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setName( String aName ) {
		_columnSetupMetrics.setName( aName );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setVisible( boolean isVisible ) {
		_columnSetupMetrics.setVisible( isVisible );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setColumnWidth( int aColumnWidth ) {
		_columnSetupMetrics.setColumnWidth( aColumnWidth );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnSetupMetrics withName( String aName ) {
		return _columnSetupMetrics.withName( aName );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnSetupMetrics withColumnWidth( int aColumnWidth ) {
		return _columnSetupMetrics.withColumnWidth( aColumnWidth );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void show() {
		_columnSetupMetrics.show();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnSetupMetrics withShow() {
		return _columnSetupMetrics.withShow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnWidthType getColumnWidthType() {
		return _columnSetupMetrics.getColumnWidthType();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void hide() {
		_columnSetupMetrics.hide();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnSetupMetrics withHide() {
		return _columnSetupMetrics.withHide();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnSetupMetrics withEscapeCode( String aEscapeCode ) {
		return _columnSetupMetrics.withEscapeCode( aEscapeCode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setColumnWidthType( ColumnWidthType aColumnWidthType ) {
		_columnSetupMetrics.setColumnWidthType( aColumnWidthType );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnSetupMetrics withHeaderEscapeCode( String aEscapeCode ) {
		return _columnSetupMetrics.withHeaderEscapeCode( aEscapeCode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void fromColumnWidthMetrics( ColumnWidthMetrics aColumnWidthMetrics ) {
		_columnSetupMetrics.fromColumnWidthMetrics( aColumnWidthMetrics );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnSetupMetrics withRowEscapeCode( String aEscapeCode ) {
		return _columnSetupMetrics.withRowEscapeCode( aEscapeCode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnSetupMetrics withColumnWidthType( ColumnWidthType aColumnWidthType ) {
		return _columnSetupMetrics.withColumnWidthType( aColumnWidthType );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnSetupMetrics withHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode ) {
		return _columnSetupMetrics.withHorizAlignTextMode( aHorizAlignTextMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnSetupMetrics withHeaderHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode ) {
		return _columnSetupMetrics.withHeaderHorizAlignTextMode( aHorizAlignTextMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnSetupMetrics withRowHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode ) {
		return _columnSetupMetrics.withRowHorizAlignTextMode( aHorizAlignTextMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnSetupMetrics withMoreTextMode( MoreTextMode aMoreTextMode ) {
		return _columnSetupMetrics.withMoreTextMode( aMoreTextMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnSetupMetrics withHeaderMoreTextMode( MoreTextMode aMoreTextMode ) {
		return _columnSetupMetrics.withHeaderMoreTextMode( aMoreTextMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setEscapeCodeFactory( EscapeCodeFactory aEscapeCodeFactory ) {
		_columnSetupMetrics.setEscapeCodeFactory( aEscapeCodeFactory );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnSetupMetrics withRowMoreTextMode( MoreTextMode aMoreTextMode ) {
		return _columnSetupMetrics.withRowMoreTextMode( aMoreTextMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnSetupMetrics withTextFormatMode( TextFormatMode aTextFormatMode ) {
		return _columnSetupMetrics.withTextFormatMode( aTextFormatMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnSetupMetrics withSplitTextMode( SplitTextMode aSplitTextMode ) {
		return _columnSetupMetrics.withSplitTextMode( aSplitTextMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnSetupMetrics withHeaderSplitTextMode( SplitTextMode aSplitTextMode ) {
		return _columnSetupMetrics.withHeaderSplitTextMode( aSplitTextMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnSetupMetrics withRowSplitTextMode( SplitTextMode aSplitTextMode ) {
		return _columnSetupMetrics.withRowSplitTextMode( aSplitTextMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnSetupMetrics withHeaderTextFormatMode( TextFormatMode aTextFormatMode ) {
		return _columnSetupMetrics.withHeaderTextFormatMode( aTextFormatMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnSetupMetrics withRowTextFormatMode( TextFormatMode aTextFormatMode ) {
		return _columnSetupMetrics.withRowTextFormatMode( aTextFormatMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnSetupMetrics withEscapeCodeFactory( EscapeCodeFactory aEscapeCodeFactory ) {
		return _columnSetupMetrics.withEscapeCodeFactory( aEscapeCodeFactory );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHeaderEscapeCodeFactory( EscapeCodeFactory aEscapeCodeFactory ) {
		_columnSetupMetrics.setHeaderEscapeCodeFactory( aEscapeCodeFactory );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnSetupMetrics withHeaderEscapeCodeFactory( EscapeCodeFactory aEscapeCodeFactory ) {
		return _columnSetupMetrics.withHeaderEscapeCodeFactory( aEscapeCodeFactory );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnSetupMetrics withRowEscapeCodeFactory( EscapeCodeFactory aEscapeCodeFactory ) {
		return _columnSetupMetrics.withRowEscapeCodeFactory( aEscapeCodeFactory );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EscapeCodeFactory getHeaderEscapeCodeFactory() {
		return _columnSetupMetrics.getHeaderEscapeCodeFactory();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void fromColumnSetupMetrics( ColumnSetupMetrics aColumnSetupMetrics ) {
		_columnSetupMetrics.fromColumnSetupMetrics( aColumnSetupMetrics );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toHeaderEscapeCode( Object aIdentifier ) {
		return _columnSetupMetrics.toHeaderEscapeCode( aIdentifier );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setRowEscapeCodeFactory( EscapeCodeFactory aEscapeCodeFactory ) {
		_columnSetupMetrics.setRowEscapeCodeFactory( aEscapeCodeFactory );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EscapeCodeFactory getRowEscapeCodeFactory() {
		return _columnSetupMetrics.getRowEscapeCodeFactory();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toRowEscapeCode( Object aIdentifier ) {
		return _columnSetupMetrics.toRowEscapeCode( aIdentifier );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setEscapeCode( String aEscapeCode ) {
		_columnSetupMetrics.setEscapeCode( aEscapeCode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHeaderEscapeCode( String aEscapeCode ) {
		_columnSetupMetrics.setHeaderEscapeCode( aEscapeCode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getHeaderEscapeCode() {
		return _columnSetupMetrics.getHeaderEscapeCode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setRowEscapeCode( String aEscapeCode ) {
		_columnSetupMetrics.setRowEscapeCode( aEscapeCode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getRowEscapeCode() {
		return _columnSetupMetrics.getRowEscapeCode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode ) {
		_columnSetupMetrics.setHorizAlignTextMode( aHorizAlignTextMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHeaderHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode ) {
		_columnSetupMetrics.setHeaderHorizAlignTextMode( aHorizAlignTextMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HorizAlignTextMode getHeaderHorizAlignTextMode() {
		return _columnSetupMetrics.getHeaderHorizAlignTextMode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setRowHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode ) {
		_columnSetupMetrics.setRowHorizAlignTextMode( aHorizAlignTextMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HorizAlignTextMode getRowHorizAlignTextMode() {
		return _columnSetupMetrics.getRowHorizAlignTextMode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMoreTextMode( MoreTextMode aMoreTextMode ) {
		_columnSetupMetrics.setMoreTextMode( aMoreTextMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHeaderMoreTextMode( MoreTextMode aMoreTextMode ) {
		_columnSetupMetrics.setHeaderMoreTextMode( aMoreTextMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MoreTextMode getHeaderMoreTextMode() {
		return _columnSetupMetrics.getHeaderMoreTextMode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setRowMoreTextMode( MoreTextMode aMoreTextMode ) {
		_columnSetupMetrics.setRowMoreTextMode( aMoreTextMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MoreTextMode getRowMoreTextMode() {
		return _columnSetupMetrics.getRowMoreTextMode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setTextFormatMode( TextFormatMode aTextFormatMode ) {
		_columnSetupMetrics.setTextFormatMode( aTextFormatMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setSplitTextMode( SplitTextMode aSplitTextMode ) {
		_columnSetupMetrics.setSplitTextMode( aSplitTextMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHeaderSplitTextMode( SplitTextMode aSplitTextMode ) {
		_columnSetupMetrics.setHeaderSplitTextMode( aSplitTextMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SplitTextMode getHeaderSplitTextMode() {
		return _columnSetupMetrics.getHeaderSplitTextMode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setRowSplitTextMode( SplitTextMode aSplitTextMode ) {
		_columnSetupMetrics.setRowSplitTextMode( aSplitTextMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SplitTextMode getRowSplitTextMode() {
		return _columnSetupMetrics.getRowSplitTextMode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHeaderTextFormatMode( TextFormatMode aTextFormatMode ) {
		_columnSetupMetrics.setHeaderTextFormatMode( aTextFormatMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TextFormatMode getHeaderTextFormatMode() {
		return _columnSetupMetrics.getHeaderTextFormatMode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setRowTextFormatMode( TextFormatMode aTextFormatMode ) {
		_columnSetupMetrics.setRowTextFormatMode( aTextFormatMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TextFormatMode getRowTextFormatMode() {
		return _columnSetupMetrics.getRowTextFormatMode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void fromColumnFormatMetrics( ColumnFormatMetrics aColumnFormatMetrics ) {
		_columnSetupMetrics.fromColumnFormatMetrics( aColumnFormatMetrics );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<T> getType() {
		return _column.getType();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getKey() {
		return _column.getKey();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toStorageString( T aValue ) {
		return _column.toStorageString( aValue );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toStorageStrings( T aValue ) {
		return _column.toStorageStrings( aValue );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T fromStorageString( String aStringValue ) throws ParseException {
		return _column.fromStorageString( aStringValue );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T fromStorageStrings( String[] aStringArray ) throws ParseException {
		return _column.fromStorageStrings( aStringArray );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toPrintable( T aValue ) {
		return _column.toPrintable( aValue );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean contains( Record<?> aRecord ) {
		return _column.contains( aRecord );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T get( Record<?> aRecord ) throws ColumnMismatchException {
		return _column.get( aRecord );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T remove( Record<?> aRecord ) throws ColumnMismatchException {
		return _column.remove( aRecord );
	}
}
