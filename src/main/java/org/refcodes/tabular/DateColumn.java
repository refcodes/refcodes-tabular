// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Date;

import org.refcodes.time.DateFormat;
import org.refcodes.time.DateFormats;
import org.refcodes.time.DateUtility;

/**
 * {@link DateColumn} implements the {@link Column} with support for the
 * {@link Date} type and various default or custom date formats.
 */
public class DateColumn extends AbstractColumn<Date> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private DateTimeFormatter[] _dateFormats;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructor of the {@link DateColumn} with support for the default
	 * {@link SimpleDateFormat} instances as defined in the
	 * {@link DateFormats#DEFAULT_DATE_FORMATS}.
	 * 
	 * @param aKey The key to be used.
	 */
	public DateColumn( String aKey ) {
		this( aKey, DateFormats.DEFAULT_DATE_FORMATS.getDateFormats() );
	}

	/**
	 * Constructor of the {@link DateColumn} with support for the provided
	 * {@link SimpleDateFormat} instances.
	 * 
	 * @param aKey The key to be used.
	 * @param aDateFormats The {@link SimpleDateFormat} instances used for
	 *        parsing date strings.
	 */
	public DateColumn( String aKey, DateTimeFormatter... aDateFormats ) {
		super( aKey, Date.class );
		_dateFormats = new DateTimeFormatter[aDateFormats.length];
		for ( int i = 0; i < aDateFormats.length; i++ ) {
			_dateFormats[i] = aDateFormats[i];
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toStorageStrings( Date aValue ) {
		return new String[] { _dateFormats[0].format( Instant.ofEpochMilli( aValue.getTime() ) ) };
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized Date fromStorageStrings( String[] aStringValues ) throws ParseException {
		if ( aStringValues == null || aStringValues.length == 0 ) {
			return null;
		}
		else if ( aStringValues.length == 1 ) {
			if ( aStringValues[0] == null ) {
				return null;
			}
			try {
				return DateUtility.toDate( aStringValues[0], _dateFormats );
			}
			catch ( DateTimeException e ) {
				final TemporalAccessor theTemporal = DateFormat.NORM_DATE_FORMAT.getFormatter().parse( aStringValues[0] );
				final Instant theInstant = Instant.from( theTemporal );
				return new Date( theInstant.toEpochMilli() );
			}
		}
		throw new IllegalArgumentException( "The type <" + getType().getName() + "> is not an array type though the number of elements in the provided string array is <" + aStringValues.length + "> whereas only one element is being expected." );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toPrintable( Date aValue ) {
		final Instant theInstant = Instant.ofEpochMilli( aValue.getTime() );
		return _dateFormats[0].format( theInstant );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
