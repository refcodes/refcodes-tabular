package org.refcodes.tabular;

import java.util.LinkedHashSet;

/**
 * A list of {@link Column} instances, for example describing the elements of a
 * CSV file (visually speaking the of the CSV file's header line), is
 * represented by the {@link org.refcodes.tabular.FormattedHeader}. The
 * {@link org.refcodes.tabular.FormattedHeader} preserves an order for a list of
 * {@link org.refcodes.tabular.Column} instances. A
 * {@link org.refcodes.tabular.FormattedHeader} provides the semantics for
 * related {@link org.refcodes.tabular.Row} instances.
 * <p>
 * The {@link #keySet()} method must provide a predictable order as ensured by
 * the {@link LinkedHashSet} class as of the ordered nature of the
 * {@link FormattedHeader}.
 *
 * @param <T> The type managed by the {@link FormattedHeader}.
 */
public class FormattedHeader<T> extends AbstractHeader<T, FormattedColumn<? extends T>> {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _resetEscapeCode = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new formatted header impl.
	 */
	public FormattedHeader() {}

	/**
	 * Instantiates a new formatted header impl.
	 *
	 * @param aHeader the header
	 */
	@SuppressWarnings("unchecked")
	public FormattedHeader( FormattedColumn<? extends T>... aHeader ) {
		super( aHeader );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Gets the ANSI reset Escape-Code for this {@link FormattedHeader}
	 * instance. In case an ANSI Escape-Codes is set, then this ANSI Reset-Code
	 * is prepended to the according text being printed.
	 * 
	 * @return The {@link String} to be used for resetting ANSI escaping.
	 */
	public String getResetEscapeCode() {
		return _resetEscapeCode;
	}

	/**
	 * Gets the ANSI reset Escape-Code for this {@link FormattedHeader}
	 * instance. In case an ANSI Escape-Codes is set, then this ANSI Reset-Code
	 * is prepended to the according text being printed.
	 * 
	 * @param aEscapeCode The {@link String} to be used for resetting ANSI
	 *        escaping.
	 */
	public void setResetEscapeCode( String aEscapeCode ) {
		_resetEscapeCode = aEscapeCode;
	}

	/**
	 * Sets the ANSI reset Escape-Code for this {@link FormattedHeader} instance
	 * as of the Builder-Pattern. In case an ANSI Escape-Codes is set, then this
	 * ANSI Reset-Code is prepended to the according text being printed.
	 * 
	 * @param aEscapeCode The {@link String} to be used for resetting ANSI
	 *        escaping.
	 * 
	 * @return This {@link FormattedHeader} instance to continue configuration.
	 */
	public FormattedHeader<T> withResetEscapeCode( String aEscapeCode ) {
		setResetEscapeCode( aEscapeCode );
		return this;
	}
}