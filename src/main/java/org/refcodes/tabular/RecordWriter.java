// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.refcodes.io.RowWriter;
import org.refcodes.struct.SimpleType;
import org.refcodes.struct.TypeUtility;

/**
 * The {@link RecordWriter} writes data.
 *
 * @param <T> The type of the {@link Record} instances being used.
 */
public interface RecordWriter<T> extends RowWriter<T[]>, HeaderAccessor<T> {

	/**
	 * Parses and sets the {@link Header} from the provided {@link String}. You
	 * must have provided a {@link ColumnFactory} for this operation to succeed.
	 * 
	 * @param aHeader The header to be created.
	 */
	void parseHeader( String... aHeader );

	/**
	 * Parses and sets the {@link Header} from the provided {@link String}
	 * arguments and writes the CSV header to the writer (be it a Stream or a
	 * File). You must have provided a {@link ColumnFactory} for this operation
	 * to succeed.
	 * 
	 * @param aHeader The header to be created.
	 */
	void writeHeader( String... aHeader );

	/**
	 * Sets and writes the provided {@link Header} to the writer (be it a Stream
	 * or a File). .
	 * 
	 * @param aHeader The header to be created.
	 */
	void writeHeader( Header<T> aHeader );

	/**
	 * Writes the CSV header to the writer (be it a Stream or a File). You must
	 * have provided a {@link Header} for this operation to succeed.
	 */
	void writeHeader();

	/**
	 * Directly logs the provided instances to the writer.
	 * 
	 * @param aRecord The instances to be logged.
	 * 
	 * @throws IllegalArgumentException thrown in case the provided objects do
	 *         not align with the header (as of {@link #getHeader()}).
	 */
	@Override
	@SuppressWarnings("all")
	void writeNext( T... aRecord );

	/**
	 * Directly logs the provided instance to the writer. The properties of the
	 * given type matching the header are written accordingly.
	 *
	 * @param <TYPE> the generic type
	 * @param aRecord The instance to be logged.
	 * 
	 * @throws IllegalArgumentException thrown in case the provided objects do
	 *         not align with the header (as of {@link #getHeader()}).
	 */
	@SuppressWarnings("unchecked")
	default <TYPE> void writeNextType( TYPE aRecord ) {

		final Header<T> theHeader = getHeader();
		if ( theHeader != null ) {
			final Map<String, Object> theRecord = TypeUtility.toMap( aRecord );
			final List<Object> theRow = new ArrayList<>();
			Object eValue;
			String eStringValue;
			for ( Column<?> eColumn : theHeader.values() ) {
				eValue = theRecord.get( eColumn.getKey() );
				if ( eValue != null ) {
					if ( eColumn.getType().isAssignableFrom( eValue.getClass() ) ) {
						theRow.add( eValue );
					}
					// Special String support |-->
					else if ( eColumn.getType().isAssignableFrom( String.class ) ) {
						eStringValue = SimpleType.fromSimpleType( eValue );
						if ( eStringValue == null ) {
							eValue = eValue.toString();
						}
						theRow.add( eStringValue );
					}
					// Special String support <--|
					else {
						throw new IllegalArgumentException( "For the key <" + eColumn.getKey() + "> the value " + ( eValue instanceof String ? "\"" : "<" ) + eValue + ( eValue instanceof String ? "\"" : ">" ) + " (" + eValue.getClass().getName() + ") is not of the required type <" + eColumn.getType().getName() + ">!" );
					}
				}
				else {
					theRow.add( null );
				}
			}
			writeNext( (T[]) theRow.toArray() );
		}
		else {
			throw new IllegalStateException( "In order to use this method you must have configured / initialized a Header instance." );
		}
	}

	/**
	 * Directly writes the provided instance to the writer. The properties of
	 * the given type in the given order matching the header are written
	 * accordingly.
	 *
	 * @param <TYPE> the generic type
	 * @param aRecord The instance to be logged.
	 * 
	 * @throws IllegalArgumentException thrown in case the provided objects do
	 *         not align with the header (as of {@link #getHeader()}).
	 * @throws ColumnMismatchException thrown in case a value was found in the
	 *         e.g. in a {@link Record} of the wrong type than specified by a
	 *         provided {@link Column} (of for example a {@link Header}).
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link Header} and a {@link Row} (or another
	 *         {@link Header}), i.e. the index for the given key in a header may
	 *         be out of index of a given row or the given key does not exist in
	 *         a {@link Header}.
	 * @throws ParseException in case parsing the input was not possible.
	 */
	default <TYPE> void writeNextLine( String... aRecord ) throws ColumnMismatchException, HeaderMismatchException, ParseException {
		final Header<T> theHeader = getHeader();
		if ( theHeader != null ) {
			writeNext( theHeader.toRecord( aRecord ) );
		}
		else {
			throw new IllegalStateException( "In order to use this method you must have configured / initialized a Header instance." );
		}
	}

	/**
	 * Directly logs the provided {@link String} instances to the writer.
	 *
	 * @param aRecord The {@link String} instances to be logged.
	 * 
	 * @throws ColumnMismatchException Thrown in case a value was found in the
	 *         e.g. in a {@link Record} of the wrong type than specified by a
	 *         provided {@link Column} (of for example a {@link Header}).
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link Header}.
	 */
	void writeNext( Record<T> aRecord ) throws ColumnMismatchException, HeaderMismatchException;

}