// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import java.util.Map;
import java.util.Set;

import org.refcodes.struct.CanonicalMap.CanonicalMapBuilder;
import org.refcodes.struct.CanonicalMapBuilderImpl;

/**
 * A {@link Record} is a {@link java.util.Map} containing {@link Field} data
 * structures with support for {@link Column} definitions. The {@link Field}
 * instances can be managed with the functionality provided by a {@link Column}.
 * <p>
 * A value can be retrieved from a {@link Record} by providing an according
 * {@link Column} definition, which verifies the value type. Also values can be
 * converted to and from the appropriate interchange and string formats by the
 * according {@link Column} instances.
 * <p>
 * In contrast to a {@link Row}, a {@link Record} relates the key (name) to its
 * values. Similar to a {@link Row}, a {@link Record} makes use of
 * {@link Column} instances (for example as provided by a {@link Header} or a
 * {@link Header} instance) to give the fields additional semantics.
 *
 * @param <T> The type managed by the {@link Record}.
 */
public interface Record<T> extends Map<String, T> {

	/**
	 * Returns true if this {@link Record} contains a mapping for the specified
	 * {@link Column} (its key).
	 *
	 * @return True in case there is such a key and value pair matching the
	 *         provided {@link Column}.
	 */
	// boolean containsColumn( Column<?> aColumn );

	/**
	 * Retrieves a value matching the key and the type of the provided
	 * {@link Column}.
	 * 
	 * @param aColumn The {@link Column} for which to retrieve the related
	 *        value.
	 * 
	 * @exception ColumnMismatchException in case a value was found in the
	 *            {@link Record} of the wrong type than specified by the
	 *            provided {@link Column}.
	 */
	// <T> T getColumnValue( Column<?> aColumn ) throws ColumnMismatchException;

	/**
	 * Removes a {@link Field} (key and value pair) matching the key and the
	 * type of the provided {@link Column}.
	 * 
	 * @param aColumn The {@link Column} for which to remove the related Field.
	 * 
	 * @return The value related to the given {@link Column} (its key) or null
	 *         if there was none such value found.
	 * 
	 * @throws ColumnMismatchException in case a value was found in the
	 *         {@link Record} of the wrong type than specified by the provided
	 *         {@link Column}.
	 */
	// <T> T removeColumn( Column<?> aColumn ) throws ColumnMismatchException;

	/**
	 * Creates a {@link Row} representation of the Record. The {@link Row}
	 * looses the relations of the values to the according keys. To retain this
	 * semantics, the provided {@link Header} is required.
	 * 
	 * @return The {@link Row} representation of the {@link Record} as of the
	 *         provided {@link Header}. throws ColumnMismatchException in case a
	 *         value was found in the {@link Record} of the wrong type than
	 *         specified by the {@link Column} instances as provided by the
	 *         {@link Header}.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link Header}.
	 */
	// Row toRow( Header aHeader ) throws ColumnMismatchException,
	// HeaderMismatchException;

	/**
	 * Returns the {@link Field} {@link Set} representation of the
	 * {@link Record}.
	 * 
	 * @return The {@link Field} instances representation of the {@link Record}.
	 */
	Set<Field<T>> toFieldSet();

	/**
	 * This {@link Record} is taken and only all non null values are taken for
	 * the returned {@link Record}. In case of string objects, only
	 * {@link String} instances with a length greater than zero are taken into
	 * account for the returned {@link Record}.
	 * 
	 * @return The output {@link Record} without any keys which's values were
	 *         null or in case of strings which's length was zero.
	 */
	@SuppressWarnings("unchecked")
	default Record<T> toPurged() {
		final Record<T> theRecord = new RecordImpl<>();
		Object eValue;
		for ( String eKey : keySet() ) {
			eValue = get( eKey );
			if ( eValue != null ) {
				if ( eValue instanceof String ) {
					if ( ( (String) eValue ).length() > 0 ) {
						theRecord.put( eKey, (T) eValue );
					}
				}
				else {
					theRecord.put( eKey, (T) eValue );
				}
			}
		}
		return theRecord;
	}

	/**
	 * Converts the content of the {@link Record} to the according type. The
	 * {@link Header} keys must match the type's properties.
	 *
	 * @param <TYPE> The type of the element to which the line to be read is to
	 *        be converted to.
	 * @param aClass the type of the instance to be created.
	 * 
	 * @return The instance of the type to which the {@link Record} is being
	 *         converted to.
	 */
	default <TYPE> TYPE toType( Class<TYPE> aClass ) {
		final CanonicalMapBuilder theBuilder = new CanonicalMapBuilderImpl();
		for ( String eKey : keySet() ) {
			theBuilder.insertTo( eKey, get( eKey ) );
		}
		return theBuilder.toType( aClass );
	}
}