// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

/**
 * Implementation of the {@link Records} interface being {@link Cloneable}.
 *
 * @param <T> The type managed by the {@link Records}.
 */
public class RecordsImpl<T> implements Records<T>, Cloneable {

	private final Iterator<Record<T>> records;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link RecordsImpl} instance configured with the provided
	 * {@link Record} instances.
	 * 
	 * @param aRecords The {@link Collection} with the {@link Record} instances
	 *        to be contained in the {@link RecordsImpl} .
	 */
	public RecordsImpl( Collection<Record<T>> aRecords ) {
		this.records = aRecords.iterator();
	}

	/**
	 * Constructs the {@link RecordsImpl} instance configured with the provided
	 * {@link Record} instances.
	 * 
	 * @param aRecords The array with the {@link Record} instances to be
	 *        contained in the {@link RecordsImpl} .
	 */
	@SafeVarargs
	public RecordsImpl( Record<T>... aRecords ) {
		this.records = Arrays.asList( aRecords ).iterator();
	}

	// /////////////////////////////////////////////////////////////////////////
	// ITERATOR:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext() {
		return this.records.hasNext();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Record<T> next() {
		return this.records.next();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void remove() {
		this.records.remove();
	}

	// /////////////////////////////////////////////////////////////////////////
	// COMMON:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
