// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.zip.ZipException;

import org.refcodes.data.Delimiter;
import org.refcodes.data.Text;
import org.refcodes.exception.Trap;
import org.refcodes.io.ZipUtility;
import org.refcodes.textual.CsvBuilder;
import org.refcodes.textual.CsvEscapeMode;
import org.refcodes.textual.CsvMixin;

/**
 * The {@link CsvRecordReader} is an implementation of the {@link RecordReader}
 * interface and provides functionality to parse CSV input streams:
 * 
 * <code>
 *	Column&lt;Float&gt; floatColumn = floatColumn( "volume" );
 *	Column&lt;String&gt; stringColumn = floatColumn( "name" );
 *	Column&lt;Integer&gt; intColumn = floatColumn( "length" );
 *	Column&lt;Date&gt; dateColumn = floatColumn( "time" );
 *
 *	try (CsvRecordReader&lt;?&gt; theCsvReader = new CsvRecordReader&lt;&gt;( headerOf( stringColumn, intColumn, dateColumn, floatColumn ), theSourceFile, theSourceDelimiter )) {
 *		theCsvReader.readHeader(); // Level the CsvRecordReader's header with the actual file's header
 *		Record&lt;?&gt; eRecord;
 *		while ( theCsvReader.hasNext() ) {
 *			record = theCsvReader.next();
 *			float volume = floatColumn.get( record );
 *			String name = stringColumn.get( record );
 *			Date time = dateColumn.get( record );
 *			int length = intColumn.get( record );
 *		}
 *	}
 * </code>
 * 
 * @param <T> The type managed by the {@link Records}.
 */
public class CsvRecordReader<T> implements CsvMixin, RecordReader<T> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Header<T> _header = null;
	private BufferedReader _reader;
	private String _nextLine;
	private int _line = -1;
	private boolean _isStrict;
	private long _erroneousRecordCount = 0;
	private CsvBuilder _csvBuilder;
	private ColumnFactory<T> _columnFactory;
	private String[] _commentPrefixes = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvRecordReader( Header<T> aHeader, File aCsvFile ) throws IOException {
		this( aHeader, null, ZipUtility.toInputStream( aCsvFile ), null, Delimiter.CSV.getChar(), true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvRecordReader( Header<T> aHeader, File aCsvFile, char aCsvSeparator ) throws IOException {
		this( aHeader, null, ZipUtility.toInputStream( aCsvFile ), null, aCsvSeparator, true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvRecordReader( Header<T> aHeader, File aCsvFile, boolean isStrict ) throws IOException {
		this( aHeader, null, ZipUtility.toInputStream( aCsvFile ), null, Delimiter.CSV.getChar(), isStrict );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvRecordReader( Header<T> aHeader, File aCsvFile, char aCsvSeparator, boolean isStrict ) throws IOException {
		this( aHeader, null, ZipUtility.toInputStream( aCsvFile ), null, aCsvSeparator, isStrict );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 *
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvRecordReader( ColumnFactory<T> aColumnFactory, File aCsvFile ) throws IOException {
		this( null, aColumnFactory, ZipUtility.toInputStream( aCsvFile ), null, Delimiter.CSV.getChar(), true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 *
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvRecordReader( ColumnFactory<T> aColumnFactory, File aCsvFile, char aCsvSeparator ) throws IOException {
		this( null, aColumnFactory, ZipUtility.toInputStream( aCsvFile ), null, aCsvSeparator, true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 *
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvRecordReader( ColumnFactory<T> aColumnFactory, File aCsvFile, boolean isStrict ) throws IOException {
		this( null, aColumnFactory, ZipUtility.toInputStream( aCsvFile ), null, Delimiter.CSV.getChar(), isStrict );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 *
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvRecordReader( ColumnFactory<T> aColumnFactory, File aCsvFile, char aCsvSeparator, boolean isStrict ) throws IOException {
		this( null, aColumnFactory, ZipUtility.toInputStream( aCsvFile ), null, aCsvSeparator, isStrict );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 * 
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public CsvRecordReader( Header<T> aHeader, InputStream aCsvInputStream ) throws IOException {
		this( aHeader, null, aCsvInputStream, null, Delimiter.CSV.getChar(), true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 * 
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public CsvRecordReader( Header<T> aHeader, InputStream aCsvInputStream, boolean isStrict ) throws IOException {
		this( aHeader, null, aCsvInputStream, null, Delimiter.CSV.getChar(), isStrict );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aCsvDelimiter The delimiter being expected for the CSV input
	 *        stream.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public CsvRecordReader( Header<T> aHeader, InputStream aCsvInputStream, char aCsvDelimiter ) throws IOException {
		this( aHeader, null, aCsvInputStream, null, aCsvDelimiter, true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 * 
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aCsvDelimiter The delimiter being expected for the CSV input
	 *        stream.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public CsvRecordReader( Header<T> aHeader, InputStream aCsvInputStream, char aCsvDelimiter, boolean isStrict ) throws IOException {
		this( aHeader, null, aCsvInputStream, null, aCsvDelimiter, isStrict );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 * 
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public CsvRecordReader( ColumnFactory<T> aColumnFactory, InputStream aCsvInputStream ) throws IOException {
		this( null, aColumnFactory, aCsvInputStream, null, Delimiter.CSV.getChar(), true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 * 
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public CsvRecordReader( ColumnFactory<T> aColumnFactory, InputStream aCsvInputStream, boolean isStrict ) throws IOException {
		this( null, aColumnFactory, aCsvInputStream, null, Delimiter.CSV.getChar(), isStrict );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 * 
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aCsvDelimiter The delimiter being expected for the CSV input
	 *        stream.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public CsvRecordReader( ColumnFactory<T> aColumnFactory, InputStream aCsvInputStream, char aCsvDelimiter ) throws IOException {
		this( null, aColumnFactory, aCsvInputStream, null, aCsvDelimiter, true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 * 
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aCsvDelimiter The delimiter being expected for the CSV input
	 *        stream.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public CsvRecordReader( ColumnFactory<T> aColumnFactory, InputStream aCsvInputStream, char aCsvDelimiter, boolean isStrict ) throws IOException {
		this( null, aColumnFactory, aCsvInputStream, null, aCsvDelimiter, isStrict );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvRecordReader( Header<T> aHeader, File aCsvFile, Charset aEncoding ) throws IOException {
		this( aHeader, null, ZipUtility.toInputStream( aCsvFile ), aEncoding, Delimiter.CSV.getChar(), true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvRecordReader( Header<T> aHeader, File aCsvFile, Charset aEncoding, char aCsvSeparator ) throws IOException {
		this( aHeader, null, ZipUtility.toInputStream( aCsvFile ), aEncoding, aCsvSeparator, true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvRecordReader( Header<T> aHeader, File aCsvFile, Charset aEncoding, boolean isStrict ) throws IOException {
		this( aHeader, null, ZipUtility.toInputStream( aCsvFile ), aEncoding, Delimiter.CSV.getChar(), isStrict );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvRecordReader( Header<T> aHeader, File aCsvFile, Charset aEncoding, char aCsvSeparator, boolean isStrict ) throws IOException {
		this( aHeader, null, ZipUtility.toInputStream( aCsvFile ), aEncoding, aCsvSeparator, isStrict );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 *
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvRecordReader( ColumnFactory<T> aColumnFactory, File aCsvFile, Charset aEncoding ) throws IOException {
		this( null, aColumnFactory, ZipUtility.toInputStream( aCsvFile ), aEncoding, Delimiter.CSV.getChar(), true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 *
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvRecordReader( ColumnFactory<T> aColumnFactory, File aCsvFile, Charset aEncoding, char aCsvSeparator ) throws IOException {
		this( null, aColumnFactory, ZipUtility.toInputStream( aCsvFile ), aEncoding, aCsvSeparator, true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 *
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvRecordReader( ColumnFactory<T> aColumnFactory, File aCsvFile, Charset aEncoding, boolean isStrict ) throws IOException {
		this( null, aColumnFactory, ZipUtility.toInputStream( aCsvFile ), aEncoding, Delimiter.CSV.getChar(), isStrict );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 *
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvRecordReader( ColumnFactory<T> aColumnFactory, File aCsvFile, Charset aEncoding, char aCsvSeparator, boolean isStrict ) throws IOException {
		this( null, aColumnFactory, ZipUtility.toInputStream( aCsvFile ), aEncoding, aCsvSeparator, isStrict );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 * 
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public CsvRecordReader( Header<T> aHeader, InputStream aCsvInputStream, Charset aEncoding ) throws IOException {
		this( aHeader, null, aCsvInputStream, aEncoding, Delimiter.CSV.getChar(), true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 * 
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public CsvRecordReader( Header<T> aHeader, InputStream aCsvInputStream, Charset aEncoding, boolean isStrict ) throws IOException {
		this( aHeader, null, aCsvInputStream, aEncoding, Delimiter.CSV.getChar(), isStrict );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding the encoding
	 * @param aCsvDelimiter The delimiter being expected for the CSV input
	 *        stream.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public CsvRecordReader( Header<T> aHeader, InputStream aCsvInputStream, Charset aEncoding, char aCsvDelimiter ) throws IOException {
		this( aHeader, null, aCsvInputStream, aEncoding, aCsvDelimiter, true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 * 
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvDelimiter The delimiter being expected for the CSV input
	 *        stream.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public CsvRecordReader( Header<T> aHeader, InputStream aCsvInputStream, Charset aEncoding, char aCsvDelimiter, boolean isStrict ) throws IOException {
		this( aHeader, null, aCsvInputStream, aEncoding, aCsvDelimiter, isStrict );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 * 
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public CsvRecordReader( ColumnFactory<T> aColumnFactory, InputStream aCsvInputStream, Charset aEncoding ) throws IOException {
		this( null, aColumnFactory, aCsvInputStream, aEncoding, Delimiter.CSV.getChar(), true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 * 
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public CsvRecordReader( ColumnFactory<T> aColumnFactory, InputStream aCsvInputStream, Charset aEncoding, boolean isStrict ) throws IOException {
		this( null, aColumnFactory, aCsvInputStream, aEncoding, Delimiter.CSV.getChar(), isStrict );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 * 
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvDelimiter The delimiter being expected for the CSV input
	 *        stream.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public CsvRecordReader( ColumnFactory<T> aColumnFactory, InputStream aCsvInputStream, Charset aEncoding, char aCsvDelimiter ) throws IOException {
		this( null, aColumnFactory, aCsvInputStream, aEncoding, aCsvDelimiter, true );
	}

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type.
	 * 
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvDelimiter The delimiter being expected for the CSV input
	 *        stream.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public CsvRecordReader( ColumnFactory<T> aColumnFactory, InputStream aCsvInputStream, Charset aEncoding, char aCsvDelimiter, boolean isStrict ) throws IOException {
		this( null, aColumnFactory, aCsvInputStream, aEncoding, aCsvDelimiter, isStrict );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs a {@link CsvRecordReader} with the given parameters. This
	 * constructor supports a {@link ColumnFactory} for creating {@link Column}
	 * instance according to the keys found in the CSV top line. The
	 * {@link Column} instances are required to convert the CSV line values from
	 * the storage format to the actual required type. If a {@link Header} is
	 * provided, then the {@link Header} is used for generating the
	 * {@link Column} instances instead of the top line of the CSV file.
	 * 
	 * @param aHeader The {@link Header} to use when parsing the lines retrieved
	 *        from the {@link InputStream}.
	 * @param aColumnFactory A {@link ColumnFactory} to be used to generate
	 *        {@link Column} instances from the top line of the CSF file,
	 *        required for parsing the CSV lines and converting them to
	 *        {@link Record} instances.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvDelimiter The delimiter being expected for the CSV input
	 *        stream.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	protected CsvRecordReader( Header<T> aHeader, ColumnFactory<T> aColumnFactory, InputStream aCsvInputStream, Charset aEncoding, char aCsvDelimiter, boolean isStrict ) throws IOException {
		_csvBuilder = new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withDelimiter( aCsvDelimiter );
		_isStrict = isStrict;
		_reader = aEncoding != null ? new BufferedReader( new InputStreamReader( aCsvInputStream, aEncoding ) ) : new BufferedReader( new InputStreamReader( aCsvInputStream ) );
		if ( aHeader == null && aColumnFactory == null ) {
			throw new IllegalArgumentException( "You must provide either a header or a column factory, but none was provided." );
		}
		_header = aHeader;
		_columnFactory = aColumnFactory;
		readNextLine();
	}

	// /////////////////////////////////////////////////////////////////////////
	// ITERATOR:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Header<T> readHeader() throws IOException {
		return readHeader( _header );
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Header<T> readHeader( Column<T>... aColumns ) throws IOException {
		return readHeader( new HeaderImpl<T>( aColumns ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Header<T> readHeader( Header<T> aHeader ) throws IOException {
		if ( _nextLine == null ) {
			readNextLine();
			if ( _nextLine != null ) {
				throw new IOException( "Cannot read Header as there are no lines which can be read anymore." );
			}
		}
		// _nextLine = TruncateTextBuilder.asTruncated( _nextLine, TruncateMode.TAIL, EndOfLine.toChars() );
		final String[] theHeaderKeys = _csvBuilder.toFields( _nextLine );

		final Header<T> theHeader;

		if ( _columnFactory != null ) {
			theHeader = TabularUtility.toHeader( theHeaderKeys, _columnFactory );
		}
		else {
			theHeader = (Header<T>) TabularUtility.toHeader( theHeaderKeys, new StringColumnFactory() );
		}

		_header = toHeader( theHeader, aHeader );
		_columnFactory = null;
		readNextLine();
		return _header;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String skipHeader() throws IOException {
		final String currentLine = _nextLine;
		readNextLine();
		return currentLine;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext() {
		if ( _header == null ) {
			try {
				readHeader();
			}
			catch ( IOException e ) {
				return false;
			}
		}
		return ( _nextLine != null );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] nextRow() {
		final Record<T> theRecord = next();
		final String[] theNext = new String[theRecord.size()];
		for ( int i = 0; i < theNext.length; i++ ) {
			final Column<? extends T> column = _header.get( i );
			final T eValue = theRecord.get( column.getKey() );
			theNext[i] = column.toStorageString_( eValue );
		}
		return theNext;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <TYPE> TYPE nextType( Class<TYPE> aClass ) {
		try {
			return next().toType( aClass );
		}
		catch ( Exception e ) {
			_erroneousRecordCount++;
			throw new NoSuchElementException( "An exception of type \"" + e.getClass().getName() + "\" at line <" + _line + "> occurred while parsing for type <" + aClass.getName() + ">", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String nextRaw() {
		final String theRawLine = _nextLine;
		if ( _nextLine == null || _header == null ) {
			throw new NoSuchElementException( "There is no more element beyond line <" + _line + "> in this iterator." );
		}
		try {
			readNextLine();
		}
		catch ( IOException e ) {
			_nextLine = null;
		}
		if ( !hasNext() ) {
			try {
				_reader.close();
			}
			catch ( IOException e ) {}
		}
		return theRawLine;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Record<T> next() {
		if ( _header == null ) {
			try {
				readHeader();
			}
			catch ( IOException e ) {
				throw new NoSuchElementException( "An exception of type \"" + e.getClass().getName() + "\" at line <" + _line + "> occurred while probing for a header, the line is \"" + _nextLine + "\": " + Trap.asMessage( e ) );
			}
		}
		if ( _nextLine == null || _header == null ) {
			throw new NoSuchElementException( "There is no more element beyond line <" + _line + "> in this iterator." );
		}
		final List<Field<T>> theItems = new ArrayList<>();
		final String[] theValues = _csvBuilder.toFields( _nextLine );
		T eValue = null;
		String eString;
		List<String> eStringList;
		String eStringValue;
		String[] eStringValues;
		Column<?> eColumn;
		final int theSize = _header.size() <= theValues.length ? _header.size() : theValues.length;
		for ( int i = 0; i < theSize; i++ ) {
			eValue = null;
			eString = theValues[i];
			eColumn = _header.get( i );
			try {
				// Do we have an array type for the given column?
				if ( eColumn.getType().isArray() ) {
					eStringList = new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withRecord( eString ).withDelimiter( Delimiter.ARRAY.getChar() ).toFields();
					eStringValues = eStringList.toArray( new String[eStringList.size()] );
					eValue = (T) eColumn.fromStorageStrings( eStringValues );
				}
				// We have a plain type for the given column:
				else {
					eStringValue = eString;
					eValue = (T) eColumn.fromStorageString( eStringValue );
				}
			}
			catch ( ParseException e ) {
				_erroneousRecordCount++;
				if ( _isStrict ) {
					throw new NoSuchElementException( "A parse exception at line <" + _line + "> occurred while parsing the value \"" + eString + "\" for key \"" + eColumn.getKey() + "\" (type = " + eColumn.getType().getName() + ")\", the line is \"" + _nextLine + "\": " + Trap.asMessage( e ) );
				}
			}
			catch ( IndexOutOfBoundsException e ) {
				_erroneousRecordCount++;
				if ( _isStrict ) {
					throw new NoSuchElementException( "An index out of bounds exception at line <" + _line + "> occurred while parsing the value for key \"" + eColumn.getKey() + "\" (type = " + eColumn.getType().getName() + "), the line is \"" + _nextLine + "\": " + Trap.asMessage( e ) );
				}
			}
			catch ( UnsupportedOperationException e ) {
				_erroneousRecordCount++;
				if ( _isStrict ) {
					throw new NoSuchElementException( "An unsupported operation exception at line <" + _line + "> occurred while parsing the value for key \"" + eColumn.getKey() + "\" (type = " + eColumn.getType().getName() + "), the line is \"" + _nextLine + "\": " + Trap.asMessage( e ) );
				}
			}
			catch ( NumberFormatException e ) {
				_erroneousRecordCount++;
				if ( _isStrict ) {
					throw new NoSuchElementException( "A number format exception exception at line <" + _line + "> occurred while parsing the value for key \"" + eColumn.getKey() + "\" (type = " + eColumn.getType().getName() + "), the line is \"" + _nextLine + "\": " + Trap.asMessage( e ) );
				}
			}
			catch ( Exception e ) {
				_erroneousRecordCount++;
				if ( _isStrict ) {
					throw new NoSuchElementException( "An exception of type \"" + e.getClass().getName() + "\" at line <" + _line + "> occurred while parsing the value for key \"" + eColumn.getKey() + "\" (type = " + eColumn.getType().getName() + "), the line is \"" + _nextLine + "\": " + Trap.asMessage( e ) );
				}
			}
			theItems.add( new FieldImpl<T>( eColumn.getKey(), eValue ) );
		}
		final Record<T> theRecord = new RecordImpl<>( theItems );
		try {
			readNextLine();
		}
		catch ( IOException e ) {
			_nextLine = null;
		}
		if ( !hasNext() ) {
			try {
				_reader.close();
			}
			catch ( IOException e ) {}
		}
		return theRecord;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void remove() {
		throw new UnsupportedOperationException( Text.UNSUPPORTED_OPERATION.getText() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Header<T> getHeader() {
		if ( _header == null ) {
			try {
				readHeader();
			}
			catch ( IOException ignore ) {}
		}
		return _header;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCommentPrefixes( String... aCommentPrefixes ) {
		_commentPrefixes = aCommentPrefixes;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] getCommentPrefixes() {
		return _commentPrefixes;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clearCommentPrefixes() {
		_commentPrefixes = null;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CsvRecordReader<T> withCommentPrefixes( String... aCommentPrefixes ) {
		setCommentPrefixes( aCommentPrefixes );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		_reader.close();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CsvEscapeMode getCsvEscapeMode() {
		return _csvBuilder.getCsvEscapeMode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isTrim() {
		return _csvBuilder.isTrim();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char getDelimiter() {
		return _csvBuilder.getDelimiter();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setTrim( boolean isTrim ) {
		_csvBuilder.setTrim( isTrim );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setDelimiter( char aCsvDelimiter ) {
		_csvBuilder.setDelimiter( aCsvDelimiter );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CsvRecordReader<T> withTrim( boolean isTrim ) {
		_csvBuilder.setTrim( isTrim );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CsvRecordReader<T> withCsvEscapeMode( CsvEscapeMode aCsvEscapeMode ) {
		_csvBuilder.setCsvEscapeMode( aCsvEscapeMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CsvRecordReader<T> withDelimiter( char aCsvDelimiter ) {
		_csvBuilder.setDelimiter( aCsvDelimiter );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCsvEscapeMode( CsvEscapeMode aCsvEscapeMode ) {
		_csvBuilder.setCsvEscapeMode( aCsvEscapeMode );
	}

	/**
	 * Gets the erroneous record count.
	 *
	 * @return the erroneous record count
	 */
	@Override
	public long getErroneousRecordCount() {
		return _erroneousRecordCount;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void readNextLine() throws IOException {
		_nextLine = _reader.readLine();
		if ( _nextLine != null ) {
			_line++;
			while ( _nextLine != null && ( isComment( _nextLine ) || _nextLine.length() == 0 ) ) {
				_nextLine = _reader.readLine();
				_line++;
			}
		}
	}
}
