// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// /////////////////////////////////////////////////////////////////////////----
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// /////////////////////////////////////////////////////////////////////////----
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// /////////////////////////////////////////////////////////////////////////----
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import java.util.List;

/**
 * A {@link Row} holds multiple data elements loosely coupled to a
 * {@link Header}. As a {@link Row} just contains a list of data elements with
 * no keys (names) directly associated to the elements, a {@link Header} with
 * the according list of key (name) and type definitions is required to give a
 * {@link Row} its semantics.
 * <p>
 * In terms of a CSV file, a {@link Row} represents a single line of comma
 * separated values. The {@link Header} stands for the header line (top line) of
 * the CSV file. The semantics for the content of a {@link Row} is provided by
 * the according {@link Header}.
 * <p>
 * Many {@link Row} instances are contained in a {@link Rows} instance which in
 * turn requires a single {@link Header} to give all the therein contained
 * {@link Row} instances their semantics (in terms of key (name) and type
 * information).
 * <p>
 * In contrast to a {@link Row}, a {@link Record} relates an element field with
 * a key (name), called {@link Field}.
 * <p>
 * In contrast to a {@link Record}, a {@link Row} does not relate a key (name)
 * to its values. To do this, a {@link Header} is required. Similar to a
 * {@link Record}, a {@link Row} makes use of {@link Column} instances (provided
 * by a {@link Header} instance) to give the fields additional semantics.
 *
 * @param <T> The type managed by the {@link Row}.
 */
public interface Row<T> extends List<T> {

	/**
	 * This method retrieves a value from the row by taking the index of the
	 * according column in the header of the given key (the one with the given
	 * key) and returns that value. This is possible as the values in the row
	 * and in the header have an order. This method is used as a convenience
	 * delegate by {@link Header#get(Row, String)}.
	 * 
	 * @param aHeader The header being the reference for retrieval of the value
	 *        by the given key.
	 * @param aKey The key for which to retrieve the value.
	 * 
	 * @return The value in the row representing the key in the header.
	 * 
	 * @throws HeaderMismatchException Thrown in case there is a mismatch
	 *         between the given {@link HeaderMismatchException} and the
	 *         {@link Row}, i.e. the index for the given key in the header may
	 *         be out of index of the given row or the given key does not exist
	 *         in the {@link Header}.
	 * @throws ColumnMismatchException in case the type defined in the column
	 *         for that key does not match the type in the row identified by the
	 *         given key.
	 */
	T get( Header<T> aHeader, String aKey ) throws HeaderMismatchException, ColumnMismatchException;
}
