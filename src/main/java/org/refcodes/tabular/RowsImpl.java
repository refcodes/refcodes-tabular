// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.refcodes.textual.CsvBuilder;
import org.refcodes.textual.CsvEscapeMode;

/**
 * Implementation of the {@link Rows} interface being {@link Cloneable}.
 *
 * @param <T> The type managed by the {@link Rows}.
 */
public class RowsImpl<T> implements Rows<T>, Cloneable {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final Header<T> _header;

	private final Iterator<Row<T>> _rows;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link RowsImpl} instance configured with the provided
	 * {@link Row} instances and the provided {@link Header} instance. The
	 * {@link Row} instances must match with toe provided {@link Header} in
	 * terms of {@link Header#isEqualWith(Row)}.
	 * 
	 * @param aHeader The {@link Header} instances used for validating the
	 *        managing the {@link Row} instances.
	 * @param aRows The {@link Row} instances to be contained in the
	 *        {@link RowsImpl} in the order as passed.
	 */
	@SafeVarargs
	public RowsImpl( Header<T> aHeader, Row<T>... aRows ) {
		final List<Row<T>> theRows = new ArrayList<>();
		for ( Row<T> eRow : aRows ) {
			if ( !aHeader.isEqualWith( eRow ) ) {
				throw new IllegalArgumentException( "Row <" + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eRow ).toRecord() + "> mismatch with header <" + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( aHeader ).toRecord() + ">." );
			}
			theRows.add( eRow );
		}
		_header = aHeader;
		_rows = theRows.iterator();
	}

	/**
	 * Constructs the {@link RowsImpl} instance configured with the provided
	 * {@link Row} instances and the provided {@link Header} instance. The
	 * {@link Row} instances must match with toe provided {@link Header} in
	 * terms of {@link Header#isEqualWith(Row)}.
	 * 
	 * @param aHeader The {@link Header} instances used for validating the
	 *        managing the {@link Row} instances.
	 * @param aRows The {@link Row} instances to be contained in the
	 *        {@link RowsImpl} in the order as passed.
	 */
	public RowsImpl( Header<T> aHeader, Collection<Row<T>> aRows ) {
		for ( Row<T> eRow : aRows ) {
			if ( !aHeader.isEqualWith( eRow ) ) {
				throw new IllegalArgumentException( "Row <" + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eRow ).toRecord() + "> mismatch with header <" + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( aHeader ).toRecord() + ">." );
			}
		}
		_header = aHeader;
		_rows = aRows.iterator();
	}

	// /////////////////////////////////////////////////////////////////////////
	// ROWS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Header<T> getHeader() {
		return _header;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	// /////////////////////////////////////////////////////////////////////////
	// ITERATOR:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext() {
		return _rows.hasNext();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row<T> next() {
		return _rows.next();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void remove() {
		_rows.remove();
	}
}
