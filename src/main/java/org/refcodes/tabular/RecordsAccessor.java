// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

/**
 * Provides an accessor for a {@link Records} property.
 *
 * @param <T> The type managed by the {@link Records}.
 */
public interface RecordsAccessor<T> {

	/**
	 * Retrieves the {@link Records} property.
	 * 
	 * @return The {@link Records} being stored by this property.
	 */
	Records<T> getRecords();

	/**
	 * Provides a mutator for a {@link Records} property.
	 *
	 * @param <T> the generic type
	 */
	public interface RecordsMutator<T> {

		/**
		 * Sets the {@link Records} property.
		 * 
		 * @param aDataRecord The {@link Records} to be stored by this property.
		 */
		void setRecords( Records<T> aDataRecord );
	}

	/**
	 * Provides a {@link Records} property.
	 *
	 * @param <T> the generic type
	 */
	public interface RecordsProperty<T> extends RecordsAccessor<T>, RecordsMutator<T> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setRecords(Records)} and returns the very same value
		 * (getter).
		 * 
		 * @param aRecords The value to set (via {@link #setRecords(Records)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Records<T> letRecords( Records<T> aRecords ) {
			setRecords( aRecords );
			return aRecords;
		}
	}
}
