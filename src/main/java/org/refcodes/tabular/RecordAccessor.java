// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

/**
 * Provides an accessor for a {@link Record} property.
 *
 * @param <T> The type managed by the {@link Record}.
 */
public interface RecordAccessor<T> {

	/**
	 * Retrieves the {@link Record} property.
	 * 
	 * @return The {@link Record} being stored by this property.
	 */
	Record<T> getRecord();

	/**
	 * Provides a mutator for a {@link Record} property.
	 *
	 * @param <T> the generic type
	 */
	public interface RecordMutator<T> {

		/**
		 * Sets the {@link Record} property.
		 * 
		 * @param aDataRecord The {@link Record} to be stored by this property.
		 */
		void setRecord( Record<T> aDataRecord );
	}

	/**
	 * Provides a {@link Record} property.
	 *
	 * @param <T> the generic type
	 */
	public interface RecordProperty<T> extends RecordAccessor<T>, RecordMutator<T> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setRecord(Record)} and returns the very same value (getter).
		 * 
		 * @param aRecord The value to set (via {@link #setRecord(Record)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Record<T> letRecord( Record<T> aRecord ) {
			setRecord( aRecord );
			return aRecord;
		}
	}
}
