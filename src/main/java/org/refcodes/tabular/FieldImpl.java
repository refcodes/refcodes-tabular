// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

/**
 * Implementation of the {@link Field} interface being {@link Cloneable}.
 *
 * @param <T> The type of the value of the field.
 */
public class FieldImpl<T> implements Field<T>, Cloneable {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final String _key;

	private final T _value;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link Field}.
	 * 
	 * @param aKey The key of the {@link Field}.
	 * @param aValue The value of the {@link Field}.
	 */
	public FieldImpl( String aKey, T aValue ) {
		_key = aKey;
		_value = aValue;
	}

	// /////////////////////////////////////////////////////////////////////////
	// FIELD:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getKey() {
		return _key;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T getValue() {
		return _value;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	//	@Override
	//	public void setKey( String aKey ) {
	//		_key = aKey;
	//	}

	//	@Override
	//	public Relation<String, T> withKey( String aKey ) {
	//		_key = aKey;
	//		return this;
	//	}

	//	@Override
	//	public void setValue( T aValue ) {
	//		_value = aValue;
	//	}

	//	@Override
	//	public Relation<String, T> withValue( T aValue ) {
	//		_value = aValue;
	//		return this;
	//	}
}
