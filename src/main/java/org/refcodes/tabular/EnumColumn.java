// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import java.text.ParseException;

import org.refcodes.mixin.AliasAccessor;

/**
 * Implementation of the {@link Column} interface for working with {@link Enum}
 * instances, being {@link Cloneable}.
 */
public class EnumColumn<T extends Enum<T>> extends AbstractColumn<T> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a {@link EnumColumn} managing {@link Double} instances.
	 * 
	 * @param aKey The key for the {@link EnumColumn}.
	 * @param aType The enumeration's type.
	 */
	public EnumColumn( String aKey, Class<T> aType ) {
		super( aKey, aType );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 * 
	 * In case the enumeration implements the {@link AliasAccessor} interface,
	 * then the according alias is the preferred storage string to be used.
	 */
	@Override
	public String[] toStorageStrings( T aValue ) {

		// ENUM alias |-->
		if ( aValue instanceof AliasAccessor ) {
			return new String[] { ( (AliasAccessor) aValue ).getAlias() };
		}
		// ENUM alias <--|

		// ENUM name() |-->
		return new String[] { aValue.name() };
		// ENUM name() |<--|
	}

	/**
	 * {@inheritDoc}
	 * 
	 * If possible, the enumeration's name is used for resolving the value. In
	 * case the enumeration implements the {@link AliasAccessor} interface, then
	 * also the according alias is used for resolving the value. If both fails,
	 * the the storage string is interpreted as index pointing into the
	 * enumeration's values.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public T fromStorageStrings( String[] aStringValues ) throws ParseException {
		if ( aStringValues == null || aStringValues.length == 0 ) {
			return null;
		}
		else if ( aStringValues.length == 1 ) {
			if ( aStringValues[0] == null ) {
				return null;
			}

			// ENUM name() |-->
			try {
				return Enum.valueOf( getType(), aStringValues[0] );
			}
			catch ( Exception ignore ) {}
			// ENUM name() |<--|

			// ENUM alias |-->
			if ( AliasAccessor.class.isAssignableFrom( getType() ) ) {
				for ( Object eValue : getType().getEnumConstants() ) {
					if ( aStringValues[0].equals( ( (AliasAccessor) eValue ).getAlias() ) ) {
						return (T) eValue;
					}
				}
			}
			// ENUM alias <--|

			// ENUM index |-->
			try {
				return getType().getEnumConstants()[Integer.parseInt( aStringValues[0] )];
			}
			catch ( NumberFormatException e ) {
				throw new ParseException( "The value \"" + aStringValues[0] + "\" cannot be converted to the type <" + getType().getName() + ">!", 0 );
			}
			// ENUM index <--|

		}
		throw new IllegalArgumentException( "The type <" + getType().getName() + "> is not an array type though the number of elements in the provided string array is <" + aStringValues.length + "> whereas only one element is being expected." );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}