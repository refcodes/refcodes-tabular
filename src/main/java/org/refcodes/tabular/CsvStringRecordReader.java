// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.zip.ZipException;

import org.refcodes.textual.CsvEscapeMode;

/**
 * THis implementation of the {@link CsvRecordReader} assumes that for ease of
 * use we just want to handle {@link String} columns.
 */
public class CsvStringRecordReader extends CsvRecordReader<String> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( Header<String> aHeader, File aCsvFile, boolean isStrict ) throws IOException {
		super( aHeader, aCsvFile, isStrict );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( Header<String> aHeader, File aCsvFile, char aCsvSeparator, boolean isStrict ) throws IOException {
		super( aHeader, aCsvFile, aCsvSeparator, isStrict );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( Header<String> aHeader, File aCsvFile, char aCsvSeparator ) throws IOException {
		super( aHeader, aCsvFile, aCsvSeparator );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( Header<String> aHeader, File aCsvFile, Charset aEncoding, boolean isStrict ) throws IOException {
		super( aHeader, aCsvFile, aEncoding, isStrict );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( Header<String> aHeader, File aCsvFile, Charset aEncoding, char aCsvSeparator, boolean isStrict ) throws IOException {
		super( aHeader, aCsvFile, aEncoding, aCsvSeparator, isStrict );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( Header<String> aHeader, File aCsvFile, Charset aEncoding, char aCsvSeparator ) throws IOException {
		super( aHeader, aCsvFile, aEncoding, aCsvSeparator );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( Header<String> aHeader, File aCsvFile, Charset aEncoding ) throws IOException {
		super( aHeader, aCsvFile, aEncoding );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( Header<String> aHeader, File aCsvFile ) throws IOException {
		super( aHeader, aCsvFile );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( Header<String> aHeader, InputStream aCsvInputStream, boolean isStrict ) throws IOException {
		super( aHeader, aCsvInputStream, isStrict );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( Header<String> aHeader, InputStream aCsvInputStream, char aCsvDelimiter ) throws IOException {
		super( aHeader, aCsvInputStream, aCsvDelimiter );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( Header<String> aHeader, InputStream aCsvInputStream, Charset aEncoding, boolean isStrict ) throws IOException {
		super( aHeader, aCsvInputStream, aEncoding, isStrict );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( Header<String> aHeader, InputStream aCsvInputStream, Charset aEncoding, char aCsvDelimiter ) throws IOException {
		super( aHeader, aCsvInputStream, aEncoding, aCsvDelimiter );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( Header<String> aHeader, InputStream aCsvInputStream, Charset aEncoding ) throws IOException {
		super( aHeader, aCsvInputStream, aEncoding );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( Header<String> aHeader, InputStream aCsvInputStream ) throws IOException {
		super( aHeader, aCsvInputStream );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvStringRecordReader( File aCsvFile, boolean isStrict, String... aHeader ) throws IOException {
		super( new StringHeader( aHeader ), aCsvFile, isStrict );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvStringRecordReader( File aCsvFile, char aCsvSeparator, boolean isStrict, String... aHeader ) throws IOException {
		super( new StringHeader( aHeader ), aCsvFile, aCsvSeparator, isStrict );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvStringRecordReader( File aCsvFile, char aCsvSeparator, String... aHeader ) throws IOException {
		super( new StringHeader( aHeader ), aCsvFile, aCsvSeparator );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvStringRecordReader( File aCsvFile, Charset aEncoding, boolean isStrict, String... aHeader ) throws IOException {
		super( new StringHeader( aHeader ), aCsvFile, aEncoding, isStrict );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvStringRecordReader( File aCsvFile, Charset aEncoding, char aCsvSeparator, boolean isStrict, String... aHeader ) throws IOException {
		super( new StringHeader( aHeader ), aCsvFile, aEncoding, aCsvSeparator, isStrict );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvStringRecordReader( File aCsvFile, Charset aEncoding, char aCsvSeparator, String... aHeader ) throws IOException {
		super( new StringHeader( aHeader ), aCsvFile, aEncoding, aCsvSeparator );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvStringRecordReader( File aCsvFile, Charset aEncoding, String... aHeader ) throws IOException {
		super( new StringHeader( aHeader ), aCsvFile, aEncoding );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvStringRecordReader( File aCsvFile, String... aHeader ) throws IOException {
		super( new StringHeader( aHeader ), aCsvFile );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public CsvStringRecordReader( InputStream aCsvInputStream, boolean isStrict, String... aHeader ) throws IOException {
		super( new StringHeader( aHeader ), aCsvInputStream, isStrict );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aCsvDelimiter The delimiter being expected for the CSV input
	 *        stream.
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public CsvStringRecordReader( InputStream aCsvInputStream, char aCsvDelimiter, String... aHeader ) throws IOException {
		super( new StringHeader( aHeader ), aCsvInputStream, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public CsvStringRecordReader( InputStream aCsvInputStream, Charset aEncoding, boolean isStrict, String... aHeader ) throws IOException {
		super( new StringHeader( aHeader ), aCsvInputStream, aEncoding, isStrict );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding the encoding
	 * @param aCsvDelimiter The delimiter being expected for the CSV input
	 *        stream.
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public CsvStringRecordReader( InputStream aCsvInputStream, Charset aEncoding, char aCsvDelimiter, String... aHeader ) throws IOException {
		super( new StringHeader( aHeader ), aCsvInputStream, aEncoding, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public CsvStringRecordReader( InputStream aCsvInputStream, Charset aEncoding, String... aHeader ) throws IOException {
		super( new StringHeader( aHeader ), aCsvInputStream, aEncoding );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public CsvStringRecordReader( InputStream aCsvInputStream, String... aHeader ) throws IOException {
		super( new StringHeader( aHeader ), aCsvInputStream );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvStringRecordReader( String[] aHeader, File aCsvFile, boolean isStrict ) throws IOException {
		super( new StringHeader( aHeader ), aCsvFile, isStrict );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvStringRecordReader( String[] aHeader, File aCsvFile, char aCsvSeparator, boolean isStrict ) throws IOException {
		super( new StringHeader( aHeader ), aCsvFile, aCsvSeparator, isStrict );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvStringRecordReader( String[] aHeader, File aCsvFile, char aCsvSeparator ) throws IOException {
		super( new StringHeader( aHeader ), aCsvFile, aCsvSeparator );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvStringRecordReader( String[] aHeader, File aCsvFile, Charset aEncoding, boolean isStrict ) throws IOException {
		super( new StringHeader( aHeader ), aCsvFile, aEncoding, isStrict );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvStringRecordReader( String[] aHeader, File aCsvFile, Charset aEncoding, char aCsvSeparator, boolean isStrict ) throws IOException {
		super( new StringHeader( aHeader ), aCsvFile, aEncoding, aCsvSeparator, isStrict );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param aCsvSeparator The delimiter being expected for the CSV input
	 *        stream.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvStringRecordReader( String[] aHeader, File aCsvFile, Charset aEncoding, char aCsvSeparator ) throws IOException {
		super( new StringHeader( aHeader ), aCsvFile, aEncoding, aCsvSeparator );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvStringRecordReader( String[] aHeader, File aCsvFile, Charset aEncoding ) throws IOException {
		super( new StringHeader( aHeader ), aCsvFile, aEncoding );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * @param aCsvFile The CSV {@link File} which to parse.
	 * 
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException in case there were problems working with the given
	 *         {@link File}.
	 * @throws ZipException thrown in case processing a ZIP compressed file
	 *         encountered problems.
	 */
	public CsvStringRecordReader( String[] aHeader, File aCsvFile ) throws IOException {
		super( new StringHeader( aHeader ), aCsvFile );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public CsvStringRecordReader( String[] aHeader, InputStream aCsvInputStream, boolean isStrict ) throws IOException {
		super( new StringHeader( aHeader ), aCsvInputStream, isStrict );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aCsvDelimiter The delimiter being expected for the CSV input
	 *        stream.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public CsvStringRecordReader( String[] aHeader, InputStream aCsvInputStream, char aCsvDelimiter ) throws IOException {
		super( new StringHeader( aHeader ), aCsvInputStream, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * @param isStrict When true, then parsing will abort with an exception in
	 *        case of parsing problems, else parsing is gracefully continued and
	 *        erroneous records are skipped. The error count
	 *        {@link #getErroneousRecordCount()} is incremented by each
	 *        erroneous {@link Record}.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public CsvStringRecordReader( String[] aHeader, InputStream aCsvInputStream, Charset aEncoding, boolean isStrict ) throws IOException {
		super( new StringHeader( aHeader ), aCsvInputStream, aEncoding, isStrict );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 *
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding the encoding
	 * @param aCsvDelimiter The delimiter being expected for the CSV input
	 *        stream.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public CsvStringRecordReader( String[] aHeader, InputStream aCsvInputStream, Charset aEncoding, char aCsvDelimiter ) throws IOException {
		super( new StringHeader( aHeader ), aCsvInputStream, aEncoding, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 * 
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * @param aEncoding The {@link Charset} for the character encoding to use.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public CsvStringRecordReader( String[] aHeader, InputStream aCsvInputStream, Charset aEncoding ) throws IOException {
		super( new StringHeader( aHeader ), aCsvInputStream, aEncoding );
	}

	/**
	 * Constructs a {@link CsvStringRecordReader} with the given parameters.
	 * Internally {@link Column} instances are generated according to the keys
	 * found in the CSV top line. The {@link Column} instances are required to
	 * convert the CSV line values. If a {@link Header} is provided, then the
	 * {@link Header} is used for generating the {@link Column} instances
	 * instead of the top line of the CSV file.
	 * 
	 * @param aHeader The {@link String} elements representing the
	 *        {@link Header} to use when parsing the input.
	 * @param aCsvInputStream The CSV {@link InputStream} which to parse.
	 * 
	 * @throws IOException in case there were problems working with the given
	 *         {@link InputStream}.
	 */
	public CsvStringRecordReader( String[] aHeader, InputStream aCsvInputStream ) throws IOException {
		super( new StringHeader( aHeader ), aCsvInputStream );
	}

	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( File aCsvFile, boolean isStrict ) throws IOException {
		super( new StringColumnFactory(), aCsvFile, isStrict );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( File aCsvFile, char aCsvSeparator, boolean isStrict ) throws IOException {
		super( new StringColumnFactory(), aCsvFile, aCsvSeparator, isStrict );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( File aCsvFile, char aCsvSeparator ) throws IOException {
		super( new StringColumnFactory(), aCsvFile, aCsvSeparator );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( File aCsvFile ) throws IOException {
		super( new StringColumnFactory(), aCsvFile );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( InputStream aCsvInputStream, boolean isStrict ) throws IOException {
		super( new StringColumnFactory(), aCsvInputStream, isStrict );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( InputStream aCsvInputStream, char aCsvDelimiter, boolean isStrict ) throws IOException {
		super( new StringColumnFactory(), aCsvInputStream, aCsvDelimiter, isStrict );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( InputStream aCsvInputStream, char aCsvDelimiter ) throws IOException {
		super( new StringColumnFactory(), aCsvInputStream, aCsvDelimiter );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( InputStream aCsvInputStream ) throws IOException {
		super( new StringColumnFactory(), aCsvInputStream );
	}

	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( File aCsvFile, Charset aEncoding, boolean isStrict ) throws IOException {
		super( new StringColumnFactory(), aCsvFile, aEncoding, isStrict );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( File aCsvFile, Charset aEncoding, char aCsvSeparator, boolean isStrict ) throws IOException {
		super( new StringColumnFactory(), aCsvFile, aEncoding, aCsvSeparator, isStrict );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( File aCsvFile, Charset aEncoding, char aCsvSeparator ) throws IOException {
		super( new StringColumnFactory(), aCsvFile, aEncoding, aCsvSeparator );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( File aCsvFile, Charset aEncoding ) throws IOException {
		super( new StringColumnFactory(), aCsvFile, aEncoding );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( InputStream aCsvInputStream, Charset aEncoding, boolean isStrict ) throws IOException {
		super( new StringColumnFactory(), aCsvInputStream, aEncoding, isStrict );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( InputStream aCsvInputStream, Charset aEncoding, char aCsvDelimiter, boolean isStrict ) throws IOException {
		super( new StringColumnFactory(), aCsvInputStream, aEncoding, aCsvDelimiter, isStrict );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( InputStream aCsvInputStream, Charset aEncoding, char aCsvDelimiter ) throws IOException {
		super( new StringColumnFactory(), aCsvInputStream, aEncoding, aCsvDelimiter );
	}

	/**
	 * {@inheritDoc}
	 */
	public CsvStringRecordReader( InputStream aCsvInputStream, Charset aEncoding ) throws IOException {
		super( new StringColumnFactory(), aCsvInputStream, aEncoding );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CsvStringRecordReader withTrim( boolean isTrimRecords ) {
		setTrim( isTrimRecords );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CsvStringRecordReader withCsvEscapeMode( CsvEscapeMode aCsvEscapeMode ) {
		setCsvEscapeMode( aCsvEscapeMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CsvStringRecordReader withDelimiter( char aCsvDelimiter ) {
		setDelimiter( aCsvDelimiter );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CsvStringRecordReader withCommentPrefixes( String... aCommentPrefixes ) {
		setCommentPrefixes( aCommentPrefixes );
		return this;
	}
}
