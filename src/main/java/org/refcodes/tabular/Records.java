// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import java.util.Iterator;

/**
 * A bunch of {@link Record} instances is represented by a {@link Records}
 * instance. As of efficiency reasons, a {@link Records} instance can be
 * iterated through, returning a single {@link Record} within each iteration.
 * This approach is making it possible to query a DB in the background and just
 * hold the {@link Record} instances being served next in the {@link Records}
 * instance (in memory).
 * <p>
 * The way how {@link Records} instances manage their related {@link Record}
 * instances is implementation specific, a plain java implementation might use
 * some kind of collection for storing the {@link Record} instances (this can
 * get memory intensive and not applicable when working with big data). A DB
 * implementation might retrieve the {@link Record} instances in blocks one by
 * one, one after the other, by querying the DB accordingly (in terms of "next"
 * result sets).
 *
 * @param <T> The type managed by the {@link Records}.
 */
public interface Records<T> extends Iterable<Record<T>>, Iterator<Record<T>> {

	/**
	 * {@inheritDoc}
	 */
	@Override
	default Iterator<Record<T>> iterator() {
		return this;
	}
}
