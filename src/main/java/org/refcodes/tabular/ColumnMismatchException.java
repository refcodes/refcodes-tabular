// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import org.refcodes.tabular.TabularException.ColumnException.ColumnValueException;

/**
 * Thrown in case a value was found in the e.g. in a {@link Record} of the wrong
 * type than specified by a provided {@link Column} (of for example a
 * {@link Header}).
 */
public class ColumnMismatchException extends ColumnValueException {

	private static final long serialVersionUID = 1L;

	/**
	 * {@inheritDoc}
	 */
	public ColumnMismatchException( String aMessage, Column<?> aColumn, Object aValue, String aErrorCode ) {
		super( aMessage, aColumn, aValue, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public ColumnMismatchException( Column<?> aColumn, Object aValue, String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aColumn, aValue, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public ColumnMismatchException( Column<?> aColumn, Object aValue, String aMessage, Throwable aCause ) {
		super( aMessage, aColumn, aValue, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public ColumnMismatchException( String aMessage, Column<?> aColumn, Object aValue ) {
		super( aMessage, aColumn, aValue );
	}

	/**
	 * {@inheritDoc}
	 */
	public ColumnMismatchException( Column<?> aColumn, Object aValue, Throwable aCause, String aErrorCode ) {
		super( aColumn, aValue, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public ColumnMismatchException( Column<?> aColumn, Object aValue, Throwable aCause ) {
		super( aColumn, aValue, aCause );
	}
}
