// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import java.util.ArrayList;

/**
 * Implementation of the {@link Row} interface being {@link Cloneable}.
 *
 * @param <T> The type managed by the {@link Row}.
 */
public class RowImpl<T> extends ArrayList<T> implements Row<T> {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link RowImpl} instance configured with the provided
	 * elements.
	 * 
	 * @param aValues The elements to be contained in the {@link RowImpl} in the
	 *        order as passed.
	 */
	@SafeVarargs
	public RowImpl( T... aValues ) {
		for ( T eValue : aValues ) {
			add( eValue );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T get( Header<T> aHeader, String aKey ) throws HeaderMismatchException, ColumnMismatchException {
		final int theIndex = aHeader.indexOf( aKey );
		if ( theIndex == -1 ) {
			throw new HeaderMismatchException( "Unknwon header key \"" + aKey + "\", no such column.", aKey );
		}
		if ( theIndex > size() - 1 ) {
			throw new HeaderMismatchException( "Header out of bounds index at <" + theIndex + "> for key \"" + aKey + "\" for row (size <" + size() + ">).", aKey );
		}

		final T theValue = get( theIndex );
		final Column<? extends T> theColumn = aHeader.get( aKey );
		if ( !theColumn.getType().isAssignableFrom( theValue.getClass() ) ) {
			throw new ColumnMismatchException( "Column type <" + theColumn.getType().getName() + "> mismatch for key \"" + theColumn.getKey() + "\" with value <" + theValue.getClass().getName() + "> at index <" + theIndex + ">.", theColumn, theValue );
		}
		return theValue;
	}
}
