// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import org.refcodes.tabular.TabularException.KeyException;

/**
 * Thrown in case there is a mismatch between the given {@link Header} and a
 * {@link Row} (or another {@link Header}), i.e. the index for the given key in
 * a header may be out of index of a given row or the given key does not exist
 * in a {@link Header}.
 */
public class HeaderMismatchException extends KeyException {

	private static final long serialVersionUID = 1L;

	/**
	 * {@inheritDoc}
	 */
	public HeaderMismatchException( String aMessage, String aKey, String aErrorCode ) {
		super( aMessage, aKey, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public HeaderMismatchException( String aMessage, String aKey, Throwable aCause, String aErrorCode ) {
		super( aMessage, aKey, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public HeaderMismatchException( String aMessage, String aKey, Throwable aCause ) {
		super( aMessage, aKey, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public HeaderMismatchException( String aMessage, String aKey ) {
		super( aMessage, aKey );
	}

	/**
	 * {@inheritDoc}
	 */
	public HeaderMismatchException( String aKey, Throwable aCause, String aErrorCode ) {
		super( aKey, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public HeaderMismatchException( String aKey, Throwable aCause ) {
		super( aKey, aCause );
	}
}
