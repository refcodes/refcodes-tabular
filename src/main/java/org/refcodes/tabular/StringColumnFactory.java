// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import java.util.Map;

/**
 * Most generic {@link ColumnFactory} for creating {@link Column} instances
 * managing instances of the (sub-)type {@link String}.
 */
public class StringColumnFactory implements ColumnFactory<String> {

	// /////////////////////////////////////////////////////////////////////////
	// FACTORY:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Column<String> create( String aKey ) {
		return new StringColumn( aKey );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Column<String> create( String aKey, Map<String, String> aProperties ) {
		return new StringColumn( aKey );
	}
}