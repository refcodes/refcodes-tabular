package org.refcodes.tabular;

/**
 * The Enum PrintStackTrace.
 */
public enum PrintStackTrace {

	EXPLODED,

	COMPACT,

	NONE
}