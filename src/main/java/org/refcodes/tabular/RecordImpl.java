// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

/**
 * Implementation of the {@link Record} interface being {@link Cloneable}.
 *
 * @param <T> The type managed by the {@link Record}.
 */
public class RecordImpl<T> extends LinkedHashMap<String, T> implements Record<T> {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link RecordImpl} instance.
	 */
	public RecordImpl() {}

	/**
	 * Constructs the {@link RecordImpl} instance configured with the provided
	 * {@link Field} instances.
	 * 
	 * @param aFields The {@link Field} instances to be contained in the
	 *        {@link RecordImpl}.
	 */
	@SafeVarargs
	public RecordImpl( Field<? extends T>... aFields ) {
		for ( Field<? extends T> eField : aFields ) {
			if ( put( eField.getKey(), eField.getValue() ) != null ) {
				throw new IllegalArgumentException( "A field with key \"" + eField.getKey() + "\" already exists, cannot add a field with the same key twice." );
			}
		}
	}

	/**
	 * Constructs a record with the given items.
	 * 
	 * @param aFields The items to be added.
	 */
	@SuppressWarnings("unchecked")
	public RecordImpl( List<Field<T>> aFields ) {
		this( aFields.toArray( new Field[aFields.size()] ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Set<Field<T>> toFieldSet() {
		final Set<Field<T>> theFieldSet = new HashSet<>();
		for ( String eKey : keySet() ) {
			theFieldSet.add( new FieldImpl( eKey, get( eKey ) ) );
		}
		return theFieldSet;
	}
}
