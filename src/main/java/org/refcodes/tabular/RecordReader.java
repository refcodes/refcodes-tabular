// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import java.io.IOException;

import org.refcodes.io.RowReader;

/**
 * Extends the {@link Records} with functionality for file based implementations
 * regarding header management and means to monitor the state of reading data.
 * Reading {@link Record} instances may aCause problems but no abortion of an
 * operation (if desired), the statistics of the problems may be reported by an
 * implementation of this interface.
 *
 * @param <T> The type managed by the {@link Records}.
 */
public interface RecordReader<T> extends AutoCloseable, Records<T>, HeaderAccessor<T>, RowReader<String[]> {

	/**
	 * The number of erroneous records which were not read by the
	 * {@link RecordReader}.
	 *
	 * @return the erroneous record count
	 */
	long getErroneousRecordCount();

	/**
	 * Creates a {@link Header} from the next line to be read. In case a header
	 * has already been provided (in the constructor), then the according header
	 * is matched against the next line being read and ordered accordingly,
	 * placeholder {@link Column} instances are inserted if needed, created by
	 * the provided (default) {@link ColumnFactory}.
	 * 
	 * @return Returns the {@link Header} being read.
	 * 
	 * @throws IOException thrown in case reading from IO failed.
	 */
	Header<T> readHeader() throws IOException;

	/**
	 * Creates a {@link Header} from the next line to be read. The provided
	 * header is matched against the next line being read and ordered
	 * accordingly, place holder {@link Column} instances are inserted if
	 * needed, created by the provided (default) {@link ColumnFactory}.
	 *
	 * @param aColumns The {@link Column} instances for creating the actual
	 *        {@link Header}.
	 * 
	 * @return Returns the {@link Header} being read.
	 * 
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@SuppressWarnings("unchecked")
	Header<T> readHeader( Column<T>... aColumns ) throws IOException;

	/**
	 * Creates a {@link Header} from the next line to be read. The provided
	 * header is matched against the next line being read and ordered
	 * accordingly, place holder {@link Column} instances are inserted if
	 * needed, created by the provided (default) {@link ColumnFactory}.
	 *
	 * @param aHeader The {@link Header} for creating the actual {@link Header}.
	 * 
	 * @return Returns the {@link Header} being read.
	 * 
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	Header<T> readHeader( Header<T> aHeader ) throws IOException;

	/**
	 * Skips the next line to be read. This line will not be processed by the
	 * {@link #next()} method. This methods is useful when skipping the first
	 * line (header).
	 *
	 * @return The line being skipped.
	 * 
	 * @throws IOException thrown in case if IO problems.
	 */
	String skipHeader() throws IOException;

	/**
	 * Reads the next line from the (standard) input (stream or file) and
	 * returns an array of its {@link String} representation.
	 *
	 * @return The {@link String} array representation of the next
	 *         {@link Record} instance read.
	 */
	@Override
	String[] nextRow();

	/**
	 * Reads the next line from the (standard) input (stream or file) and
	 * converts it to the given type which's instance is then returned. The
	 * {@link Header} keys must match the type's properties.
	 *
	 * @param <TYPE> The type of the element to which the line to be read is to
	 *        be converted to.
	 * @param aClass the type of the instance to be created.
	 * 
	 * @return The instance of the type to which the {@link Record} is being
	 *         converted to.
	 */
	default <TYPE> TYPE nextType( Class<TYPE> aClass ) {
		return next().toType( aClass );
	}

	/**
	 * Merges the primary {@link Header} with the supplement {@link Header}. A
	 * copy of the primary {@link Header} is returned with any {@link Column}
	 * replaced in the primary {@link Header} with a supplement {@link Header}'s
	 * {@link Column} of the same key.
	 *
	 * @param aPrimaryHeader the primary header
	 * @param aSupplementHeader the supplement header
	 * 
	 * @return The accordingly merged {@link Header}.
	 */
	default Header<T> toHeader( Header<T> aPrimaryHeader, Header<T> aSupplementHeader ) {
		final Header<T> theTargetHeader;
		theTargetHeader = new HeaderImpl<>();
		for ( int i = 0; i < aPrimaryHeader.size(); i++ ) {
			out: {
				if ( aSupplementHeader != null && aSupplementHeader.size() != 0 ) {
					for ( int j = 0; j < aSupplementHeader.size(); j++ ) {
						if ( aPrimaryHeader.get( i ).getKey().equals( aSupplementHeader.get( j ).getKey() ) ) {
							theTargetHeader.add( aSupplementHeader.get( j ) );
							break out;
						}
					}
				}
				theTargetHeader.add( aPrimaryHeader.get( i ) );
			}
		}
		return theTargetHeader;
	}
}
