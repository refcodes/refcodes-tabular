// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

/**
 * This archetype helps processing table like data structures including the
 * processing of
 * <a href="https://en.wikipedia.org/wiki/Comma-separated_values">CSV</a> files
 * with records, headers as well as comments whilst supporting
 * <a href="https://en.wikipedia.org/wiki/Plain_old_Java_object">Plain old Java
 * objects</a> (POJO) and simple new Java
 * <a href="https://openjdk.java.net/jeps/395">record</a> types.
 * 
 * <p style="font-style: plain; font-weight: normal; background-color: #f8f8ff;
 * padding: 1.5rem; border-style: solid; border-width: 1pt; border-radius: 10pt;
 * border-color: #cccccc;text-align: center;">
 * Please refer to the <a href=
 * "https://www.metacodes.pro/refcodes/refcodes-tabular"><strong>refcodes-tabular:
 * Process tabular data and CSV files using POJOs</strong></a> documentation for
 * an up-to-date and detailed description on the usage of this artifact.
 * </p>
 * 
 */
package org.refcodes.tabular;
