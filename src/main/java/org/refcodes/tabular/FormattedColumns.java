// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// /////////////////////////////////////////////////////////////////////////----
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// /////////////////////////////////////////////////////////////////////////----
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// /////////////////////////////////////////////////////////////////////////----
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

/**
 * In case no order of the {@link FormattedColumn} instances is explicitly
 * required (similar to a DB table or a spread sheet, in contrast to a CSV file)
 * then {@link FormattedColumn} instances are grouped by a
 * {@link FormattedColumns} set.
 * <p>
 * {@link FormattedColumns} provide additional semantics to the {@link Field}
 * instances stored in a {@link Record}.
 *
 * @param <T> The type managed by the {@link FormattedColumns}.
 */
public interface FormattedColumns<T> extends ColumnRow<T, FormattedColumn<? extends T>> {

	/**
	 * With columns.
	 *
	 * @param aColumns the columns
	 * 
	 * @return the formatted columns
	 */
	@SuppressWarnings("unchecked")
	@Override
	default FormattedColumns<T> withColumns( FormattedColumn<? extends T>... aColumns ) {
		for ( FormattedColumn<? extends T> eColumn : aColumns ) {
			add( eColumn );
		}
		return this;
	}
}
