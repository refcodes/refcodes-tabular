// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

/**
 * Provides an accessor for a {@link Column} property.
 *
 * @param <T> the generic type
 */
public interface ColumnAccessor<T> {

	/**
	 * Retrieves the {@link Column} property.
	 * 
	 * @return The {@link Column} being stored by this property.
	 */
	Column<T> getColumn();

	/**
	 * Provides a mutator for a {@link Column} property.
	 *
	 * @param <T> the generic type
	 */
	public interface ColumnMutator<T> {

		/**
		 * Sets the {@link Column} property.
		 * 
		 * @param aColumn The {@link Column} to be stored by this property.
		 */
		void setColumn( Column<T> aColumn );
	}

	/**
	 * Provides a {@link Column} property.
	 *
	 * @param <T> the generic type
	 */
	public interface ColumnProperty<T> extends ColumnAccessor<T>, ColumnMutator<T> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Column} (setter)
		 * as of {@link #setColumn(Column)} and returns the very same value
		 * (getter).
		 * 
		 * @param aColumn The {@link Column} to set (via
		 *        {@link #setColumn(Column)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Column<T> letColumn( Column<T> aColumn ) {
			setColumn( aColumn );
			return aColumn;
		}
	}
}
