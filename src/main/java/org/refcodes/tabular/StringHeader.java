// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

/**
 * A {@link Header} implementation dedicated to the {@link String} type.
 */
public class StringHeader extends HeaderImpl<String> {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a {@link Header} for managing {@link String} instances.
	 * 
	 * @param aHeader The elements representing the {@link Header}'s
	 *        {@link Column} instances.
	 */
	public StringHeader( String... aHeader ) {
		super( toColumns( aHeader ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	@SuppressWarnings("unchecked")
	private static Column<String>[] toColumns( String... aHeader ) {
		final Column<String>[] theColumns = new Column[aHeader.length];
		String eKey;
		for ( int i = 0; i < aHeader.length; i++ ) {
			eKey = aHeader[i];
			theColumns[i] = new StringColumn( eKey );
		}
		return theColumns;

	}
}
