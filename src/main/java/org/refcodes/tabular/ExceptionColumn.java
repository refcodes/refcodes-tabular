// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;

import org.refcodes.data.Text;
import org.refcodes.exception.Trap;

/**
 * The Class ExceptionColumnImpl.
 */
public class ExceptionColumn extends AbstractColumn<Throwable> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private PrintStackTrace _printStackTrace;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates an {@link ExceptionColumn} which will consider the stack trace in
	 * its string representation {@link #toStorageString(Object)}.
	 * 
	 * @param aKey The key for the {@link ExceptionColumn}.
	 */
	public ExceptionColumn( String aKey ) {
		this( aKey, PrintStackTrace.EXPLODED );
	}

	/**
	 * Creates an {@link ExceptionColumn} which can consider or omit the stack
	 * trace in its string representation {@link #toStorageString(Object)}.
	 *
	 * @param aKey The key for the {@link ExceptionColumn}.
	 * @param aPrintStackTrace the print stack trace
	 */
	public ExceptionColumn( String aKey, PrintStackTrace aPrintStackTrace ) {
		super( aKey, Throwable.class );
		_printStackTrace = aPrintStackTrace;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toStorageStrings( Throwable aValue ) {
		switch ( _printStackTrace ) {
		case COMPACT:
			return new String[] { Trap.asMessage( aValue ) + ": " + toStackTrace( aValue ) };
		case NONE:
			return new String[] { Trap.asMessage( aValue ) };
		case EXPLODED:
		default:
			return new String[] { Trap.asMessage( aValue ) + ":\n" + toStackTrace( aValue ) };
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized Throwable fromStorageStrings( String[] aStringValues ) throws ParseException {
		throw new UnsupportedOperationException( Text.UNSUPPORTED_OPERATION.getText() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toPrintable( Throwable aValue ) {
		switch ( _printStackTrace ) {
		case COMPACT:
			return Trap.asMessage( aValue ) + ": " + toStackTrace( aValue );
		case NONE:
			return Trap.asMessage( aValue );
		case EXPLODED:
		default:
			return aValue.getMessage() + ":\n" + toStackTrace( aValue );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieves a stack trace from the given exception.
	 * 
	 * @param aThrowable The exception from which to get the stack trace.
	 * 
	 * @return The stack trace from the exception.
	 */
	private static String toStackTrace( Throwable aThrowable ) {
		String eIndent = "";
		final String theAppend = ">>";
		final StringBuilder theBuffer = new StringBuilder();
		final Set<Throwable> theThrowables = new HashSet<>();
		Throwable eThrowable = aThrowable;
		while ( eThrowable != null ) {
			if ( theThrowables.contains( eThrowable ) ) {
				break;
			}
			theThrowables.add( eThrowable );
			if ( aThrowable != eThrowable ) {
				theBuffer.append( "\n" );
			}
			theBuffer.append( eIndent + "> Exception <" + eThrowable.getClass().getName() + ">:\n" );
			eIndent = eIndent + theAppend;
			StackTraceElement eElement;
			for ( int i = 0; i < eThrowable.getStackTrace().length; i++ ) {
				eElement = eThrowable.getStackTrace()[i];
				theBuffer.append( eIndent + " " + eElement.toString() );
				if ( i < eThrowable.getStackTrace().length - 1 ) {
					theBuffer.append( "\n" );
				}
			}
			eThrowable = eThrowable.getCause();
		}
		return theBuffer.toString();
	}
}