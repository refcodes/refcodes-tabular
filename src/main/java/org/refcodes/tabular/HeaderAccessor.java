// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

/**
 * Provides an accessor for a {@link Header} property.
 *
 * @param <T> The type managed by the {@link Header}.
 */
public interface HeaderAccessor<T> {

	/**
	 * Retrieves the {@link Header} property.
	 * 
	 * @return The {@link Header} being stored by this property.
	 */
	Header<T> getHeader();

	/**
	 * Provides a mutator for a {@link Header} property.
	 *
	 * @param <T> the generic type
	 */
	public interface HeaderMutator<T> {

		/**
		 * Sets the {@link Header} property.
		 * 
		 * @param aHeader The {@link Header} to be stored by this property.
		 */
		void setHeader( Header<T> aHeader );
	}

	/**
	 * Provides a mutator for an header property.
	 * 
	 * @param <T> The managed by the builder which implements the
	 *        {@link HeaderBuilder}.
	 */
	public interface HeaderBuilder<T extends HeaderBuilder<?>> {

		/**
		 * Sets the {@link Header}to use returns this builder as of the builder
		 * pattern.
		 * 
		 * @param aHeader The {@link Header} to be stored by this property.
		 * 
		 * @return This {@link HeaderBuilder} instance to continue
		 *         configuration.
		 */
		T withHeader( Header<T> aHeader );
	}

	/**
	 * Provides a {@link Header} property.
	 *
	 * @param <T> the generic type
	 */
	public interface HeaderProperty<T> extends HeaderAccessor<T>, HeaderMutator<T> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setHeader(Header)} and returns the very same value (getter).
		 * 
		 * @param aHeader The value to set (via {@link #setHeader(Header)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Header<T> letHeader( Header<T> aHeader ) {
			setHeader( aHeader );
			return aHeader;
		}
	}
}
