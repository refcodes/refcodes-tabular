package org.refcodes.tabular;

/**
 * Implementation of the {@link FormattedColumn} interface.
 *
 * @param <T> The type managed by the {@link Header}.
 */
public class FormattedColumnImpl<T> extends FormattedColumnDecorator<T> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an instance of the {@link FormattedColumn} type.
	 * 
	 * @param aKey The key for the {@link Column}.
	 * @param aType The type to be used.
	 */
	public FormattedColumnImpl( String aKey, Class<T> aType ) {
		super( aKey, aType );
	}
}
