module org.refcodes.tabular {
	requires org.refcodes.data;
	requires org.refcodes.time;
	requires transitive org.refcodes.component;
	requires transitive org.refcodes.controlflow;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.factory;
	requires transitive org.refcodes.graphical;
	requires transitive org.refcodes.mixin;
	requires transitive org.refcodes.struct;
	requires transitive org.refcodes.textual;
	requires org.refcodes.io;
	requires org.refcodes.runtime;

	exports org.refcodes.tabular;
}
