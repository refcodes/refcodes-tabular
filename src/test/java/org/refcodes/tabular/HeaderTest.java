// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import static org.junit.jupiter.api.Assertions.*;
import java.text.ParseException;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.textual.CsvBuilder;
import org.refcodes.textual.CsvEscapeMode;

public class HeaderTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Header<Object> HEADER = new HeaderImpl<>( new ObjectColumn<Integer>( "id", Integer.class ), new StringColumn( "alias" ), new StringColumn( "name" ), new StringColumn( "description" ), new StringsColumn( "array" ), new ObjectColumn<Object[]>( "details", Object[].class ) );

	// @formatter:off
	@SuppressWarnings("unchecked")
	private  static final Row<Object>[] ROWS = new Row[] {
			new RowImpl<Object>( 0, "alias0", "name0", "description0", new String[] {"01", "02", "03"}, new Object[] {"A", "B", "C"} ), 
			new RowImpl<Object>( 1, "alias1", "name1", "description1", new String[] {"04", "05", "06"}, new Object[] {"D", "E", "F"} ), 
			new RowImpl<Object>( 2, "alias2", "name2", "description2", new String[] {"07", "08", "09"}, new Object[] {"G", "H", "I"} ), 
			new RowImpl<Object>( 3, "alias3", "name3", "description3", new String[] {"10", "11", "12"}, new Object[] {"J", "K", "L"} ), 
			new RowImpl<Object>( 4, "alias4", "name4", "description4", new String[] {"13", "14", "15"}, new Object[] {"M", "N", "O"} ), 
			new RowImpl<Object>( 5, "alias5", "name5", "description5", new String[] {"16", "17", "18"}, new Object[] {"P", "Q", "R"} ), 
			new RowImpl<Object>( 6, "alias6", "name6", "description6", new String[] {"19", "20", "21"}, new Object[] {"S", "T", "U"} ),
			new RowImpl<Object>( 7, "alias7", "name7", "description7", new String[] {"22", "23", "24"}, new Object[] {"V", "W", "X"} ), 
			new RowImpl<Object>( 8, "alias8", "name8", "description8", new String[] {"25", "26", "27"}, new Object[] {"Y", "Z", "Ä"} ), 
			new RowImpl<Object>( 9, "alias9", "name9", "description9", new String[] {"28", "29", "30"}, new Object[] {"Ö", "Ü", "ß"} )
	};
	// @formatter:on

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testRowIsEquivalent1() throws HeaderMismatchException, ColumnMismatchException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "*** Testing equality of rows with a header (1) ***".toUpperCase() );
		}
		final Rows<?> e = new RowsImpl<Object>( HEADER, ROWS );
		Row<?> eRow;
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Header := " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( HEADER.keySet() ).toRecord() );
		}
		while ( e.hasNext() ) {
			eRow = e.next();
			assertTrue( HEADER.isEqualWith( eRow ) );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Row := " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eRow ).toRecord() );
			}
		}
	}

	@Test
	public void testRowIsEquivalent2() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "*** Testing equality of rows with a header (2) ***".toUpperCase() );
		}
		try {
			/* Rows e = */new RowsImpl<Object>( HEADER, new RowImpl<Object>( 0, 2, "name1", "description1" ) );
		}
		catch ( IllegalArgumentException e ) {
			// Expected
		}
	}

	@Test
	public void testStorageStringRecord() throws HeaderMismatchException, ColumnMismatchException, ParseException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "*** Testing conversion of a record to a storage strings record and back ***".toUpperCase() );
		}
		final Rows<Object> e = new RowsImpl<>( HEADER, ROWS );
		Row<?> eRow;
		Record<?> eRecord;
		Record<?> eFromStorageRecord;
		Record<String> eToStorageRecord;
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Header := " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( HEADER.keySet() ).toRecord() );
		}
		while ( e.hasNext() ) {
			eRow = e.next();
			assertTrue( HEADER.isEqualWith( eRow ) );
			eRecord = HEADER.toRecord( eRow );
			eToStorageRecord = HEADER.toStorageString( eRecord );
			eFromStorageRecord = HEADER.fromStorageString( eToStorageRecord );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Record --> To storage record --> From storage record := " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eRecord.values() ).toRecord() + " --> " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eToStorageRecord.values() ).toRecord() + " --> " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eFromStorageRecord.values() ).toRecord() );
			}
			assertEquals( new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eRecord.keySet() ).toRecord(), new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eFromStorageRecord.keySet() ).toRecord() );
			assertEquals( new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eRecord.values() ).toRecord(), new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eFromStorageRecord.values() ).toRecord() );
			assertEquals( new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eRow ).toRecord(), new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( HEADER.toRow( eFromStorageRecord ) ).toRecord() );
		}
	}

	@Test
	public void testStorageStringsRecord() throws HeaderMismatchException, ColumnMismatchException, ParseException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "*** Testing conversion of a record to a storage string arrays record and back ***".toUpperCase() );
		}
		final Rows<Object> e = new RowsImpl<>( HEADER, ROWS );
		Row<?> eRow;
		Record<?> eRecord;
		Record<?> eFromStorageRecord;
		Record<String[]> eToStorageRecord;
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Header := " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( HEADER.keySet() ).toRecord() );
		}
		while ( e.hasNext() ) {
			eRow = e.next();
			assertTrue( HEADER.isEqualWith( eRow ) );
			eRecord = HEADER.toRecord( eRow );
			eToStorageRecord = HEADER.toStorageStrings( eRecord );
			eFromStorageRecord = HEADER.fromStorageStrings( eToStorageRecord );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Record --> To storage record --> From storage record := " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eRecord.values() ).toRecord() + " --> " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eToStorageRecord.values() ).toRecord() + " --> " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eFromStorageRecord.values() ).toRecord() );
			}
			assertEquals( new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eRecord.keySet() ).toRecord(), new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eFromStorageRecord.keySet() ).toRecord() );
			assertEquals( new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eRecord.values() ).toRecord(), new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eFromStorageRecord.values() ).toRecord() );
			assertEquals( new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eRow ).toRecord(), new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( HEADER.toRow( eFromStorageRecord ) ).toRecord() );
		}
	}

	@Test
	public void testPrintableRecord() throws HeaderMismatchException, ColumnMismatchException, ParseException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "*** Testing conversion of a record to a printable record ***".toUpperCase() );
		}
		final Rows<Object> e = new RowsImpl<>( HEADER, ROWS );
		Row<?> eRow;
		Record<?> eRecord;
		Record<String> eToPrintableRecord;
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Header := " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( HEADER.keySet() ).toRecord() );
		}
		while ( e.hasNext() ) {
			eRow = e.next();
			assertTrue( HEADER.isEqualWith( eRow ) );
			eRecord = HEADER.toRecord( eRow );
			eToPrintableRecord = HEADER.toPrintable( eRecord );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Record --> To printable record := " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eRecord.values() ).toRecord() + " --> " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eToPrintableRecord.values() ).toRecord() );
			}
		}
	}

	@Test
	public void testStorageStringRow() throws HeaderMismatchException, ColumnMismatchException, ParseException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "*** Testing conversion of a row to a storage strings row and back ***".toUpperCase() );
		}
		final Rows<?> e = new RowsImpl<Object>( HEADER, ROWS );
		Row<?> eRow;
		Row<?> eFromStorageRow;
		Row<String> eToStorageRow;
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Header := " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( HEADER.keySet() ).toRecord() );
		}
		while ( e.hasNext() ) {
			eRow = e.next();
			assertTrue( HEADER.isEqualWith( eRow ) );
			eToStorageRow = HEADER.toStorageString( eRow );
			eFromStorageRow = HEADER.fromStorageString( eToStorageRow );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Row --> To storage row --> From storage row := " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eRow ).toRecord() + " --> " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eToStorageRow ).toRecord() + " --> " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eFromStorageRow ).toRecord() );
			}
			assertEquals( new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eRow ).toRecord(), new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eFromStorageRow ).toRecord() );
		}
	}

	@Test
	public void testStorageStringsRow() throws HeaderMismatchException, ColumnMismatchException, ParseException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "*** Testing conversion of a row to a storage string arrays row and back ***".toUpperCase() );
		}
		final Rows<?> e = new RowsImpl<Object>( HEADER, ROWS );
		Row<?> eRow;
		Row<?> eFromStorageRow;
		Row<String[]> eToStorageRow;
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Header := " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( HEADER.keySet() ).toRecord() );
		}
		while ( e.hasNext() ) {
			eRow = e.next();
			assertTrue( HEADER.isEqualWith( eRow ) );
			eToStorageRow = HEADER.toStorageStrings( eRow );
			eFromStorageRow = HEADER.fromStorageStrings( eToStorageRow );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Row --> To storage row --> From storage row := " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eRow ).toRecord() + " --> " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eToStorageRow ).toRecord() + " --> " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eFromStorageRow ).toRecord() );
			}
			assertEquals( new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eRow ).toRecord(), new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eFromStorageRow ).toRecord() );
		}
	}

	@Test
	public void testPrintableRow() throws HeaderMismatchException, ColumnMismatchException, ParseException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "*** Testing conversion of a row to a printable row ***".toUpperCase() );
		}
		final Rows<?> e = new RowsImpl<Object>( HEADER, ROWS );
		Row<?> eRow;
		Row<String> eToPrintableRow;
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Header := " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( HEADER.keySet() ).toRecord() );
		}
		while ( e.hasNext() ) {
			eRow = e.next();
			assertTrue( HEADER.isEqualWith( eRow ) );
			eToPrintableRow = HEADER.toPrintable( eRow );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Row --> To storage row --> From storage row := " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eRow ).toRecord() + " --> " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( eToPrintableRow ).toRecord() );
			}
		}
	}

	@Test
	public void testToRow() throws HeaderMismatchException, ColumnMismatchException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "*** Testing conversion of a record to a row and back ***".toUpperCase() );
		}
		final Record<?> theRecord = HEADER.toRecord( ROWS[0] );
		final Row<?> theRow = HEADER.toRow( theRecord );
		assertEquals( new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( ROWS[0] ).toRecord(), new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( theRow ).toRecord() );
	}

	@Test
	public void testToRowIgnoreType() throws HeaderMismatchException, ColumnMismatchException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "*** Testing conversion ignoring types of a record to a row and back ***".toUpperCase() );
		}
		final Record<String> theRecord = HEADER.toStorageStringRecord( ROWS[0] );
		Row<?> theRow;
		try {
			theRow = HEADER.toRow( theRecord );
			fail( "Should not reach this code!" );
		}
		catch ( ColumnMismatchException aException ) {
			/* expected */
		}
		theRow = HEADER.toRowIgnoreType( theRecord );
		assertEquals( new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( HEADER.toStorageString( ROWS[0] ) ).toRecord(), new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( theRow ).toRecord() );
	}

	@Test
	public void testToRecordIgnoreType() throws HeaderMismatchException, ColumnMismatchException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "*** Testing conversion ignoring types of a row to a record and back ***".toUpperCase() );
		}
		final Row<String> theRow = HEADER.toStorageString( ROWS[0] );
		Record<?> theRecord;
		try {
			theRecord = HEADER.toRecord( theRow );
			fail( "Should not reach this code!" );
		}
		catch ( ColumnMismatchException aException ) {
			/* expected */
		}
		theRecord = HEADER.toRecordIgnoreType( theRow );
		assertEquals( new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( theRow ).toRecord(), new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( HEADER.toRowIgnoreType( theRecord ) ).toRecord() );
	}
}
