// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import static org.junit.jupiter.api.Assertions.*;
import java.text.ParseException;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.textual.CsvBuilder;
import org.refcodes.textual.CsvEscapeMode;

/**
 * Test cases verifying various aspects of the {@link Column} implementation.
 */
public class ColumnTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private enum EnumValues {
		VALUE_A, VALUE_B, VALUE_C, VALUE_D
	}

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testEnumColumn1() throws ParseException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "*** Testing conversion of an enum value to a storage string and back ***".toUpperCase() );
		}
		final Column<EnumValues> theColumn = new EnumColumn<>( "value", EnumValues.class );
		final EnumValues theValue = EnumValues.VALUE_B;
		final String theToStorageString = theColumn.toStorageString( theValue );
		final EnumValues theFromStorageString = theColumn.fromStorageString( theToStorageString );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Value --> To storage string --> From storage string := " + theValue + " --> " + theToStorageString + " --> " + theFromStorageString );
		}
		assertEquals( theValue, theFromStorageString );
	}

	@Test
	public void testEnumColumn2() throws ParseException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "*** Testing conversion of an enum value to a storage string and back ***".toUpperCase() );
		}
		final Column<EnumValues> theColumn = new EnumColumn<>( "value", EnumValues.class );
		final EnumValues theValue = EnumValues.VALUE_C;
		final String theToStorageString = "2";
		final EnumValues theFromStorageString = theColumn.fromStorageString( theToStorageString );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theFromStorageString + " --> " + theFromStorageString );
		}
		assertEquals( theValue, theFromStorageString );
	}

	@Test
	public void testIntegerColumn1() throws ParseException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "*** Testing conversion of an integer value to a storage string and back ***".toUpperCase() );
		}
		final Column<Integer> theColumn = new ObjectColumn<>( "value", Integer.class );
		final Integer theValue = 5161;
		final String theToStorageString = theColumn.toStorageString( theValue );
		final Integer theFromStorageString = theColumn.fromStorageString( theToStorageString );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Value --> To storage string --> From storage string := " + theValue + " --> " + theToStorageString + " --> " + theFromStorageString );
		}
		assertEquals( theValue, theFromStorageString );
	}

	@Test
	public void testIntegerColumn2() throws ParseException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "*** Testing conversion of an integer value to a storage string array and back ***".toUpperCase() );
		}
		final Column<Integer> theColumn = new ObjectColumn<>( "value", Integer.class );
		final Integer theValue = 5161;
		final String[] theToStorageStrings = theColumn.toStorageStrings( theValue );
		final Integer theFromStorageString = theColumn.fromStorageStrings( theToStorageStrings );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Value --> To storage string --> From storage string := " + theValue + " --> " + theToStorageStrings + " --> " + theFromStorageString );
		}
		assertEquals( theValue, theFromStorageString );
	}

	@Test
	public void testIntegersColumn1() throws ParseException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "*** Testing conversion of an integer array to a storage string and back ***".toUpperCase() );
		}
		final Column<Integer[]> theColumn = new ObjectColumn<>( "value", Integer[].class );
		final Integer[] theValues = new Integer[] { 5, 1, 6, 1 };
		final String theToStorageString = theColumn.toStorageString( theValues );
		final Integer[] theFromStorageStrings = theColumn.fromStorageString( theToStorageString );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Value --> To storage string --> From storage string := " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( theValues ).toRecord() + " --> " + theToStorageString + " --> " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( theFromStorageStrings ).toRecord() );
		}
		assertArrayEquals( theValues, theFromStorageStrings );
		for ( int i = 0; i < theValues.length; i++ ) {
			assertEquals( theValues[i], theFromStorageStrings[i] );
		}
	}

	@Test
	public void testIntegersColumn2() throws ParseException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "*** Testing conversion of an integer array to a storage string array and back ***".toUpperCase() );
		}
		final Column<Integer[]> theColumn = new ObjectColumn<>( "value", Integer[].class );
		final Integer[] theValues = new Integer[] { 5, 1, 6, 1 };
		final String[] theToStorageStrings = theColumn.toStorageStrings( theValues );
		final Integer[] theFromStorageStrings = theColumn.fromStorageStrings( theToStorageStrings );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Value --> To storage string --> From storage string := " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( theValues ).toRecord() + " --> " + theToStorageStrings + " --> " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( theFromStorageStrings ).toRecord() );
		}
		assertArrayEquals( theValues, theFromStorageStrings );
		for ( int i = 0; i < theValues.length; i++ ) {
			assertEquals( theValues[i], theFromStorageStrings[i] );
		}
	}

	@Test
	public void testStringColumn1() throws ParseException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "*** Testing conversion of a string value to a storage string and back ***".toUpperCase() );
		}
		final Column<String> theColumn = new StringColumn( "value" );
		final String theValue = "This is a test text.";
		final String theToStorageString = theColumn.toStorageString( theValue );
		final String theFromStorageString = theColumn.fromStorageString( theToStorageString );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Value --> To storage string --> From storage string := " + theValue + " --> " + theToStorageString + " --> " + theFromStorageString );
		}
		assertEquals( theValue, theFromStorageString );
	}

	@Test
	public void testStringColumn2() throws ParseException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "*** Testing conversion of a string value to a storage string array and back ***".toUpperCase() );
		}
		final Column<String> theColumn = new StringColumn( "value" );
		final String theValue = "This is a test text.";
		final String[] theToStorageStrings = theColumn.toStorageStrings( theValue );
		final String theFromStorageString = theColumn.fromStorageStrings( theToStorageStrings );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Value --> To storage string --> From storage string := " + theValue + " --> " + theToStorageStrings + " --> " + theFromStorageString );
		}
		assertEquals( theValue, theFromStorageString );
	}

	@Test
	public void testStringsColumn1() throws ParseException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "*** Testing conversion of a string array to a storage string and back ***".toUpperCase() );
		}
		final Column<String[]> theColumn = new StringsColumn( "value" );
		final String[] theValues = new String[] { "A", "B", "C", "D" };
		final String theToStorageString = theColumn.toStorageString( theValues );
		final String[] theFromStorageStrings = theColumn.fromStorageString( theToStorageString );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Value --> To storage string --> From storage string := " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( theValues ).toRecord() + " --> " + theToStorageString + " --> " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( theFromStorageStrings ).toRecord() );
		}
		assertArrayEquals( theValues, theFromStorageStrings );
		for ( int i = 0; i < theValues.length; i++ ) {
			assertEquals( theValues[i], theFromStorageStrings[i] );
		}
	}

	@Test
	public void testStringsColumn2() throws ParseException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "*** Testing conversion of a string array to a storage string array and back ***".toUpperCase() );
		}
		final Column<String[]> theColumn = new StringsColumn( "value" );
		final String[] theValues = new String[] { "A", "B", "C", "D" };
		final String[] theToStorageStrings = theColumn.toStorageStrings( theValues );
		final String[] theFromStorageStrings = theColumn.fromStorageStrings( theToStorageStrings );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Value --> To storage string --> From storage string := " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( theValues ).toRecord() + " --> " + theToStorageStrings + " --> " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( theFromStorageStrings ).toRecord() );
		}
		assertArrayEquals( theValues, theFromStorageStrings );
		for ( int i = 0; i < theValues.length; i++ ) {
			assertEquals( theValues[i], theFromStorageStrings[i] );
		}
	}
}
