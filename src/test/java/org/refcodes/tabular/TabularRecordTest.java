// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.tabular.TabularSugar.*;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

public class TabularRecordTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testCsvTypeReader() throws IOException, URISyntaxException {
		final String[] theExpected = { "Person [firstName=nolan, lastName=bushnell, age=30]", "Person [firstName=jack, lastName=tramiel, age=29]", "Person [firstName=kommisar, lastName=kniepel, age=45]", "Person [firstName=john, lastName=romero, age=31]", "Person [firstName=john, lastName=carmack, age=27]" };
		final URL theResource = getClass().getClassLoader().getResource( "persons.csv" );
		final File theFile = new File( theResource.getPath() );
		try ( CsvRecordReader<?> theReader = new CsvRecordReader<>( headerOf( stringColumn( "firstName" ), stringColumn( "lastName" ), intColumn( "age" ) ), theFile, ';' ) ) {
			theReader.readHeader();
			int index = 0;
			Person ePerson;
			for ( Record<?> eRecord : theReader ) {
				ePerson = eRecord.toType( Person.class );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( ePerson );
				}
				assertEquals( theExpected[index], ePerson.toString() );
				index++;
			}
			assertEquals( 5, index );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	public static record Person( String firstName, String lastName, int age ) {
		@Override
		public String toString() {
			return "Person [firstName=" + firstName + ", lastName=" + lastName + ", age=" + age + "]";
		}
	}
}
