// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.tabular.TabularSugar.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

public class CsvWriterTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testCsvTypeWriter() throws IOException, URISyntaxException {
		final String testResult = "title;firstName;lastName;age;nolan;bushnell;30;jack;tramiel;29;kommisar;kniepel;45;john;romero;31;john;carmack;27";
		final Person[] thePersons = new Person[] { new Person( "nolan", "bushnell", 30 ), new Person( "jack", "tramiel", 29 ), new Person( "kommisar", "kniepel", 45 ), new Person( "john", "romero", 31 ), new Person( "john", "carmack", 27 ) };
		try ( ByteArrayOutputStream theOutStream = new ByteArrayOutputStream(); CsvRecordWriter<?> theWriter = new CsvRecordWriter<>( headerOf( stringColumn( "title" ), stringColumn( "firstName" ), stringColumn( "lastName" ), intColumn( "age" ) ), theOutStream, ';' ) ) {
			theWriter.writeHeader();
			for ( Person ePerson : thePersons ) {
				theWriter.writeNextType( ePerson );
			}
			final String theOutput = theOutStream.toString().replace( "\n", "" ).replace( "\r", "" );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theOutStream.toString() );
			}
			assertEquals( testResult, theOutput );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	public static class Person {
		public String firstName;
		public String lastName;
		public int age;

		public Person() {}

		public Person( String firstName, String lastName, int age ) {
			this.age = age;
			this.firstName = firstName;
			this.lastName = lastName;
		}

		@Override
		public String toString() {
			return "Person [firstName=" + firstName + ", lastName=" + lastName + ", age=" + age + "]";
		}
	}
}
