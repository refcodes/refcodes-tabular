// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import static org.junit.jupiter.api.Assertions.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.junit.jupiter.api.Test;
import org.refcodes.textual.VerboseTextBuilder;

public class CsvStringWriterTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testCsvStringWriter1() throws IOException {
		final String[][] theCsv = { { "\"Hallo\"", "\"Du\"", "\"Welt\"", "\"Da!\"" }, { "\"He\"llo\"", "\"You\"", "\"World\"", "\"There!\"" } };
		final ByteArrayOutputStream theOut = new ByteArrayOutputStream();
		try ( CsvStringRecordWriter theWriter = new CsvStringRecordWriter( theOut ) ) {
			theWriter.writeHeader( "a", "b", "c", "d" );
			theWriter.writeNext( theCsv[0] );
			theWriter.writeNext( theCsv[1] );
			System.out.println( theOut.toString() );
			final ByteArrayInputStream theIn = new ByteArrayInputStream( theOut.toByteArray() );
			try ( CsvStringRecordReader theReader = new CsvStringRecordReader( theIn ) ) {
				int index = 0;
				String[] eRecord;
				final VerboseTextBuilder theVerbose = new VerboseTextBuilder();
				while ( theReader.hasNext() ) {
					eRecord = theReader.nextRow();
					// eRecord = theReader.getHeader().toValues( theReader.next() );
					System.out.println( index + ": " + theVerbose.toString( theCsv[index] ) );
					System.out.println( index + ": " + theVerbose.toString( eRecord ) );
					assertArrayEquals( theCsv[index++], eRecord );
				}
				assertEquals( theCsv.length, index );
			}
		}
	}

	@Test
	public void testCsvStringWriter2() throws IOException {
		final String[][] theCsv = { { "\"Hallo\"", "\"Du\"", "\"Welt\"", "\"Da!\"" }, { "\"He\"llo\"", "\"You\"", "\"World\"", "\"There!\"" } };
		final ByteArrayOutputStream theOut = new ByteArrayOutputStream();
		try ( CsvStringRecordWriter theWriter = new CsvStringRecordWriter( theOut ) ) {
			theWriter.writeHeader( "a", "b", "c", "d" );
			theWriter.writeNext( theCsv[0] );
			theWriter.writeNext( theCsv[1] );
			System.out.println( theOut.toString() );
			final ByteArrayInputStream theIn = new ByteArrayInputStream( theOut.toByteArray() );
			try ( CsvStringRecordReader theReader = new CsvStringRecordReader( theIn ) ) {
				int index = 0;
				String[] eRecord;
				final VerboseTextBuilder theVerbose = new VerboseTextBuilder();
				while ( theReader.hasNext() ) {
					// eRecord = theReader.readNext();
					eRecord = theReader.getHeader().toValues( theReader.next() );
					System.out.println( index + ": " + theVerbose.toString( theCsv[index] ) );
					System.out.println( index + ": " + theVerbose.toString( eRecord ) );
					assertArrayEquals( theCsv[index++], eRecord );
				}
				assertEquals( theCsv.length, index );
			}
		}
	}

	@Test
	public void testCsvStringWriter3() throws IOException {
		try ( CsvStringRecordWriter theWriter = new CsvStringRecordWriter( System.out ) ) {
			theWriter.writeHeader( "a", "b", "c", "d", "e" );
			theWriter.writeNext( "0a", "0b", "0c", "0d" );
			theWriter.writeNext( "1a", "1b", "1c", "1d" );
			theWriter.writeNext( "2a", "2b", "2c", "2d" );
			theWriter.writeNext( "3a", "3b", "3c", "3d" );
			theWriter.writeNext( "4a", "4b", "4c", "4d" );
			theWriter.writeNext( "5a", "5b", "5c", "5d" );
			theWriter.writeNext( "6a", "6b", "6c", "6d" );
			theWriter.writeNext( "7a", "7b", "7c", "7d" );
			theWriter.writeNext( "8a", "8b", "8c", "8d" );
			theWriter.writeNext( "9a", "9b", "9c", "9d" );
			fail();
		}
		catch ( IllegalArgumentException ignore ) {
			/* Expected */
		}
	}
}
