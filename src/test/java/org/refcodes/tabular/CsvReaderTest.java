// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.tabular.TabularSugar.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.textual.CsvBuilder;
import org.refcodes.textual.CsvEscapeMode;

public class CsvReaderTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Header<Object> HEADER = new HeaderImpl<>( new ObjectColumn<Integer>( "id", Integer.class ), new StringColumn( "alias" ), new StringColumn( "name" ), new StringColumn( "description" ), new StringsColumn( "array" ), new ObjectColumn<Object[]>( "details", Object[].class ) );

	// @formatter:off
	@SuppressWarnings("unchecked")
	private  static final Row<Object>[] ROWS = new Row[] {
		new RowImpl<Object>( 0, "alias0", "name0", "description0", new String[] {"01", "02", "03"}, new Object[] {"A", "B", "C"} ), 
		new RowImpl<Object>( 1, "alias1", "name1", "description1", new String[] {"04", "05", "06"}, new Object[] {"D", "E", "F"} ), 
		new RowImpl<Object>( 2, "alias2", "name2", "description2", new String[] {"07", "08", "09"}, new Object[] {"G", "H", "I"} ), 
		new RowImpl<Object>( 3, "alias3", "name3", "description3", new String[] {"10", "11", "12"}, new Object[] {"J", "K", "L"} ), 
		new RowImpl<Object>( 4, "alias4", "name4", "description4", new String[] {"13", "14", "15"}, new Object[] {"M", "N", "O"} ), 
		new RowImpl<Object>( 5, "alias5", "name5", "description5", new String[] {"16", "17", "18"}, new Object[] {"P", "Q", "R"} ), 
		new RowImpl<Object>( 6, "alias6", "name6", "description6", new String[] {"19", "20", "21"}, new Object[] {"S", "T", "U"} ),
		new RowImpl<Object>( 7, "alias7", "name7", "description7", new String[] {"22", "23", "24"}, new Object[] {"V", "W", "X"} ), 
		new RowImpl<Object>( 8, "alias8", "name8", "description8", new String[] {"25", "26", "27"}, new Object[] {"Y", "Z", "Ä"} ), 
		new RowImpl<Object>( 9 , "alias9", "name9", "description9", new String[] {"28", "29", "30"}, new Object[] {"Ö", "Ü", "ß"} )
	};
	// @formatter:on

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////
	/**
	 * Tests CSV records loaded from a stream (created from a CSV file).
	 */
	@Test
	public void testCsvReader() throws IOException, ColumnMismatchException, HeaderMismatchException {
		final InputStream theInputStream = CsvReaderTest.class.getResourceAsStream( "/records.csv" );
		try ( CsvRecordReader<Object> eRecords = new CsvRecordReader<>( HEADER, theInputStream ) ) {
			Record<Object> eRecord;
			final Rows<?> eRows = new RowsImpl<Object>( HEADER, ROWS );
			Row<?> eRow;
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Header := " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( HEADER.keySet() ).toRecord() );
			}
			while ( eRecords.hasNext() ) {
				eRecord = eRecords.next();
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Printable record := " + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( HEADER.toPrintable( eRecord ).values() ).toRecord() );
				}
				eRow = eRows.next();
				assertEquals( new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( HEADER.toStorageString( eRecord ).values() ).toRecord(), new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( HEADER.toStorageString( HEADER.toRecord( eRow ) ).values() ).toRecord() );
			}
		}
	}

	@Test
	public void testCsvTypeReader() throws IOException, URISyntaxException {
		final String[] theExpected = { "Person [firstName=nolan, lastName=bushnell, age=30]", "Person [firstName=jack, lastName=tramiel, age=29]", "Person [firstName=kommisar, lastName=kniepel, age=45]", "Person [firstName=john, lastName=romero, age=31]", "Person [firstName=john, lastName=carmack, age=27]" };
		final URL theResource = getClass().getClassLoader().getResource( "persons.csv" );
		final File theFile = new File( theResource.getPath() );
		try ( CsvRecordReader<?> theReader = new CsvRecordReader<>( headerOf( stringColumn( "firstName" ), stringColumn( "lastName" ), intColumn( "age" ) ), theFile, ';' ) ) {
			theReader.readHeader();
			int index = 0;
			Person ePerson;
			for ( Record<?> eRecord : theReader ) {
				ePerson = eRecord.toType( Person.class );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( ePerson );
				}
				assertEquals( theExpected[index], ePerson.toString() );
				index++;
			}
			assertEquals( 5, index );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	public static class Person {
		@Override
		public String toString() {
			return "Person [firstName=" + firstName + ", lastName=" + lastName + ", age=" + age + "]";
		}

		public String firstName;
		public String lastName;
		public int age;
	}
}
