// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.tabular;

import static org.junit.jupiter.api.Assertions.*;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.tabular.CsvReaderTest.Person;

/**
 * @author steiner
 */
public class CsvStringReaderTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testCsvStringReader() throws IOException, URISyntaxException {
		final URL theResource = getClass().getClassLoader().getResource( "string_records.csv" );
		final File theFile = new File( theResource.getPath() );
		try ( CsvStringRecordReader theReader = new CsvStringRecordReader( theFile, ';' ) ) {
			// theReader.readHeader();
			final Header<String> theHeader = theReader.getHeader();
			//for ( Column<? extends String> eElement : theHeader ) {
			//	System.out.println( eElement.getKey() );
			//}
			int index = 0;
			for ( Record<String> eRecord : theReader ) {
				for ( Column<?> eColumn : theHeader ) {
					// System.out.println( index + ": " + eColumn.getKey() + " = " + eRecord.get( eColumn.getKey() ) );
					assertEquals( index + eColumn.getKey(), eRecord.get( eColumn.getKey() ) );
				}
				for ( String eKey : theHeader.keySet() ) {
					// System.out.println( index + ": " + eHeader.getKey() + " = " + eRecord.get( eHeader.getKey() ) );
					assertEquals( index + eKey, eRecord.get( eKey ) );
				}
				index++;
			}
			assertEquals( 10, index );
		}
	}

	@Test
	public void testCsvTypeStringReader() throws IOException, URISyntaxException {
		final String[] theExpected = { "Person [firstName=nolan, lastName=bushnell, age=30]", "Person [firstName=jack, lastName=tramiel, age=29]", "Person [firstName=kommisar, lastName=kniepel, age=45]", "Person [firstName=john, lastName=romero, age=31]", "Person [firstName=john, lastName=carmack, age=27]" };
		final URL theResource = getClass().getClassLoader().getResource( "persons.csv" );
		final File theFile = new File( theResource.getPath() );
		try ( CsvStringRecordReader theReader = new CsvStringRecordReader( theFile, ';' ) ) {
			theReader.readHeader();
			int index = 0;
			Person ePerson;
			while ( theReader.hasNext() ) {
				ePerson = theReader.nextType( Person.class );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( ePerson );
				}
				assertEquals( theExpected[index], ePerson.toString() );
				index++;
			}
			assertEquals( 5, index );
		}
	}
}
