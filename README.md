# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***This archetype helps processing table like data structures including the processing of*** **[CSV](https://en.wikipedia.org/wiki/Comma-separated_values)** ***files with records, headers as well as comments whilst supporting*** **[Plain old Java objects](https://en.wikipedia.org/wiki/Plain_old_Java_object)** ***(`POJO`) and simple new*** **Java [record](https://openjdk.java.net/jeps/395)** ***types.***

## Getting started ##

> Please refer to the [refcodes-tabular: Process tabular data and CSV files using POJOs](https://www.metacodes.pro/refcodes/refcodes-tabular) documentation for an up-to-date and detailed description on the usage of this artifact. 

## How do I get set up? ##

To get up and running, include the following dependency (without the three dots "...") in your `pom.xml`:

```
<dependencies>
	...
	<dependency>
		<groupId>org.refcodes</groupId>
		<artifactId>refcodes-tabular</artifactId>
		<version>3.3.9</version>
	</dependency>
	...
</dependencies>
```

The artifact is hosted directly at [Maven Central](http://search.maven.org). Jump straight to the source codes at [Bitbucket](https://bitbucket.org/refcodes/refcodes-tabular). Read the artifact's javadoc at [javadoc.io](http://www.javadoc.io/doc/org.refcodes/refcodes-tabular).

## Contribution guidelines ##

* [Report issues](https://bitbucket.org/refcodes/refcodes-tabular/issues)
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.